import ROOT
import argparse
import matplotlib.pyplot as plt
import numpy as np
from rootpy.plotting.style import get_style, set_style
from matplotlib.ticker import MultipleLocator, AutoMinorLocator
from matplotlib import rc
from scipy.stats import linregress
import logging 
logging.getLogger("matplotlib").setLevel(logging.WARNING)
set_style("ATLAS", mpl=True)
from utils import eta_map, pt_map

parser = argparse.ArgumentParser("Give pT and eta")
parser.add_argument("-e", dest="eta", required=True, help="eta bin")
parser.add_argument("-p", dest="pt", required=True, help="pT bin")
parser.add_argument("-l", dest="linFit", action='store_true', help='Do Linear Fit')
args = parser.parse_args()
if args.eta == 4: sys.exit()

fdir = "/xdata/tburch/workspace/photonid/data/evaluated_bdt_binned/"
output_dir = "/xdata/tburch/workspace/photonid/run/profiles/"

def mpl_atlasLabel(x,y,ha):
    rc('text', usetex=True)
    hfont = {'fontname': 'Helvetica'} 

    plt.annotate(r"\textit{\textbf{ATLAS}} Internal", xy=(x, y), xycoords='axes fraction', horizontalalignment=ha,fontsize=18,  **hfont)
    rc('text', usetex=False)

def getProfileParameters(tree, vx, vy, argdict):
    ext = "{0}_{1}_{2}_prof".format(tree.GetName(),vx,vy)
    p = ROOT.TProfile(ext, ext, argdict["ybin"], argdict["ymin"], argdict["ymax"],0,1)
    tree.Draw(
        "{0}:{1}>>{2}".format(vx,vy,ext),
        "mcTotWeight",
        "prof")

    vals, errs = [],[]
    for i in range(0, argdict["ybin"]):
        vals.append(p.GetBinContent(i))
        errs.append(p.GetBinError(i))
    w = p.GetBinWidth(2)
    
    return vals, errs, w


def lin_fit_profile(xvals, yvals, mean, std, nStd):
    x = np.array(xvals)
    y = np.array(yvals)

    fit_elements = np.logical_and( 
        x > mean - std*nStd,
        x < mean + std*nStd
    )

    x = x[fit_elements]
    y = y[fit_elements]

    return linregress(x,y)


def pileup_v_score(t_sig, t_bkg):
    pileup_dict ={
        "ymin":5,
        "ymax":45,
        "ybin":8
    }

    y_signal, e_signal, w = getProfileParameters(t_sig, "bdt_eval","evt_mu", pileup_dict)
    y_bkg, e_bkg, _ = getProfileParameters(t_bkg, "bdt_eval","evt_mu", pileup_dict)
    
    xvals = np.linspace(pileup_dict["ymin"], pileup_dict["ymax"], pileup_dict["ybin"])
    x_bin_size = w/2.0
    plt.errorbar(x=xvals, y=y_signal, xerr=x_bin_size, yerr=e_signal, linestyle="none", color="cornflowerblue", label="Signal (truth-matched photons)")
    plt.errorbar(x=xvals, y=y_bkg, xerr=x_bin_size, yerr=e_bkg, linestyle="none", color="firebrick", label="Bkg (truth-vetoed jet-fakes)")
    
    plt.gca().xaxis.set_minor_locator(AutoMinorLocator())
    plt.gca().yaxis.set_minor_locator(AutoMinorLocator())

    if args.linFit:
        std = 6.8
        mean = 22.9
        nstd = 1.5
        s_slope, s_intercept, s_r, s_p, s_stderr = lin_fit_profile(xvals, y_signal, mean=mean, std=std, nStd=nstd)
        b_slope, b_intercept, b_r, b_p, b_stderr = lin_fit_profile(xvals, y_bkg, mean=mean, std=std, nStd=nstd) # 87%
        
        line_range = (mean - std*nstd, mean + std*nstd)
        lin_x = np.arange(line_range[0],line_range[1],0.01)
        s_lin_y = s_slope*lin_x + s_intercept
        b_lin_y = b_slope*lin_x + b_intercept
        plt.plot(lin_x, s_lin_y, color="cornflowerblue", linestyle="--")
        plt.plot(lin_x, b_lin_y, color="firebrick", linestyle="--")
        # TODO : figure out a better way to store this info
        print(s_slope)
        print(b_slope)

    plt.xlabel("$\mu$", ha='right', x=1)
    plt.ylabel("BDT Score",ha='right',y=1)

    eta_s = "$%.2f < |\eta| < %.2f$" % (eta_map[args.eta][0], eta_map[args.eta][1])
    pt_s = "$%i < pT < %i$" % (pt_map[args.pt][0], pt_map[args.pt][1])


    plt.gca().xaxis.set_major_locator(MultipleLocator(5))
    plt.ylim(bottom=0, top=1)
    mpl_atlasLabel(.98, .93, "right")
    hfont = {'fontname': 'Helvetica'} 
    plt.annotate(eta_s, xy=(0.98, 0.89), xycoords='axes fraction', horizontalalignment="right",fontsize=14,  **hfont)
    plt.annotate(pt_s, xy=(0.98, 0.84), xycoords='axes fraction', horizontalalignment="right",fontsize=14,  **hfont)

    plt.legend(frameon=False,loc="lower right", fontsize=16)
    plt.savefig(output_dir+"/pileupProfile_eta{0}pt{1}.png".format(args.eta, args.pt))


def main():
    f = ROOT.TFile(fdir+"evaluated_eta{0}pt{1}_binnedModel.root".format(args.eta, args.pt))
    t_sig = f.Get("gammajet")
    t_bkg = f.Get("jetjet")
    pileup_v_score(t_sig, t_bkg)


if __name__ == "__main__":
    main()