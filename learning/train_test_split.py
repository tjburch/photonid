import ROOT
import sys

def eventloop(t_in, f_out):
    
    t_out_train = t_in.CloneTree(0)
    t_out_train.SetName(t_in.GetName()+"_train")
    t_out_test = t_in.CloneTree(0)
    t_out_test.SetName(t_in.GetName()+"_test")

    print("iterating over {0} entries".format(t_in.GetEntries()))
    check_reg = t_in.GetEntries()/10    

    for e in range(t_in.GetEntries()):
        if e%check_reg == 0 : print("On event {0}".format(e))

        t_in.GetEntry(e)

        if e%2 == 1:
            t_out_train.Fill()
        else:
            t_out_test.Fill()

    f_out.cd()
    t_out_train.Write()
    t_out_test.Write()


if __name__ == "__main__":
    
    s_in = sys.argv[1]
    if len(sys.argv) == 3:
        treename = sys.argv[2]
    else:
        treename = "SinglePhoton"


    f_in = ROOT.TFile(s_in)
    
    typeB = False # typeB has both gammajet and jetjet in one file
    for key in f_in.GetListOfKeys():
        if "GammaJet" in str(key):
            typeB = True

    if not typeB:
        trees_todo = ["SinglePhoton"]
    else:
        trees_todo = ["GammaJet","JetJet"]

    if not typeB: print("fail")
    t_in = [ f_in.Get(treename) for treename in trees_todo]
    s_out = s_in.split(".root")[0]+"_traintest.root"
    f_out = ROOT.TFile(s_out,"RECREATE")

    for t in t_in:
        print("Doing tree: {0}".format(t.GetName()))
        eventloop(t,f_out)
    f_out.Close()