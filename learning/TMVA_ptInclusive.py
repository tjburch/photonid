# mimic https://indico.cern.ch/event/725111/contributions/2984861/attachments/1640770/2619953/PIDOptimization_status_20180426.pdf
# This for a baseline test, do with and without topo cluster variables
import ROOT
import shutil
import os
import argparse
import sys
from array import array

# Configurables
parser = argparse.ArgumentParser('BDT')
parser.add_argument('-e', dest='eta', required=True, help='Input SinglePhoton ntuple')
parser.add_argument('-t', dest='topo',action='store_true', help='do topo file')
parser.add_argument('-c', dest='converted',action='store_true', help='conversion')
parser.add_argument('--no-reweight', dest='noReweight',action='store_true', help='Dont reweight pT distribution')
args = parser.parse_args()
if args.eta == 4: sys.exit()

eta_map={
    "0" : (0, 0.6),
    "1" : (0.6, 0.8),
    "2" : (0.8, 1.15),
    "3" : (1.15, 1.37),
    "5" : (1.52, 1.81),
    "6" : (1.81, 2.01),
    "7" : (2.01, 2.37)
}


# Initalize files, TMVA objects
workdir = '/bdata/tburch/tmva_submit'
#writedir = workdir+'/models/'
writedir = workdir
#output = writedir+'tmva_output_topoAdded.root'
print "Inputs - conv: ", args.converted, " topo: ",args.topo," eta: ",args.eta 
if args.converted: s_c = 'Converted'
else:  s_c = 'Unconverted'
if args.topo: s_t = 'topo'
else: s_t = 'nominal'
#output = writedir+'BDT_eta%spt%s%s_%s.root' % (args.eta, args.pt, s_c, s_t)
output = '/bdata/tburch/tmva_submit/'+'BDT_eta%sptInclusive%s_%s.root' % (args.eta, s_c, s_t)
outputFile = ROOT.TFile.Open(output, 'RECREATE')
ROOT.TMVA.UseOffsetMethod = True

# Create Factory Object
factoryOptions = '!Silent:Transformations=I;D;P;G,D:Correlations'
factory = ROOT.TMVA.Factory('BoostedDecisionTree', outputFile, factoryOptions)

# Create Dataloader
dataloader = ROOT.TMVA.DataLoader('SinglePhoton')
dataloader.AddVariable('y_Rhad1','F')
dataloader.AddVariable('y_Reta', 'F')
dataloader.AddVariable('y_Rphi', 'F')
dataloader.AddVariable('y_weta1', 'F')
dataloader.AddVariable('y_weta2', 'F')
dataloader.AddVariable('y_wtots1', 'F')
dataloader.AddVariable('y_fracs1', 'F')
dataloader.AddVariable('y_deltae', 'F')
dataloader.AddVariable('y_Eratio', 'F')

# Other Variables for correlation
dataloader.AddSpectator('y_ptcone20','F')
dataloader.AddSpectator('y_ptcone40','F')
dataloader.AddSpectator('y_topoetcone20','F')
dataloader.AddSpectator('y_topoetcone40','F')
dataloader.AddSpectator('y_iso_FixedCutLoose','F')
dataloader.AddSpectator('y_iso_FixedCutTight','F')
dataloader.AddSpectator('y_iso_FixedCutTightCaloOnly','F')
dataloader.AddSpectator('mcTotWeight','F')
dataloader.AddSpectator('reweight_SF','F')

# new
if args.topo:
    dataloader.AddVariable('y_topoCluster0_secondLambda', 'F')
    dataloader.AddVariable('y_topoCluster0_secondR', 'F')
    dataloader.AddVariable('y_nTopoClusters', 'I')
    dataloader.AddVariable('y_topoCluster0_centerLambda', 'F')
    dataloader.AddVariable('y_topoCluster0_emProbability', 'F')
    dataloader.AddVariable('y_topoCluster0_isolation', 'F')


# Get all trees
#data_dir = workdir+'/ntuples/'
data_dir = workdir
tree_file = ROOT.TFile('/bdata/tburch/tmva_submit/TMVA_ready_withSF_traintest.root')
signalTree = tree_file.Get('GammaJet_train')
backgroundTree = tree_file.Get('JetJet_train')
signalTree_test = tree_file.Get('GammaJet_test')
backgroundTree_test = tree_file.Get('JetJet_test')    

# Set Weights TODO
# ---------------------------------------------------------------
if args.noReweight:
    weight_expression = 'mcTotWeight'
else:
    weight_expression = 'mcTotWeight * reweight_SF * y_topoCluster0_isolation'
print("Reweighting expression: "+weight_expression)
dataloader.SetWeightExpression(weight_expression) # adds pT Reweighting

# Prep Dataset
# ------------------------------------------------------------------


# Build cut
s_cut = 'acceptEventPtBin && '
s_cut += 'y_IsLoose!=0 && '
s_cut += 'y_f1 > 0.005 && '
s_cut += 'y_wtots1 > -10 && '
s_cut += 'y_weta1 > -100 && '
if args.converted:
    s_cut += 'y_convType != 0 && '
else:
    s_cut += 'y_convType == 0 && '
s_cut += 'abs(y_eta) > %s && ' % eta_map[args.eta][0]
s_cut += 'abs(y_eta) < %s && ' % eta_map[args.eta][1]
s_cut += 'evt_mu > 1 && '
s_cut += 'evt_mu < 60 '


preselection_cut = ROOT.TCut(s_cut)
split_options = 'nTrain_Signal=0:'  # all signal
split_options += 'nTrain_Background=0:'  # all background
split_options += 'NormMode=NumEvents:'  # Normalization of weights
#split_options += 'CreateMVAPdfs=True:'
split_options += 'SplitMode=Random:'
split_options += 'EqualNumEvents:'


print "TJB # signal: ", signalTree.GetEntries()
print "TJB # bkg: ", backgroundTree.GetEntries()
print s_cut
print "TJB # signal after selection: ", signalTree.GetEntries(s_cut + ' && y_isTruthMatchedPhoton!=0')
print "TJB # bkg after selection: ", backgroundTree.GetEntries(s_cut + ' && y_isTruthMatchedPhoton==0')
print split_options

sigcut = ROOT.TCut(s_cut + ' && y_isTruthMatchedPhoton!=0')
bkgcut = ROOT.TCut(s_cut + ' && y_isTruthMatchedPhoton==0')

#  Load in and prepare trees
dataloader.AddSignalTree(signalTree,1,"train")
dataloader.AddSignalTree(signalTree_test,1,"test")
dataloader.AddBackgroundTree(backgroundTree,1,"train")
dataloader.AddBackgroundTree(backgroundTree_test,1,"test")

dataloader.PrepareTrainingAndTestTree(sigcut, bkgcut, split_options)
# note: All events which have not been marked when the trees are provided are split 
# according to the options given in PrepareTrainingAndTestTree.


# MVA Settings
# ------------------------------------------------------------------

# BDT 
bdt_opts = 'NegWeightTreatment=IgnoreNegWeightsInTraining:'
bdt_opts += 'BoostType=Grad:'
bdt_opts += 'NTrees=800'
bdt = factory.BookMethod(dataloader, ROOT.TMVA.Types.kBDT, 'BDT', bdt_opts)

# Rectangular Cuts (no fixed range here.)
cuts_options = 'H:V:'
cuts_options += 'FitMethod=GA:'
cuts_options += 'VarProp=FSmart:'
cuts_options += 'EffSel:'
cuts_options += 'CreateMVAPdfs:'
cuts_options += 'Popsize=500:'
cuts_options += 'Steps=20:'
cuts_options += 'Cycles=6'

#rect = factory.BookMethod(dataloader, ROOT.TMVA.Types.kCuts, 'cuts', cuts_options)


# Train, Test, Evaluate all methods
# ------------------------------------------------------------------
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()

# Clean up

outputFile.Close()
#writedir = workdir+'/models/'
#os.rename('SinglePhoton', '/bdata/tburch/tmva_submit/BDT_eta%sptInclusive%s_%s_reweighted'% (args.eta,  s_c, s_t) ) 
