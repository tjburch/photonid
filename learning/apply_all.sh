touch $1
echo "eta,pt,signal_efficiency,background_rejection" >> $1
for eta in 0 1 2 3 5 6 7; do
    for pt in {1..12}; do
        python apply_model_binned.py -e ${eta} -p ${pt} >> $1
    done
done