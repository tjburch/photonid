import matplotlib.pyplot as plt
import numpy as np
import h5py

eta_map={
    "0" : (0, 0.6),
    "1" : (0.6, 0.8),
    "2" : (0.8, 1.15),
    "3" : (1.15, 1.37),
    "5" : (1.52, 1.81),
    "6" : (1.81, 2.01),
    "7" : (2.01, 2.37)
}
pt_map={
    "1" : (25,30),
    "2" : (30,40),
    "3" : (40,50),
    "4" : (50,60),
    "5" : (60,80),
    "6" : (80,100),
    "7" : (100,125),
    "8" : (125,150),
    "9" : (150,175),
    "10" : (175,250),
    "11" : (250, 500),
    "12" : (500,1500),
    "high" : (80,1500),
    "g100": (100,1500),
}

def flatten(data, weights, binWidth):
    """ Find resampling weight so outcome is flat in pT
    1) Make some fixed binning size in pT (5 GeV?)
    2) Calculate nEvents_{bin} / nEvents_{total} in each bin as weighting
    3) Return event by event weights
    """
    
    # 1) Find bins
    # TODO: This binning works, but would be nice to make this more generic    
    bins = np.arange(0, 55, 5)
    bins = np.append(bins, [60, 80, 100, 125, 175, 250, 400, 600, 1000])

    binVals, binEdges = np.histogram(data,bins=bins,weights=weights)
    binVals = np.append(binVals, [data[data>bins[-1]].shape[0]])

    # 2) Calculate nEvents_{bin} / nEvents_{total}
    # indicies = np.digitize(data, binEdges)
    try:
        indicies = np.digitize(data, bins[:-1])
        normalize = np.ones(binVals.shape, dtype=np.float64)/binVals
    except ValueError: 
        raise ValueError("Bin size bigger than pT range!")

    # Make array of weights
    weights = np.array([normalize[indicies[i]-1] for i in range(indicies.shape[0]) ])
    return weights




def reweight(base_data, reweight_data, base_weights, rw_weights, binWidth):
    """Make distribution A look like distribution B
    """

    # Get bins
    bins = np.arange(0, 1000, binWidth)
    base_binVals, base_binEdges = np.histogram(base_data, bins=bins, weights=base_weights)
    rw_binVals, rw_binEdges = np.histogram(reweight_data, bins=bins, weights=rw_weights)

    try:
        base_indicies = np.digitize(base_data, bins[:-1])
        rw_indicies = np.digitize(reweight_data, bins[:-1])
        scale_factor_bins = np.divide(base_binVals, rw_binVals)
        weights = np.array([scale_factor_bins[rw_indicies[i]-1] for i in range(rw_indicies.shape[0]) ])
        return weights
    except ValueError: 
        raise ValueError("Bin size bigger than pT range!")    
    

# Below here add tests of functions
# ---------------------------------
def test_flattening():

    print("Loading Files...")
    f_gammajet = h5py.File('/blues/gpfs/group/3/ATLAS/PhotonID/singlephoton/h5/preselection_gammajet_mc16a.h5', 'r')
    np_arr_gammajet_0 = f_gammajet.get('SinglePhoton')
    print("Done!")

    # Skim down to reasonable pT
    # TODO: for now I'll just look up to a TeV... revisit later
    np_arr_gammajet_0 = np_arr_gammajet_0[np_arr_gammajet_0["y_pt"] < 1000]

    pt = 'Inclusive'
    eta = "0"
    print('Skimming to bin %.2f < |eta| < %.2f, pT Inclusive, %s Photons' % \
         (eta_map[eta][0], eta_map[eta][1], 'unconverted')) 
    np_arr_gammajet = np_arr_gammajet_0[(np.abs(np_arr_gammajet_0['y_eta']) > eta_map[eta][0]) & (np.abs(np_arr_gammajet_0['y_eta']) < eta_map[eta][1])]

    flatten_weights = flatten(np_arr_gammajet["y_pt"], np_arr_gammajet['mcTotWeight'], 5)#* 1E9
    total_weights = np_arr_gammajet['mcTotWeight'] * flatten_weights

    bins = np.arange(0, 55, 5)
    bins = np.append(bins, [60, 80, 100, 125, 175, 250, 400, 600, 1000])

    ## Make plots with it
    fig, (ax1,ax2) = plt.subplots(nrows=1,ncols=2)
    bigax = fig.add_subplot(111,frameon=False)
    bigax.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    plt.sca(ax1)
    plt.hist(np_arr_gammajet['y_pt'], weights=total_weights, bins=bins)
    ax1.set_title("Flattened in pT")
    plt.sca(ax2)
    plt.hist(np_arr_gammajet['y_pt'],  weights=np_arr_gammajet['mcTotWeight'], bins=bins)
    plt.yscale('log')
    ax2.set_title("Not Flattened")
    bigax.set_xlabel('$p_{T_{\gamma}}$ [GeV]')
    bigax.set_ylabel('Events')
    plt.tight_layout()
    plt.savefig('flatten_pt.pdf')
    plt.close()

    fig, (ax1,ax2) = plt.subplots(nrows=1,ncols=2)
    plt.yscale('log')
    bigax = fig.add_subplot(111,frameon=False)
    bigax.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    plt.sca(ax1)
    plt.hist(np_arr_gammajet['y_eta'], weights=total_weights, bins=50)
    ax1.set_title("Flattened in pT")
    plt.sca(ax2)
    plt.hist(np_arr_gammajet['y_eta'], weights=np_arr_gammajet['mcTotWeight'], bins=50)
    ax2.set_title("Not Flattened")
    bigax.set_xlabel('$\eta_{\gamma}$ ')
    plt.tight_layout()
    plt.savefig('flatten_eta.pdf')
    plt.close()

    fig, (ax1,ax2) = plt.subplots(nrows=1,ncols=2)
    plt.yscale('log')
    bigax = fig.add_subplot(111,frameon=False)
    bigax.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    plt.sca(ax1)
    plt.hist(np_arr_gammajet['y_topoCluster0_secondR'], weights=total_weights)
    ax1.set_title("Flattened in pT")
    plt.sca(ax2)
    plt.hist(np_arr_gammajet['y_topoCluster0_secondR'], weights=np_arr_gammajet['mcTotWeight'])#, density=True)
    ax2.set_title("Not Flattened")
    bigax.set_xlabel('$\sqrt{r^{2}}$')
    plt.tight_layout()
    plt.savefig('flatten_topo.pdf')
    plt.close()

    fig = plt.figure()
    ax = plt.gca()
    plt.yscale('log')
    plt.hist(flatten_weights)
    ax.set_xlabel("Flattening weight")
    plt.savefig('flatten_weights.pdf')
    plt.close()

def test_reweighting():
    print("Loading Files...")
    f_gammajet = h5py.File('/blues/gpfs/group/3/ATLAS/PhotonID/singlephoton/h5/preselection_gammajet_mc16a.h5', 'r')
    f_jetjet = h5py.File('/blues/gpfs/group/3/ATLAS/PhotonID/singlephoton/h5/preselection_jetjet_mc16a.h5', 'r')
    np_arr_gammajet_0 = f_gammajet.get('SinglePhoton')
    np_arr_jetjet_0 = f_jetjet.get('SinglePhoton')
    print("Done!")

    # Skim down to reasonable pT
    # TODO: for now I'll just look up to a TeV... revisit later
    np_arr_gammajet_0 = np_arr_gammajet_0[np_arr_gammajet_0["y_pt"] < 1000]
    np_arr_jetjet_0 = np_arr_jetjet_0[np_arr_jetjet_0["y_pt"] < 1000]

    pt = 'Inclusive'
    eta = "0"
    print('Skimming to bin %.2f < |eta| < %.2f, pT Inclusive, %s Photons' % \
         (eta_map[eta][0], eta_map[eta][1], 'unconverted')) 
    np_arr_gammajet = np_arr_gammajet_0[(np.abs(np_arr_gammajet_0['y_eta']) > eta_map[eta][0]) & (np.abs(np_arr_gammajet_0['y_eta']) < eta_map[eta][1])]
    np_arr_jetjet = np_arr_jetjet_0[(np.abs(np_arr_jetjet_0['y_eta']) > eta_map[eta][0]) & (np.abs(np_arr_jetjet_0['y_eta']) < eta_map[eta][1])]

    rescale_weights = reweight(
        base_data=np_arr_gammajet["y_pt"],
        reweight_data=np_arr_jetjet["y_pt"],
        base_weights=np_arr_gammajet['mcTotWeight'],
        rw_weights=np_arr_jetjet['mcTotWeight'],
        binWidth=5
    )

    signal_weights = np_arr_gammajet['mcTotWeight'] 
    background_weights = np_arr_jetjet['mcTotWeight'] * rescale_weights

    bins = np.arange(0, 1000, 5)

    # Make Ratio plot
    # Normal
    fig, ax = plt.subplots(nrows=2, gridspec_kw={'height_ratios': [3, 1]})
    bigax = fig.add_subplot(111,frameon=False)
    bigax.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    s, s_bins, s_patches = ax[0].hist(np_arr_gammajet['y_pt'], 
                                      weights=np_arr_gammajet['mcTotWeight'],
                                      bins=bins, histtype='step', 
                                      color='cornflowerblue',
                                      label='Signal (truth-matched photons)',
                                      density=True)

    b, b_bins, b_patches = ax[0].hist(np_arr_jetjet['y_pt'],
                                      weights=np_arr_jetjet['mcTotWeight'], 
                                      bins=bins, histtype='step', 
                                      color='firebrick',
                                      label='Bkg (truth-vetoed jet-fakes)',
                                      density=True)
    for a in ax:
        a.set_xlim(left=0, right=250)

    ax[0].legend(frameon=False)
    
    # Ratio plot
    minvalue = min([np_arr_gammajet["y_pt"].min(), np_arr_jetjet["y_pt"].min()])
    # TODO: yerrors
    ax[1].errorbar(x=s_bins[:-1]+2.5, y=s/b, xerr=2.5,color='forestgreen',linestyle='none')

    # Stylize
    ax[0].set_ylabel("Events (Normalized to 1)", ha='right', position=(0.5,1))
    ax[1].set_ylabel("S/B")
    ax[1].set_xlabel("$pT_{\gamma}$", ha='right', position=(1,0.5))
    ax[1].set_ylim(bottom=0,top=2)
    ax[1].axhline(y=1,color='grey',linestyle='--')
    fig.align_ylabels(ax)

    plt.savefig('pt_ratio')

    # Normal
    fig, ax = plt.subplots(nrows=2, gridspec_kw={'height_ratios': [3, 1]})
    bigax = fig.add_subplot(111,frameon=False)
    bigax.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    s, s_bins, s_patches = ax[0].hist(np_arr_gammajet['y_pt'], 
                                      weights=signal_weights,
                                      bins=bins, histtype='step', 
                                      color='cornflowerblue',
                                      label='Signal (truth-matched photons)',
                                      )

    b, b_bins, b_patches = ax[0].hist(np_arr_jetjet['y_pt'],
                                      weights=background_weights, 
                                      bins=bins, histtype='step', 
                                      color='firebrick',
                                      label='Bkg (truth-vetoed jet-fakes)',
                                      )
    for a in ax:
        a.set_xlim(left=0,right=250)

    ax[0].legend(frameon=False)

    ax[1].errorbar(x=s_bins[:-1]+2.5, y=s/b, xerr=2.5, color='forestgreen',linestyle='none')

    ax[0].set_ylabel("Events", ha='right', position=(0.5,1))
    ax[1].set_ylabel("S/B")
    ax[1].set_ylim(bottom=0,top=2)
    ax[1].axhline(y=1,color='grey',linestyle='--')
    ax[1].set_xlabel("$pT_{\gamma}$", ha='right', position=(1,0.5))
    fig.align_ylabels(ax)

    plt.savefig('pt_ratio_rw')




if __name__ == "__main__":
    #test_flattening()
    test_reweighting()