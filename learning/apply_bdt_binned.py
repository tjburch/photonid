import ROOT
import array
from utils import eta_map, pt_map
import argparse

pt_map={
    "1" : (25,30),
    "2" : (30,40),
    "3" : (40,50),
    "4" : (50,60),
    "5" : (60,80),
    "6" : (80,1500),
#    "7":  (100,1500),
}

parser = argparse.ArgumentParser("Give pT and eta")
parser.add_argument("-e", dest="eta", required=True, help="eta bin")
parser.add_argument("-p", dest="pt", required=True, help="pT bin")
parser.add_argument('-c', dest='converted',action='store_true', help='conversion')
parser.add_argument('-t', dest='topo',action='store_true', help='do topo clusters')
parser.add_argument('--inclusive', dest='inclusive',action='store_true', help='Dont reweight pT distribution')
parser.add_argument('--reweight', dest='reweight',action='store_true', help='Dont reweight pT distribution')
parser.add_argument('--high-pt', dest='highPt',action='store_true', help='Use High pT model')
parser.add_argument('--cuts', dest='cuts', action='store_true')
#parser.add_argument('--pt-g100', dest='pt100',action='store_true', help='Evaluate for pT > 100')
args = parser.parse_args()
if args.eta == 4: sys.exit()

ttree_file = ROOT.TFile('/bdata/tburch/tmva_submit/TMVA_ready_withSF_traintest.root')
signalTree = ttree_file.Get('GammaJet_test')
backgroundTree = ttree_file.Get('JetJet_test')

spectators = 10
if not args.cuts:
    if args.reweight:
        if args.topo:
            model_name = '/bdata/tburch/tmva_submit/BDT_eta{0}ptInclusive_t_Reweight/weights/BoostedDecisionTree_BDT.weights.xml'.format(args.eta)
            output='/xdata/tburch/workspace/photonid/data/evaluated_bdt_reweight/evaluated_eta{0}pt{1}_topo_reweight.root'.format(args.eta,args.pt)
            spectators = 8
        else:
            model_name = '/bdata/tburch/tmva_submit/BDT_eta{0}ptInclusive__reweighted/weights/BoostedDecisionTree_BDT.weights.xml'.format(args.eta)
            output='/xdata/tburch/workspace/photonid/data/evaluated_bdt_reweight/evaluated_eta{0}pt{1}_reweight.root'.format(args.eta,args.pt)

    elif args.highPt:
        model_name = '/bdata/tburch/tmva_submit/BDT_eta{0}ptHigh/weights/BoostedDecisionTree_BDT.weights.xml'.format(args.eta)
        output='/xdata/tburch/workspace/photonid/data/evaluated_bdt_highPt/evaluated_eta{0}pt{1}HighModel.root'.format(args.eta,args.pt)    
        spectators = 9
    elif args.inclusive:
        if args.topo:
            model_name = '/bdata/tburch/tmva_submit/BDT_eta{0}ptInclusive_t_/weights/BoostedDecisionTree_BDT.weights.xml'.format(args.eta)
            output='/xdata/tburch/workspace/photonid/data/evaluated_bdt_inclusive/evaluated_eta{0}pt{1}_topo_inclusiveModel.root'.format(args.eta,args.pt)
            spectators = 8
        else:
            model_name = '/bdata/tburch/tmva_submit/BDT_eta{0}ptInclusive__/weights/BoostedDecisionTree_BDT.weights.xml'.format(args.eta)
            output='/xdata/tburch/workspace/photonid/data/evaluated_bdt_inclusive/evaluated_eta{0}pt{1}_inclusiveModel.root'.format(args.eta,args.pt)   
            spectators = 8
    else:    
        if args.topo:
            model_name = '/bdata/tburch/tmva_submit/BDT_eta{0}pt{1}_t/weights/BoostedDecisionTree_BDT.weights.xml'.format(args.eta,args.pt)
            output='/xdata/tburch/workspace/photonid/data/evaluated_bdt_binned/evaluated_eta{0}pt{1}_topo_binnedModel.root'.format(args.eta,args.pt)
            spectators = 7    
        else:
            model_name = '/bdata/tburch/tmva_submit/BDT_eta{0}pt{1}_/weights/BoostedDecisionTree_BDT.weights.xml'.format(args.eta,args.pt)
            output='/xdata/tburch/workspace/photonid/data/evaluated_bdt_binned/evaluated_eta{0}pt{1}_binnedModel.root'.format(args.eta,args.pt)
            spectators = 7 
if args.cuts:
    if args.converted:
        if args.topo:
            model_name = '/bdata/tburch/tmva_submit/BDT_eta{0}pt{1}_tc/weights/BoostedDecisionTree_cuts.weights.xml'.format(args.eta,args.pt)
            output='/xdata/tburch/workspace/photonid/data/evaluated_bdt_binned/evaluated_eta{0}pt{1}_topo_cuts_conv.root'.format(args.eta,args.pt)
            spectators = 7    
        else:
            model_name = '/bdata/tburch/tmva_submit/BDT_eta{0}pt{1}_c/weights/BoostedDecisionTree_cuts.weights.xml'.format(args.eta,args.pt)
            output='/xdata/tburch/workspace/photonid/data/evaluated_bdt_binned/evaluated_eta{0}pt{1}_cuts_conv.root'.format(args.eta,args.pt)
            spectators = 7 

    else:
        if args.topo:
            model_name = '/bdata/tburch/tmva_submit/BDT_eta{0}pt{1}_t/weights/BoostedDecisionTree_cuts.weights.xml'.format(args.eta,args.pt)
            output='/xdata/tburch/workspace/photonid/data/evaluated_bdt_binned/evaluated_eta{0}pt{1}_topo_cuts.root'.format(args.eta,args.pt)
            spectators = 7    
        else:
            model_name = '/bdata/tburch/tmva_submit/BDT_eta{0}pt{1}_/weights/BoostedDecisionTree_cuts.weights.xml'.format(args.eta,args.pt)
            output='/xdata/tburch/workspace/photonid/data/evaluated_bdt_binned/evaluated_eta{0}pt{1}_cuts.root'.format(args.eta,args.pt)
            spectators = 7 


print("Using model {0}".format(model_name))

outputFile = ROOT.TFile.Open(output, 'RECREATE')
reader = ROOT.TMVA.Reader()

# Variables
y_Rhad1 = array.array('f', [0])
reader.AddVariable('y_Rhad1',y_Rhad1)
y_Reta = array.array('f', [0])
reader.AddVariable('y_Reta',y_Reta)
y_Rphi = array.array('f', [0])
reader.AddVariable('y_Rphi',y_Rphi)
y_weta1 = array.array('f', [0])
y_weta2 = array.array('f', [0])
reader.AddVariable('y_weta1',y_weta1)
reader.AddVariable('y_weta2',y_weta2)
y_wtots1 = array.array('f', [0])
reader.AddVariable('y_wtots1',y_wtots1)
y_fracs1 = array.array('f', [0])
reader.AddVariable('y_fracs1',y_fracs1)
y_deltae = array.array('f', [0])
reader.AddVariable('y_deltae',y_deltae)
y_Eratio = array.array('f', [0])
reader.AddVariable('y_Eratio',y_Eratio)
if args.topo:
    y_topoCluster0_secondLambda = array.array("f", [0])
    reader.AddVariable('y_topoCluster0_secondLambda', y_topoCluster0_secondLambda)
    y_topoCluster0_secondR = array.array("f", [0])
    reader.AddVariable('y_topoCluster0_secondR', y_topoCluster0_secondR)
    y_nTopoClusters = array.array("f", [0])
    reader.AddVariable('y_nTopoClusters', y_nTopoClusters)
    y_topoCluster0_centerLambda = array.array("f", [0])
    reader.AddVariable('y_topoCluster0_centerLambda', y_topoCluster0_centerLambda)
    y_topoCluster0_emProbability = array.array("f", [0])
    #reader.AddVariable('y_topoCluster0_emProbability', y_topoCluster0_emProbability)
    y_topoCluster0_isolation = array.array("f", [0])
    #reader.AddVariable('y_topoCluster0_isolation', y_topoCluster0_isolation)    


# Spectators
y_ptcone20 = array.array('f', [0])
reader.AddSpectator('y_ptcone20', y_ptcone20)
y_ptcone40 = array.array('f', [0])
reader.AddSpectator('y_ptcone40', y_ptcone40)
y_topoetcone20 = array.array('f', [0])
reader.AddSpectator('y_topoetcone20', y_topoetcone20)
y_topoetcone40 = array.array('f', [0])
reader.AddSpectator('y_topoetcone40', y_topoetcone40)
y_iso_FixedCutLoose = array.array('f', [0])
reader.AddSpectator('y_iso_FixedCutLoose', y_iso_FixedCutLoose)
y_iso_FixedCutTight = array.array('f', [0])
reader.AddSpectator('y_iso_FixedCutTight', y_iso_FixedCutTight)
y_iso_FixedCutTightCaloOnly = array.array('f', [0])
reader.AddSpectator('y_iso_FixedCutTightCaloOnly', y_iso_FixedCutTightCaloOnly)

mcTotWeight = array.array('f', [0])
reweight_SF = array.array('f', [0])
y_pt = array.array('f', [0])

if spectators >8:
    reader.AddSpectator('mcTotWeight',mcTotWeight)
if spectators == 10:
    reader.AddSpectator('reweight_SF', reweight_SF)
if spectators >7:
    reader.AddSpectator('y_pt', y_pt)

if args.cuts:
    reader.BookMVA("Cuts::cuts", model_name)
else:
    reader.BookMVA("BDT::BDT", model_name)

# Build cut
s_cut = 'acceptEventPtBin && '
s_cut += 'y_IsLoose!=0 && '
s_cut += 'y_f1 > 0.005 && '
s_cut += 'y_wtots1 > -10 && '
s_cut += 'y_weta1 > -100 && '
if args.converted:
    s_cut += 'y_convType != 0 && '
else:
    s_cut += 'y_convType == 0 && '
s_cut += 'abs(y_eta) > %s && ' % eta_map[args.eta][0]
s_cut += 'abs(y_eta) < %s && ' % eta_map[args.eta][1]

s_cut += 'y_pt > %s && ' % pt_map[args.pt][0]
s_cut += 'y_pt < %s && ' % pt_map[args.pt][1]
s_cut += 'evt_mu > 1 && '
s_cut += 'evt_mu < 60 '


signal_eval = array.array('f', [0])
bkg_eval = array.array('f', [0])

out_signaltree = signalTree.CloneTree(0)
out_signaltree.SetName("gammajet")
out_signaltree.Branch('bdt_eval', signal_eval, 'bdt_eval/F')

out_backgroundtree = backgroundTree.CloneTree(0)
out_backgroundtree.Branch('bdt_eval', bkg_eval, 'bdt_eval/F')
out_backgroundtree.SetName("jetjet")


# Iterate over valid signal events
signalTree.Draw(">>signalEventList", s_cut+" && y_isTruthMatchedPhoton!=0",'goff')
signalEventList = ROOT.gDirectory.Get("signalEventList")
nEntries = signalEventList.GetN()

print("Starting Signal Tree")
for e in range(0,nEntries):
    entry = signalEventList.GetEntry(e)
    signalTree.GetEntry(entry)

    # Get all Variables
    y_Rhad1[0] = signalTree.y_Rhad1
    y_Reta[0] = signalTree.y_Reta
    y_Rphi[0] = signalTree.y_Rphi
    y_weta2[0] = signalTree.y_weta2
    y_weta1[0] = signalTree.y_weta1
    y_wtots1[0] = signalTree.y_wtots1
    y_fracs1[0] = signalTree.y_fracs1
    y_deltae[0] = signalTree.y_deltae
    y_Eratio[0] = signalTree.y_Eratio
    y_ptcone20[0] = signalTree.y_ptcone20
    y_ptcone40[0] = signalTree.y_ptcone40
    y_topoetcone20[0] = signalTree.y_topoetcone20
    y_topoetcone40[0] = signalTree.y_topoetcone40
    y_iso_FixedCutLoose[0] = signalTree.y_iso_FixedCutLoose
    y_iso_FixedCutTight[0] = signalTree.y_iso_FixedCutTight
    y_iso_FixedCutTightCaloOnly[0] = signalTree.y_iso_FixedCutTightCaloOnly
    y_pt[0] = signalTree.y_pt

    # Evaluate
    if args.cuts:
        bdt_eval = reader.EvaluateMVA('Cuts::cuts',0.85)
    else:
        bdt_eval = reader.EvaluateMVA('BDT::BDT')
    signal_eval[0] = bdt_eval
    out_signaltree.Fill()


# Iterate over valid background events
backgroundTree.Draw(">>backgroundEventList", s_cut+" && y_isTruthMatchedPhoton==0",'goff')
backgroundEventList = ROOT.gDirectory.Get("backgroundEventList")
nEntries = backgroundEventList.GetN()

print("Starting Background Tree")
for e in range(0,nEntries):
    entry = backgroundEventList.GetEntry(e)
    backgroundTree.GetEntry(entry)

    # Get all Variables
    y_Rhad1[0] = backgroundTree.y_Rhad1
    y_Reta[0] = backgroundTree.y_Reta
    y_Rphi[0] = backgroundTree.y_Rphi
    y_weta2[0] = backgroundTree.y_weta2
    y_weta1[0] = backgroundTree.y_weta1
    y_wtots1[0] = backgroundTree.y_wtots1
    y_fracs1[0] = backgroundTree.y_fracs1
    y_deltae[0] = backgroundTree.y_deltae
    y_Eratio[0] = backgroundTree.y_Eratio
    y_ptcone20[0] = backgroundTree.y_ptcone20
    y_ptcone40[0] = backgroundTree.y_ptcone40
    y_topoetcone20[0] = backgroundTree.y_topoetcone20
    y_topoetcone40[0] = backgroundTree.y_topoetcone40
    y_iso_FixedCutLoose[0] = backgroundTree.y_iso_FixedCutLoose
    y_iso_FixedCutTight[0] = backgroundTree.y_iso_FixedCutTight
    y_iso_FixedCutTightCaloOnly[0] = backgroundTree.y_iso_FixedCutTightCaloOnly
    y_pt[0] = backgroundTree.y_pt

    # Evaluate
    if args.cuts:
        bdt_eval = reader.EvaluateMVA('Cuts::cuts',0.85)
    else:
        bdt_eval = reader.EvaluateMVA('BDT::BDT')
    bkg_eval[0] = bdt_eval
    out_backgroundtree.Fill()

outputFile.cd()
print("Writing to file {0}".format(output))
out_signaltree.Write("gammajet",ROOT.TObject.kOverwrite)
out_backgroundtree.Write("jetjet",ROOT.TObject.kOverwrite)
outputFile.Close()