# mimic https://indico.cern.ch/event/725111/contributions/2984861/attachments/1640770/2619953/PIDOptimization_status_20180426.pdf
# This for a baseline test, do with and without topo cluster variables
import argparse
import os
import shutil
import sys
import xml.etree.ElementTree as ET
from collections import namedtuple

import ROOT

# Configurables
parser = argparse.ArgumentParser('BDT')
parser.add_argument('-e', dest='eta', required=True, help='Input SinglePhoton ntuple')
parser.add_argument('-p', dest='pt', required=True, help='Output Collection of TH objects')
parser.add_argument('-t', dest='topo',action='store_true', help='do topo file')
parser.add_argument('-c', dest='converted',action='store_true', help='conversion')
args = parser.parse_args()
if args.eta == 4: sys.exit()

eta_map={
    "0" : (0, 0.6),
    "1" : (0.6, 0.8),
    "2" : (0.8, 1.15),
    "3" : (1.15, 1.37),
    "5" : (1.52, 1.81),
    "6" : (1.81, 2.01),
    "7" : (2.01, 2.37)
}
pt_map={
    "1" : (25,30),
    "2" : (30,40),
    "3" : (40,50),
    "4" : (50,60),
    "5" : (60,80),
    "6" : (80,100),
    "7" : (100,125),
    "8" : (125,150),
    "9" : (150,175),
    "10" : (175,250),
    "11" : (250, 500),
    "12" : (500,1500),
    "high" : (100,1500),
}



# Initalize files, TMVA objects
workdir = '/bdata/tburch/tmva_submit/'
#writedir = workdir+'/models/'
writedir = workdir
#output = writedir+'tmva_output_topoAdded.root'
s_c = 'Unconverted'
if args.converted: s_c = 'Converted'
s_t = 'nominal'
if args.topo: s_t = 'topo'
output = '/bdata/tburch/tmva_submit/'+'BDT_eta%spt%s%s_%s.root' % (args.eta, args.pt, s_c, s_t)
outputFile = ROOT.TFile.Open(output, 'RECREATE')
ROOT.TMVA.UseOffsetMethod = True

# Create Factory Object
factoryOptions = '!V:!Silent:Transformations=I;D;P;G,D:Correlations'
factory = ROOT.TMVA.Factory('BoostedDecisionTree', outputFile, factoryOptions)

variables= ['y_Rhad1','y_Reta','y_Rphi','y_weta1','y_weta2','y_wtots1','y_fracs1','y_deltae','y_Eratio']
mva_variable = namedtuple('mva_variable',['name','lower','upper','index'])        

# Get constraints from existing XML
if str(args.pt) != "high":
    matching_xml = '/bdata/tburch/PhotonIDCutsOptimization/photonIDoptim/build/inputsXML/configEta%sPt%sMu1%s.xml' % (args.eta, args.pt, s_c)
else:
    matching_xml = '/bdata/tburch/PhotonIDCutsOptimization/photonIDoptim/build/inputsXML/configEta%sPt6Mu1%s.xml' % (args.eta, s_c)
xmltext = open(matching_xml).readlines()

ss_variables =[]
for i in range(len(variables)):
    variable_name = variables[i]
    low,high = -999, 999
    for line in xmltext:
        if 'CutRangeMin[%i]' % i in line:
            low = float(line.split('=')[1].split('</OPTION')[0])
        elif 'CutRangeMax[%i]' % i in line:
            high = float(line.split('=')[1].split('</OPTION')[0])
    
    ss_variables.append(mva_variable(name=variable_name, lower=low, upper=high, index=i))

for s in ss_variables:
    print "tjb Variable check"
    print "     var: ", s.name, " upper: ", s.upper, " lower: ", s.lower

# Create Dataloader
dataloader = ROOT.TMVA.DataLoader('SinglePhoton')

range_string=''
# Handle shower shape variables
for v in ss_variables:
    dataloader.AddVariable(v.name,'F')
    range_string +=  ':CutRangeMin[%d]=%f:CutRangeMax[%d]=%f' % (v.index, v.lower, v.index, v.upper)


# Other Variables for correlation
dataloader.AddSpectator('y_ptcone20','F')
dataloader.AddSpectator('y_ptcone40','F')
dataloader.AddSpectator('y_topoetcone20','F')
dataloader.AddSpectator('y_topoetcone40','F')
dataloader.AddSpectator('y_iso_FixedCutLoose','F')
dataloader.AddSpectator('y_iso_FixedCutTight','F')
dataloader.AddSpectator('y_iso_FixedCutTightCaloOnly','F')
dataloader.AddSpectator('mcTotWeight','F')
dataloader.AddSpectator('y_pt', 'F')

# new
if args.topo:
    dataloader.AddVariable('y_topoCluster0_secondLambda', 'F')
    dataloader.AddVariable('y_topoCluster0_secondR', 'F')
    dataloader.AddVariable('y_nTopoClusters', 'I')
    dataloader.AddVariable('y_topoCluster0_centerLambda', 'F')
    dataloader.AddVariable('y_topoCluster0_emProbability', 'F')
    dataloader.AddVariable('y_topoCluster0_isolation', 'F')



# Get all trees
#data_dir = workdir+'/ntuples/'
data_dir = workdir
ttree_file = ROOT.TFile(data_dir+'/TMVA_ready_withSF_traintest.root')
signalTree_train = ttree_file.Get('GammaJet_train')
backgroundTree_train = ttree_file.Get('JetJet_train')
signalTree_test = ttree_file.Get('GammaJet_test')
backgroundTree_test = ttree_file.Get('JetJet_test')

# Set Weights TODO
# ---------------------------------------------------------------
# luminosity = 361000
dataloader.SetWeightExpression('mcTotWeight')

# Prep Dataset
# ------------------------------------------------------------------

# Build cut
s_cut = 'acceptEventPtBin && '
s_cut += 'y_IsLoose!=0 && '
s_cut += 'y_f1 > 0.005 && '
s_cut += 'y_wtots1 > -10 && '
s_cut += 'y_weta1 > -100 && '
if args.converted:
    s_cut += 'y_convType != 0 && '
else:
    s_cut += 'y_convType == 0 && '
s_cut += 'abs(y_eta) > %s && ' % eta_map[args.eta][0]
s_cut += 'abs(y_eta) < %s && ' % eta_map[args.eta][1]
s_cut += 'y_pt > %s && ' % pt_map[args.pt][0]
s_cut += 'y_pt < %s && ' % pt_map[args.pt][1]
s_cut += 'evt_mu > 1 && '
s_cut += 'evt_mu < 60 '

preselection_cut = ROOT.TCut(s_cut)
split_options = 'nTrain_Signal=0:'  # all signal
split_options += 'nTrain_Background=0:'  # all background
split_options += 'NormMode=NumEvents:'  # Normalization of weights
#split_options += 'CreateMVAPdfs=True:'
split_options += 'SplitMode=Random:'
split_options += 'EqualNumEvents:'

sigcut = ROOT.TCut(s_cut + ' && y_isTruthMatchedPhoton!=0')
bkgcut = ROOT.TCut(s_cut + ' && y_isTruthMatchedPhoton==0')

#  Load in and prepare trees
dataloader.AddSignalTree(signalTree_train, 1.0, "train")
dataloader.AddBackgroundTree(backgroundTree_train, 1.0, "train")
dataloader.AddSignalTree(signalTree_test, 1.0, "test")
dataloader.AddBackgroundTree(backgroundTree_test, 1.0, "test")

dataloader.PrepareTrainingAndTestTree(sigcut, bkgcut, split_options)

# MVA Settings
# ------------------------------------------------------------------

# BDT (adaboost)
bdt_opts = 'NegWeightTreatment=InverseBoostNegWeights:'
bdt_opts += 'BoostType=Grad:'
bdt_opts += 'NTrees=800'
bdt = factory.BookMethod(dataloader, ROOT.TMVA.Types.kBDT, 'BDT', bdt_opts)

# Rectangular Cuts
cuts_options = 'H:V:'
cuts_options += 'FitMethod=GA:'
cuts_options += 'VarProp=FSmart:'
cuts_options += 'EffSel:'
cuts_options += 'CreateMVAPdfs:'
cuts_options += 'Popsize=500:'
cuts_options += 'Steps=20:'
cuts_options += 'Cycles=6'
cuts_options += range_string

if args.pt != "high":
    rect = factory.BookMethod(dataloader, ROOT.TMVA.Types.kCuts, 'cuts', cuts_options)


# Train, Test, Evaluate all methods
# ------------------------------------------------------------------
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()

# Clean up

outputFile.Close()
#writedir = workdir+'/models/'
print cuts_options

#os.rename('SinglePhoton', '/bdata/tburch/BDT_eta%spt%s%s_%s'% (args.eta, args.pt, s_c, s_t) ) 
#shutil.move('bdt1', writedir)
