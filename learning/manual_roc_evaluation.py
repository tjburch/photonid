import ROOT
import sys
import numpy as np
import glob 
import matplotlib.pyplot as plt
plot=False
import argparse

parser = argparse.ArgumentParser("Give pT and eta")
parser.add_argument('--converted', dest='converted', action='store_true', help='Do converted photons')
parser.add_argument('--topo', dest='topo', action='store_true', help='Do topo files')
parser.add_argument('--inclusive', dest='inclusive', action='store_true', help='Do inclusive topo files')
parser.add_argument('--reweight', dest='reweight', action='store_true', help='Dont reweight pT distribution')
parser.add_argument('--high-pt', dest='highModel', action='store_true', help='Use High pT model')
parser.add_argument('--cuts', dest='cuts', action='store_true', help='Use High pT model')
args = parser.parse_args()
print(args)

def get_background_rejection(filename):
    print("Now doing %s" % filename)

    f_input = ROOT.TFile(filename)
    t_signal = f_input.Get("gammajet")
    t_background = f_input.Get("jetjet")

    bins = 500
    min_val = -1
    max_val = 1

    h_signal_eval = ROOT.TH1F(
        "h_signal_eval","h_signal_eval",bins, min_val, max_val
        )
    h_background_eval = ROOT.TH1F(
        "h_background_eval","h_background_eval",bins, min_val, max_val
        )
    try: 
        t_signal.Draw("bdt_eval>>h_signal_eval","mcTotWeight","goff")
        t_background.Draw("bdt_eval>>h_background_eval","mcTotWeight","goff")
    except TypeError:
        print("   DID NOT CONVERGE")
        return (0,0)

    # Iterate over background histogram, save rejection value at each step
    full_value = h_signal_eval.Integral()
    full_bkg_value = h_background_eval.Integral()

    maxbin = h_signal_eval.GetXaxis().FindBin(max_val)
    signal_efficiency, bkg_rej = {}, {}
    for cutval in np.linspace(start=min_val, stop=max_val, num=bins):

        cutbin = h_signal_eval.GetXaxis().FindBin(cutval)
        this_integral_value = h_signal_eval.Integral(cutbin,maxbin)
        try:
            signal_efficiency.update({cutbin : (this_integral_value)/full_value})
            this_bkg_integral_value =  h_background_eval.Integral(cutbin,maxbin)
            bkg_rej.update({cutbin : 1-(this_bkg_integral_value)/full_bkg_value})
        except ZeroDivisionError:
            bkg_rej.update({cutbin : float('nan')})

    try:
        closest_signal_efficiency = min(signal_efficiency.values(), key=lambda x:abs(x-0.85))
    except ValueError:
        return (0,0)

    for key in signal_efficiency:
        if signal_efficiency[key] == closest_signal_efficiency:
            bin_of_interest = key

    if plot:
        plt.scatter(x=signal_efficiency.values(), y=bkg_rej.values())
        plt.xlabel("signal efficiency")
        plt.ylabel("Background rejection")
        plt.show()
        plt.close()

    return round(signal_efficiency[bin_of_interest],3), round(bkg_rej[bin_of_interest],3)
    

def get_discrete_background_rejection(filename):
    """ This method works for the cuts method, where the values are just 0 or 1 """

    print("Now doing %s" % filename)

    f_input = ROOT.TFile(filename)
    t_signal = f_input.Get("gammajet")
    t_background = f_input.Get("jetjet")

    bins = 4
    min_val = 0
    max_val = 1.1

    h_signal_eval = ROOT.TH1F(
        "h_signal_eval","h_signal_eval",bins, min_val, max_val
        )
    h_background_eval = ROOT.TH1F(
        "h_background_eval","h_background_eval",bins, min_val, max_val
        )
    try: 
        # TODO : Fix these names in the pipeline, low priority
        t_signal.Draw("bdt_eval>>h_signal_eval","mcTotWeight","goff")
        t_background.Draw("bdt_eval>>h_background_eval","mcTotWeight","goff")
    except TypeError:
        print("   DID NOT CONVERGE")
        return (0,0)

    # Get background rejection of evaluated cuts
    full_background_value = h_background_eval.Integral()
    background_efficiency = h_background_eval.Integral(h_background_eval.FindBin(0.5), h_background_eval.FindBin(1)) / (full_background_value)
    
    background_rejection = round(1 - background_efficiency, 3)
    background_rejection_validation = round( h_background_eval.Integral(h_background_eval.FindBin(0), h_background_eval.FindBin(0.5)) / full_background_value, 3)
    # Check value
    if abs(background_rejection - background_rejection_validation) > 0.01 :
        raise ValueError("Mismatch in Background rejection. Solution A: {0} solution B: {1}".format(background_rejection, background_rejection_validation))

    # By definition the 85% signal efficiency point is used, so this should be near 85% but 
    full_signal_value = h_signal_eval.Integral()
    signal_efficiency = round( (h_signal_eval.Integral(h_signal_eval.FindBin(0.5), h_signal_eval.FindBin(1))) / (full_signal_value) , 3 )


    return signal_efficiency, background_rejection




if __name__ == "__main__":
    
    splitstring =""

    if args.converted:
        if args.cuts:
            if args.topo:
                csvfile = 'data/efficiency_w_cuts_topo_conv.csv'
                rootfiles = glob.glob('data/evaluated_bdt_binned/evaluated_eta*pt*_topo_cuts_conv.root')        
                splitstring = "_topo_cuts_conv"
            else:
                csvfile = 'data/efficiency_w_cuts_conv.csv'
                rootfiles = glob.glob('data/evaluated_bdt_binned/evaluated_eta*pt*_cuts_conv.root')        
                splitstring = "_cuts_conv"

    elif args.cuts:
        if args.topo:
            csvfile = 'data/efficiency_w_cuts_topo.csv'
            rootfiles = glob.glob('data/evaluated_bdt_binned/evaluated_eta*pt*_topo_cuts.root')        
            splitstring = "_topo_cuts"
        else:
            csvfile = 'data/efficiency_w_cuts.csv'
            rootfiles = glob.glob('data/evaluated_bdt_binned/evaluated_eta*pt*_cuts.root')        
            splitstring = "_cuts"

    elif args.reweight:
        csvfile = 'data/efficiency_w_ptinclusive_bdt_reweight.csv'
        rootfiles = glob.glob('data/evaluated_bdt_reweight/evaluated_eta*pt*_topo_reweight.root')
        splitstring = "_topo_reweight"
    elif args.highModel:
        csvfile = 'data/efficiency_w_highpT_model.csv'
        rootfiles = glob.glob('data/evaluated_bdt/evaluated_eta*pt*HighModel.root')
    elif args.topo and args.inclusive:
        csvfile = 'data/efficiency_w_inclusiveTopo.csv'
        rootfiles = glob.glob('data/evaluated_bdt_inclusive/evaluated_eta*pt*_topo_inclusiveModel.root')
        splitstring = "_topo_inclusiveModel"
    elif args.inclusive:
        csvfile = 'data/efficiency_w_inclusive.csv'
        rootfiles = glob.glob('data/evaluated_bdt_inclusive/evaluated_eta*pt*_inclusiveModel.root')
        rootfiles = [f for f in rootfiles if "topo" not in f]
        splitstring = "_inclusiveModel"
    elif args.topo:
        csvfile = 'data/efficiency_w_topo.csv'
        rootfiles = glob.glob('data/evaluated_bdt_binned/evaluated_eta*pt*topo_binnedModel.root')        
        splitstring = "_topo_binnedModel"

    else:
        csvfile = 'data/efficiency_w_bdt.csv'
        rootfiles = glob.glob('data/evaluated_bdt_binned/evaluated_eta*pt*_binnedModel.root')
        splitstring = "_binnedModel"
        rootfiles = [f for f in rootfiles if "unreweighted" not in f]
        rootfiles = [f for f in rootfiles if "topo" not in f]

    with open(csvfile,'w+') as f_output:
        f_output.write("eta,pt,signal_efficiency,background_rejection\n")
        for rootfile in rootfiles:
            if args.cuts:
                this_sig_eff, this_bkg_rej = get_discrete_background_rejection(rootfile)
            else:
                this_sig_eff, this_bkg_rej = get_background_rejection(rootfile)
            
            eta = rootfile.split('eta')[1].split('pt')[0]
            pt = rootfile.split('pt')[1].split('.root')[0]
            if "HighModel" in pt:
                pt = pt.replace("HighModel","")
            if "unreweighted" in pt:
                pt = pt.split("_")[0]
            pt = pt.replace(splitstring,"")

            f_output.write('{0},{1},{2},{3}\n'.format(eta,pt,this_sig_eff,this_bkg_rej))
