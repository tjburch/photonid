import h5py
import argparse
import numpy as np
from keras.models import load_model
from sklearn.metrics import roc_curve
import logging
logging.basicConfig(level=logging.INFO, format='%(message)s')

parser = argparse.ArgumentParser("Give pT and eta")
parser.add_argument("-e", dest="eta", required=True, help="eta bin")
parser.add_argument("-p", dest="pt", required=True, help="pT bin")
parser.add_argument("-u", dest="unreweighted", action="store_true", help="pT bin")
args = parser.parse_args()

# Load Datasets
# Load file, pre split in eta
filename = "test_sets/testing_eta{0}.h5".format(args.eta)
logging.debug("Loading file {0}".format(filename))
dataset_file = h5py.File(filename, "r")
logging.debug("File loaded with the following datasets:")
for i in dataset_file:
    logging.debug(i)
X_test = dataset_file["X_test"][:]
y_test = dataset_file["y_test"][:]
w_test = dataset_file["w_test"][:]
dataset_file.close()
X_test.shape
# Print stats
logging.debug("Loaded array with dimensions {0}".format(X_test.shape))

# Slim those large files down to just the indicated pT range
pt_map = {
    "1": (25, 30),
    "2": (30, 40),
    "3": (40, 50),
    "4": (50, 60),
    "5": (60, 80),
    "6": (80, 100),
    "7": (100, 125),
    "8": (125, 150),
    "9": (150, 175),
    "10": (175, 250),
    "11": (250, 500),
    "12": (500, 1500),
}
logging.debug(
    "Skimming Down to pT range {0} < pT < {1}".format(
        pt_map[args.pt][0], pt_map[args.pt][1]
    )
)


valid_rows = (X_test[:, -1] > pt_map[args.pt][0]) & (X_test[:, -1] < pt_map[args.pt][1])
X_test = X_test[valid_rows]
y_test = y_test[valid_rows]
w_test = w_test[valid_rows]

# Drop off those pesky pT rows
X_test = np.delete(X_test, -1, axis=1)

# How are we doing now?
logging.debug("After pT cuts and slimming to features-only, shape: {0}".format(X_test.shape))

# Neato, now let's Load the model
if args.unreweighted:
    model = load_model("inclusive_models_unreweighted/eta{0}_sequential.h5".format(args.eta))
else:
    model = load_model("inclusive_models_reweighted/eta{0}_sequential.h5".format(args.eta))

# Now let's FIT. THAT. DATA.
y_predict = model.predict(X_test).ravel()

# Stop, evaluate and listen.
fpr, tpr, thresholds = roc_curve(y_test, y_predict)


def find_nearest_idx(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


i = find_nearest_idx(tpr, 0.85)
bkg_rejection = 1 - fpr
logging.debug(
    "At {0:.3} Signal Efficiency, {1:.3} Background Rejection".format(
        tpr[i], bkg_rejection[i]
    )
)
print("{0},{1},{2:.3},{3:.3}".format(args.eta,args.pt,tpr[i],bkg_rejection[i]))
