import h5py
import argparse
import numpy as np
from keras.models import load_model
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import logging; logging.basicConfig(level=logging.INFO)
from scipy.stats import kde
#import seaborn as sns
from root_numpy import fill_profile
import ROOT

parser = argparse.ArgumentParser("Give pT and eta")
parser.add_argument("-e", dest="eta", required=True, help="eta bin")
parser.add_argument("-u", dest="unreweighted", action="store_true", help="eta bin")
args = parser.parse_args()

if args.unreweighted:
    model_dir = "inclusive_models_unreweighted/"
    output_dir = 'plots_unreweighted/'
else:
    model_dir = "inclusive_models_reweighted/"
    output_dir = 'plots_reweighted/'

# Load Datasets
# Load file, pre-split in eta
filename = "test_sets/testing_eta{0}.h5".format(args.eta)
logging.debug("Loading file {0}".format(filename))
dataset_file = h5py.File(filename, "r")
logging.debug("File loaded with the following datasets:")

for i in dataset_file:
    logging.debug(i)
X_test = dataset_file["X_test"][:]
y_test = dataset_file["y_test"][:]
w_test = dataset_file["w_test"][:]
dataset_file.close()

# Print stats
logging.debug("Loaded array with dimensions {0}".format(X_test.shape))

# Load the model
model = load_model(model_dir+"/eta{0}_sequential.h5".format(args.eta))

# Get pT values then drop
pt_values = X_test[:, -1]
X_test = np.delete(X_test, -1, axis=1)

# Get scores
nn_scores = model.predict(X_test).ravel()
true_photon_pt = pt_values[np.logical_and(y_test == 1, pt_values < 100)]
true_photon_scores = nn_scores[np.logical_and(y_test == 1, pt_values < 100)]
true_weights = w_test[np.logical_and(y_test == 1, pt_values < 100)]

fake_photon_pt = pt_values[np.logical_and(y_test == 0, pt_values < 100)]
fake_photon_scores = nn_scores[np.logical_and(y_test == 0, pt_values < 100)]
fake_weights = w_test[np.logical_and(y_test == 0, pt_values < 100)]


def default_formatting():
    plt.ylim(bottom=0, top=1)
    plt.xlabel("$pT_{\gamma}$")
    plt.ylabel("NN Score")
    plt.tight_layout()

def doHistogram(nbins=40):
    # Plot 2d histogram
    fig = plt.figure()
    h = plt.hist2d(x=true_photon_pt, y=true_photon_scores, bins=40,norm=LogNorm())
    plt.colorbar(h[3])

    # Aesthetics 
    plt.ylim(top=1)
    plt.xlabel("$\gamma_{pT}$")
    plt.ylabel("NN Score")

    plt.tight_layout()
    plt.savefig(output_dir+'/pt_vs_nnscore_hist_eta{0}'.format(args.eta))
    plt.close()

    # Plot 2d histogram
    fig = plt.figure()
    plt.scatter(x=true_photon_pt, y=true_photon_scores, marker='.')
    default_formatting()
    plt.savefig(output_dir+'/pt_vs_nnscore_scat_eta{0}'.format(args.eta))
    plt.close()

    fig = plt.figure()

def doContour(nbins=40):
    def plot_contour(x,y,**kwargs):
        k = kde.gaussian_kde((x,y))
        xi, yi = np.mgrid[x.min():x.max():nbins*1j, y.min():y.max():nbins*1j]
        zi = k(np.vstack([xi.flatten(), yi.flatten()]))
        ax = plt.gca()
        ax.pcolormesh(xi, yi, zi.reshape(xi.shape), shading='gouraud', **kwargs)
        ax.contour(xi, yi, zi.reshape(xi.shape) )

    plot_contour(true_photon_pt, true_photon_scores, cmap=plt.cm.BuGn_r)
    default_formatting()
    plt.savefig(output_dir+'/contour_true_eta{0}'.format(args.eta))
    plt.close()

    plot_contour(fake_photon_pt, fake_photon_scores, cmap=plt.cm.RdPu_r)
    default_formatting()
    plt.savefig(output_dir+'/contour_fake_eta{0}'.format(args.eta))
    plt.close()

def doProfileROOT():
    tprofile_signal = ROOT.TProfile("signal","signal", 10, 0, 100,0,1)
    tprofile_background = ROOT.TProfile("background","background", 10, 0, 100,0,1)

    fill_profile(tprofile_signal, np.column_stack([true_photon_pt, true_photon_scores]), weights=true_weights)
    fill_profile(tprofile_background, np.column_stack([fake_photon_pt, fake_photon_scores]), weights=fake_weights)

    # Drawing
    c1 = ROOT.TCanvas()
    tprofile_signal.Draw()
    tprofile_signal.SetLineColor(ROOT.kBlue)
    tprofile_signal.SetTitle(";pT_{#gamma};NN Score")
    tprofile_signal.GetYaxis().SetRangeUser(0,1)
    ROOT.gStyle.SetOptStat(00000000)
    tprofile_background.Draw("sames")
    tprofile_background.SetLineColor(ROOT.kRed)
    l = ROOT.TLegend(0.125,0.125,0.4,0.25)
    l.AddEntry(tprofile_signal,"Signal (truth-matched photons)","le")
    l.AddEntry(tprofile_background,"Bkg (truth-vetoed jet-fakes)","le")
    l.Draw()
    c1.SaveAs(output_dir+"/profile_eta{0}.pdf".format(args.eta))



def doProfileSNS():
    # This method doesn't incorporate the weights
    # Deprecate, replace with ROOT version
    def get_points(ax):
        lines = ax.lines
        x = [l.get_xdata().mean() for l in lines]
        y = [l.get_ydata().mean() for l in lines]
        y_std = [l.get_ydata().std() for l in lines]
        return x[:-1], y[:-1], y_std[:-1] # For some reason this gets the entire mean as well, so drop those

    # Generate points
    ax_true = sns.regplot(x=true_photon_pt, y=true_photon_scores, x_estimator=np.mean, x_bins=np.arange(0,100,10))
    t_x, t_y, t_e  = get_points(ax_true) 
    plt.close()
    ax_false = sns.regplot(x=fake_photon_pt, y=fake_photon_scores, x_estimator=np.mean, x_bins=np.arange(0,100,10))
    f_x, f_y, f_e  = get_points(ax_false)
    plt.close()

    # Plot all this
    linewidth=1
    plt.errorbar(x=t_x, y=t_y, xerr=5, yerr=t_e, color='cornflowerblue',linestyle='none', label='Signal (truth-matched photons)',elinewidth=linewidth)
    plt.errorbar(x=f_x, y=f_y,  xerr=5, yerr=f_e, color='firebrick',linestyle='none', label='Bkg (truth-vetoed jet-fakes)',elinewidth=linewidth)

    # Style
    plt.ylim(bottom=0, top=1)
    plt.xlabel("$pT_{\gamma}$")
    plt.ylabel("NN Score")

    eta_map={
        "0" : (0, 0.6),
        "1" : (0.6, 0.8),
        "2" : (0.8, 1.15),
        "3" : (1.15, 1.37),
        "5" : (1.52, 1.81),
        "6" : (1.81, 2.01),
        "7" : (2.01, 2.37)
    }
    eta_label = "${0}<|\eta|<{1}$".format(eta_map[args.eta][0], eta_map[args.eta][1])
    plt.annotate(eta_label, xy=(0.02,0.02),xycoords='axes fraction', horizontalalignment='left',fontsize=14)
    plt.legend(frameon=False)
    plt.tight_layout()
    plt.savefig(output_dir+'/regplot_eta{0}'.format(args.eta))
    plt.close()

def main():
    #doHistogram()
    #doContour()
    doProfileROOT()
    #doProfileSNS

if __name__ == "__main__":
    main()