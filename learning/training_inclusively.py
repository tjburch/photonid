"""
Script to triain inclusively in pT and save the model after usage
"""
print("Loading Libraries...")
# General
import h5py
import argparse
from time import time

# Computing
import numpy as np

# Agg backend forces no display -necessary for slurm
import matplotlib

matplotlib.use("Agg")
# Plotting
import matplotlib.pyplot as plt

# ML
from sklearn.metrics import auc, roc_curve
from sklearn.model_selection import train_test_split
from keras.models import Model
from keras.layers import Dense, Activation, Dropout
from keras.utils.vis_utils import plot_model
from keras.callbacks import TensorBoard, EarlyStopping
from keras.utils import normalize
from keras.engine.input_layer import Input

# Turn off annoying future warnings from Keras
import warnings

warnings.simplefilter(action="ignore", category=FutureWarning)

# Local Tools
from utils import reweight

# Existing Variables
eta_map = {
    "0": (0, 0.6),
    "1": (0.6, 0.8),
    "2": (0.8, 1.15),
    "3": (1.15, 1.37),
    "5": (1.52, 1.81),
    "6": (1.81, 2.01),
    "7": (2.01, 2.37),
}


# Configurables
parser = argparse.ArgumentParser("NN")
parser.add_argument("-e", dest="eta", required=True, help="Input SinglePhoton ntuple")
parser.add_argument("-t", dest="topo", action="store_true", help="do topo file")
parser.add_argument("-c", dest="converted", action="store_true", help="conversion")
parser.add_argument('--no-reweight', dest='noReweight',action='store_true', help='Dont reweight pT distribution')
args = parser.parse_args()
if args.converted:
    c_string = "Converted"
else:
    c_string = "Unconverted"
if args.topo:
    topo_string = "topo"
else:
    topo_string = "nominal"

print("input args: ", args)

# Load datasets
# -------------------------------------------------------------------------------
print("Loading Files...")
f_gammajet = h5py.File(
    "/blues/gpfs/group/3/ATLAS/PhotonID/singlephoton/h5/preselection_gammajet_mc16a.h5",
    "r",
)
np_arr_gammajet_0 = f_gammajet.get("SinglePhoton")
f_jetjet = h5py.File(
    "/blues/gpfs/group/3/ATLAS/PhotonID/singlephoton/h5/preselection_jetjet_mc16a.h5",
    "r",
)
np_arr_jetjet_0 = f_jetjet.get("SinglePhoton")
print("Done!")
print("Initial Signal Events: ", np_arr_gammajet_0.shape[0])
print("Initial Background Events: ", np_arr_jetjet_0.shape[0])

# Apply the preselection
# -------------------------------------------------------------------------------
def apply_preselection(array, isSignal):
    boolean_array = (
        (array["y_IsLoose"] != 0)
        * (array["acceptEventPtBin"] == True)
        * (array["y_f1"] > 0.005)
        * (array["y_wtots1"] > -10)
        * (array["y_weta1"] > -100)
    )
    # (np.absolute(array["mcTotWeight"]) < 10000))
    if isSignal:
        return array[(array["y_isTruthMatchedPhoton"] == 1) * boolean_array]
    else:
        return array[(array["y_isTruthMatchedPhoton"] == 0) * boolean_array]


np_arr_gammajet_0 = apply_preselection(np_arr_gammajet_0, isSignal=True)
np_arr_jetjet_0 = apply_preselection(np_arr_jetjet_0, isSignal=False)

print("Signal Events after preselection: ", np_arr_gammajet_0.shape[0])
print("Background Events after preselection: ", np_arr_jetjet_0.shape[0])

# Do slicing. TODO: Make this cleaner.
print(
    "Skimming to bin %.2f < |eta| < %.2f, pT Inclusive, %s Photons"
    % (eta_map[args.eta][0], eta_map[args.eta][1], c_string)
)

if args.converted:
    np_arr_gammajet_0 = np_arr_gammajet_0[np_arr_gammajet_0["y_convType"] != 0]
    np_arr_jetjet_0 = np_arr_jetjet_0[np_arr_jetjet_0["y_convType"] != 0]
else:
    np_arr_gammajet_0 = np_arr_gammajet_0[np_arr_gammajet_0["y_convType"] == 0]
    np_arr_jetjet_0 = np_arr_jetjet_0[np_arr_jetjet_0["y_convType"] == 0]

# Skim down to reasonable pT
np_arr_gammajet_0 = np_arr_gammajet_0[np_arr_gammajet_0["y_pt"] < 1000]
np_arr_jetjet_0 = np_arr_jetjet_0[np_arr_jetjet_0["y_pt"] < 1000]


name_string = "eta%s%s_%s_weight" % (args.eta, c_string, topo_string)

np_arr_gammajet = np_arr_gammajet_0[
    (np.abs(np_arr_gammajet_0["y_eta"]) > eta_map[args.eta][0])
    & (np.abs(np_arr_gammajet_0["y_eta"]) < eta_map[args.eta][1])
]
np_arr_jetjet = np_arr_jetjet_0[
    (np.abs(np_arr_jetjet_0["y_eta"]) > eta_map[args.eta][0])
    & (np.abs(np_arr_jetjet_0["y_eta"]) < eta_map[args.eta][1])
]

print("Signal Events in Bin: ", np_arr_gammajet.shape[0])
print("Background Events in Bin: ", np_arr_jetjet.shape[0])


def shape_array(arr, add_topo):
    ss_variables = [
        "y_Rhad1",
        "y_Reta",
        "y_Rphi",
        "y_weta1",
        "y_weta2",
        "y_wtots1",
        "y_fracs1",
        "y_deltae",
        "y_Eratio",
    ]
    topo_variables = [
        "y_topoCluster0_secondLambda",
        "y_topoCluster0_secondR",
        "y_topoCluster0_centerLambda",
        "y_topoCluster0_emProbability",
        "y_topoCluster0_isolation",
    ]
    # Do shower shape
    ss_arr = arr[:][ss_variables]
    print(len(ss_arr.dtype))
    return_arr = ss_arr.view(np.float32).reshape(ss_arr.shape + (-1,))
    if add_topo:
        n_topo = arr[:]["y_nTopoClusters"]  # this is i32
        n_topo = n_topo.view(np.float32).reshape(n_topo.shape + (-1,))
        other_topo = arr[:][topo_variables]  # these are 64
        other_topo = other_topo.view(np.float64).reshape(other_topo.shape + (-1,))
        return_arr = np.concatenate((return_arr, n_topo), axis=1)

    # Add back in pT temporarily
    pt = arr["y_pt"]
    return_arr = np.concatenate([return_arr, pt[:, None]], axis=1)
    return return_arr


print("Extracting Variables and Reshaping")
# Build Weight expression
gammajet_weight = np.absolute(np_arr_gammajet["mcTotWeight"])  # Weight from sample
gammajet_weight = gammajet_weight * (
    gammajet_weight.shape[0] / gammajet_weight.sum()  # same no. signal/bkg events
)

rescale_weights = reweight(
    base_data=np_arr_gammajet["y_pt"],
    reweight_data=np_arr_jetjet["y_pt"],
    base_weights=np.absolute(np_arr_gammajet['mcTotWeight']),
    rw_weights=np.absolute(np_arr_jetjet['mcTotWeight']),
    binWidth=5
)

np_arr_gammajet = shape_array(np_arr_gammajet, args.topo)  # Reshape

if args.noReweight:
    jetjet_weight = np.absolute(np_arr_jetjet["mcTotWeight"])
else:
    jetjet_weight = np.absolute(np_arr_jetjet["mcTotWeight"]) * rescale_weights

jetjet_weight = jetjet_weight * (gammajet_weight.shape[0] / jetjet_weight.sum())
np_arr_jetjet = shape_array(np_arr_jetjet, args.topo)

# Split into train/test.
print("Splitting into train/test samples.")
X_train_signal, X_test_signal, y_train_signal, y_test_signal, w_train_signal, w_test_signal = train_test_split(
    np_arr_gammajet, np.ones(np_arr_gammajet.shape[0]), gammajet_weight, test_size=0.5
)
X_train_bkg, X_test_bkg, y_train_bkg, y_test_bkg, w_train_bkg, w_test_bkg = train_test_split(
    np_arr_jetjet, np.zeros(np_arr_jetjet.shape[0]), jetjet_weight, test_size=0.5
)

# Renormalize, emulating logic here: https://root.cern.ch/doc/v610/DataSetFactory_8cxx_source.html#1530
w_train_signal = w_train_signal * (w_train_signal.shape[0] / w_train_signal.sum())
w_train_bkg = w_train_bkg * (w_train_signal.shape[0] / w_train_bkg.sum())

# Join with S/B labels
print("Joining signal and background, generating y")
X_train = np.concatenate((X_train_signal, X_train_bkg))
X_test = np.concatenate((X_test_signal, X_test_bkg))
y_train = np.concatenate((y_train_signal, y_train_bkg))
y_test = np.concatenate((y_test_signal, y_test_bkg))
w_train = np.concatenate((w_train_signal, w_train_bkg))
w_test = np.concatenate((w_test_signal, w_test_bkg))

# Copy and save datasets for later application
h5f = h5py.File("test_sets/testing_eta{0}.h5".format(args.eta), "w")
h5f.create_dataset("X_test", data=X_test)
h5f.create_dataset("y_test", data=y_test)
h5f.create_dataset("w_test", data=w_test)
h5f.close()
# Copy and save datasets for later application
h5f = h5py.File("test_sets/training_eta{0}.h5".format(args.eta), "w")
h5f.create_dataset("X_test", data=X_train)
h5f.create_dataset("y_test", data=y_train)
h5f.create_dataset("w_test", data=w_train)
h5f.close()

# Drop off pT for training
X_train_features = np.delete(X_train, -1, axis=1)
X_test_features = np.delete(X_test, -1, axis=1)


# Build Model
"""
For now, really simple model.
Input dim = 9 - one for each shower shape variable TODO: generalize
3-Layer fully connected network (each Dense), uniform weights
Layer 1 & 2 are relu, output layer is sigmoid
# TODO: better accuracy metric?
# TODO: play with epoch, batch size
"""
print("shape: ", X_train_features.shape)
print("Building model")



def build_model():
    # Initate Tensor
    inputs = Input(shape=(X_train_features.shape[1],))
    # Add layers
    hidden = Dense(6, activation="relu")(inputs)
    hidden = Dropout(0.2)(hidden)
    # Add second layer - equivalent without
    #hidden = Dense(4, activation="relu")(hidden)
    #hidden = Dropout(0.2)(hidden)

    # Outputs
    outputs = Dense(1, activation="sigmoid")(hidden)
    model = Model(inputs,outputs)
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=["accuracy"])
    model.summary()
    return(model)


model = build_model()
plot_model(model, to_file="model.png", show_shapes=True, show_layer_names=True)

# Define callbacks
tensorboard = TensorBoard(log_dir="logs/{}".format(time()))
earlystopping = EarlyStopping(
    monitor="val_loss",
    min_delta=0,
    patience=25,
    mode="auto",
    restore_best_weights=True,
)

# Run model
history = model.fit(
    X_train_features,
    y_train,
    sample_weight=w_train,
    epochs=1000,
    batch_size=500,
    validation_data=(X_test_features, y_test, w_test),
    callbacks=[tensorboard, earlystopping],
)

if args.noReweight:
    output_dir = "inclusive_models_unreweighted/"
else:
    output_dir = "inclusive_models_reweighted/"

model.save(output_dir+"/eta{0}_sequential.h5".format(args.eta))

# Plot training
plt.plot(history.history["acc"])
plt.plot(history.history["val_acc"])
plt.title("model accuracy")
plt.ylabel("accuracy")
plt.xlabel("epoch")
plt.legend(["train", "test"], loc="upper left")
plt.savefig(output_dir+"/plots/accuracy_%s.pdf" % name_string)
plt.close()

# summarize history for loss
plt.plot(history.history["loss"])
plt.plot(history.history["val_loss"])
plt.title("model loss")
plt.ylabel("loss")
plt.xlabel("epoch")
plt.legend(["train", "test"], loc="upper left")
plt.savefig(output_dir+"/plots/loss_%s.pdf" % name_string)
plt.close()


# Evaluate
print("Trained. Evaluating...")
scores = model.evaluate(X_test_features, y_test, sample_weight=w_test)
print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))


# Look at fpr/tpr, ROC curves
y_pred_keras = model.predict(X_test_features).ravel()
fpr_keras, tpr_keras, thresholds_keras = roc_curve(y_test, y_pred_keras)
auc_keras = auc(fpr_keras, tpr_keras)


def find_nearest_idx(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


i = find_nearest_idx(tpr_keras, 0.85)
bkg_rejection = 1 - fpr_keras
eval = "At %.3f Signal Efficiency, %.3f Background Rejection" % (
    tpr_keras[i],
    bkg_rejection[i],
)
print(eval)

# Make ROC plots
print("Making ROC Plots")
plt.figure(1)
plt.plot([1, 0], [0, 1], "k--")
plt.axvline(0.85, linestyle="--", color="g")
plt.plot(tpr_keras, 1 - fpr_keras, label="ROCAUC = {:.3f}".format(auc_keras))
plt.xlabel("True Positive Rate")
plt.ylabel("1-False Positive Rate")
plt.title("ROC curve")
plt.legend(loc="best")
plt.savefig(output_dir+"/plots/roc_curve_%s.pdf" % name_string)
