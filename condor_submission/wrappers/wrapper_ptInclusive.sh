#!/bin/bash

#NEEDED
export HOME=$(pwd)
export PROOFANADIR=$(pwd)
#ROOT STUFF
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
localSetupROOT 6.14.04-x86_64-slc6-gcc62-opt --skipConfirm
lsetup python
#export PYTHONPATH=$HOME/CondorPythonLocal/lib/python2.7/site-packages:$PYTHONPATH

printf "Start time: "; /bin/date
printf "Job is running on node: "; /bin/hostname
printf "Job running as user: "; /usr/bin/id
printf "Job is running in directory: "; /bin/pwd
printf "TJB ls output: "; ls
python TMVA_ptInclusive.py -e ${1} ${2} ${3} ${4}
#mv SinglePhoton /bdata/tburch/tmva_submit/BDT_${1}_${2}${3}${4}
#mv BDT_eta${1}ptInclusive*.root /bdata/tburch/tmva_submit/BDT_${1}_${2}${3}${4}.root
mv SinglePhoton /bdata/tburch/tmva_submit/BDT_eta${1}ptInclusive_${2: -1}${3: -1}_${4}
mv BDT_eta${1}ptInclusive*.root /bdata/tburch/tmva_submit/BDT_eta${1}ptInclusive_${2: -1}${3: -1}_${4}.root
