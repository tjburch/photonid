Target=pID_preselection
DIR=histos
ROOTFLAGS = $(shell root-config --cflags)
ROOTLIBS  = $(shell root-config --libs)
# LXPLUS
#SIGNAL_INPUT=/eos/user/t/tburch/datasets/photonID/20181008_topoclusters/Py8_gammajet_mc16a_topoClusters.root
#BACKGROUND_INPUT=/eos/user/t/tburch/datasets/photonID/20181008_topoclusters/Py8_jetjet_mc16a_topoClusters.root
#DATA_INPUT=/eos/user/t/tburch/datasets/photonID/20181008_topoclusters/data16_topo.root

#t3
SIGNAL_INPUT=/bdata/tburch/photonID/Py8_gammajet_mc16a_topoClusters.root
BACKGROUND_INPUT=/bdata/tburch/photonID/Py8_jetjet_mc16a_topoClusters.root
#DATA_INPUT=/eos/user/t/tburch/datasets/photonID/20181008_topoclusters/data16_topo.root

all: $(Target)


# Compilation

pID_preselection : src/pID_preselection.cxx
	g++ -o bin/$@ $< $(Objects) $(ROOTFLAGS) $(ROOTLIBS)

Pid_preselection_leadingSaved : src/pID_preselection_leadingSaved.cxx
	g++ -o bin/$@ $< $(Objects) $(ROOTFLAGS) $(ROOTLIBS)

TMVA_prep : src/TMVA_prep.cxx
	g++ -o bin/$@ $< $(Objects) $(ROOTFLAGS) $(ROOTLIBS)

add_pt_reweighting : src/add_pt_reweighting.cxx
	g++ -o bin/$@ $< $(Objects) $(ROOTFLAGS) $(ROOTLIBS)

# Running
signal_preselection : bin/pID_preselection
	./bin/pID_preselection $(SIGNAL_INPUT) ntuples/signal_selection.root signal loose

background_preselection : bin/pID_preselection
	./bin/pID_preselection $(BACKGROUND_INPUT) ntuples/background_selection.root background loose

signal_tight_preselection : bin/pID_preselection
	./bin/pID_preselection $(SIGNAL_INPUT) ntuples/signal_tight_selection.root signal tight

background_tight_preselection : bin/pID_preselection
	./bin/pID_preselection $(BACKGROUND_INPUT) ntuples/background_tight_selection.root background tight

data_preselection : bin/pID_preselection_leadingSaved
	nohup ./bin/pID_preselection_leadingSaved $(DATA_INPUT) ntuples/data_selection.root data loose &> logs/data_preselection.log & disown

data_tight_preselection : bin/pID_preselection_leadingSaved
	nohup ./bin/pID_preselection_leadingSaved $(DATA_INPUT) ntuples/data_tight_selection.root data tight &> logs/data_tight_preselection.log & disown

signal_preselection_none : bin/pID_preselection
	./bin/pID_preselection $(SIGNAL_INPUT) ntuples/signal_none_selection.root signal none

background_preselection_none : bin/pID_preselection
	./bin/pID_preselection $(BACKGROUND_INPUT) ntuples/background_none_selection.root background none

prepare_trees : bin/TMVA_prep
	./bin/TMVA_prep $(SIGNAL_INPUT) ntuples/TMVA_ready_gammajet_mc16a.root
	./bin/TMVA_prep $(BACKGROUND_INPUT) ntuples/TMVA_ready_jetjet_mc16a.root
	./bin/add_pt_reweighting ntuples/TMVA_ready_gammajet_mc16a.root  ntuples/TMVA_ready_jetjet_mc16a.root ntuples/TMVA_ready_withSF.root

add_pt:
	./bin/add_pt_reweighting ntuples/TMVA_ready_gammajet_mc16a.root  ntuples/TMVA_ready_jetjet_mc16a.root ntuples/TMVA_ready_withSF.root


# Histogramming
signal_histogram : python/pID_histograms.py
	python $< -i ntuples/signal_selection.root -o histograms/signal_loose_histograms/signal_loose

background_histogram : python/pID_histograms.py
	python $< -i ntuples/background_selection.root -o histograms/background_loose_histograms/background_loose

signal_tight_histogram : python/pID_histograms.py
	python $< -i ntuples/signal_tight_selection.root -o histograms/signal_tight_histograms/signal_tight

background_tight_histogram : python/pID_histograms.py
	python $< -i ntuples/background_tight_selection.root -o histograms/background_tight_histograms/background_tight

data_histogram : python/pID_histograms.py
	python $< -i ntuples/data_selection.root -o histograms/data_loose_histograms/data_loose

data_tight_histogram : python/pID_histograms.py
	python $< -i ntuples/data_tight_selection.root -o histograms/data_tight_histograms/data_tight

all_histos:
	nohup make signal_histogram &> logs/signal_hists.log  & disown
	nohup make background_histogram &> logs/background_hists.log & disown
	nohup make signal_tight_histogram &> logs/signal_tight_hists.log & disown
	nohup make background_tight_histogram &> logs/background_tight_hists.log & disown
	# nohup make data_histogram &> logs/data_hists.log  & disown
	# nohup make data_tight_histogram &> logs/data_tight_hists.log & disown

setup_directories:
	mkdir histograms/signal_loose_histograms/
	mkdir histograms/signal_tight_histograms/
	mkdir histograms/background_loose_histograms/
	mkdir histograms/background_tight_histograms/



# Eta Split histograms
split_signal_histogram : python/pID_etaSplit_histos.py
	python $< -i ntuples/signal_selection.root -o histograms/split_signal_histogram

split_background_histogram : python/pID_etaSplit_histos.py
	python $< -i ntuples/background_selection.root -o histograms/split_background_histogram

split_signal_tight_histogram : python/pID_etaSplit_histos.py
	python $< -i ntuples/signal_tight_selection.root -o histograms/split_signal_tight_histogram

split_background_tight_histogram : python/pID_etaSplit_histos.py
	python $< -i ntuples/background_tight_selection.root -o histograms/split_background_tight_histogram

# Plotting
plot_sb : python/pID_plotting.py
	python $< -s histograms/signal_histogram.root -b histograms/background_histogram.root -o run/normal/

plot_tight_sb : python/pID_plotting.py
	python $< -s histograms/signal_tight_histogram.root -b histograms/background_tight_histogram.root -o run/tight/

plot_compare : python/tight_v_normal.py
	python3 $< -o run/compare/

plot_lowEta_compare : python/tight_v_normal.py
	python $< -o run/lowEta/ -e 0_0p6

# full run
all :  bin/pID_preselection
	make pID_preselection
	make signal_preselection ; make background_preselection
	make signal_histogram ; make background_histogram

# Convert to numpy
numpy_convert: python/root2numpy.py
	python python/root2numpy.py -i ntuples/background_tight_selection.root -t eta_0_0p6 -s
	python python/root2numpy.py -i ntuples/background_selection.root -t eta_0_0p6 -s
	python python/root2numpy.py -i ntuples/signal_tight_selection.root -t eta_0_0p6 -s
	python python/root2numpy.py -i ntuples/signal_selection.root -t eta_0_0p6 -s


# download
download_histograms :
	scp tburch@lxplus.cern.ch:/afs/cern.ch/work/t/tburch/public/workspace/photonID/histograms/*histogram* histograms/

tmva_download :
	scp -r tburch@lxplus.cern.ch:/afs/cern.ch/work/t/tburch/public/workspace/photonID/models/tmva_output.root run/tmva/
	scp -r tburch@lxplus.cern.ch:/afs/cern.ch/work/t/tburch/public/workspace/photonID/SinglePhoton/weights run/tmva/

overnight:
	make pID_preselection
	make signal_preselection; make background_preselection
	make signal_tight_preselection; make background_tight_preselection
	make numpy_convert
	make signal_histogram; make background_histogram
	make signal_tight_histogram; make background_tight_histogram
