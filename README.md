# photonID

Code for my photonID studies with ATLAS CP group

## Description

src/ - c++ code for preselection

- ```pID_preselection.cxx``` - Applies preselection to files with all topo-clusters in them
- ```pID_preselection_leadingSaved.cxx``` - Applies preselection to files with the leading topo-clusters in it
- ```TMVA_prep.cxx``` - Takes files with all topo-clusters and saves just the leading (changed to ```save_leading.cxx``` in the merged repo)
- ```add_pt_reweighting``` - Bins pT distribution to find a reweighting distribution to sample in order to make the signal and background pT spectra look the  same

learning/ - Machine learning code
- ```TMVA.py``` - Runs TMVA for both cuts method and BDT
- ```keras_model.py``` - Runs NN

- ```TMVA_ptInclusive.py``` - pT inclusive BDT script
- ```apply_bdt_binned.py``` - Take existing BDT and apply it bin-wise
- ```apply_model_binned.py``` - same as apply BDT for a generic sklearn/keras type model
- ```profile_plotting.py``` - Generates profile plots for pileup vs bdt score. Very roundabout, probably should be deprecated and rewritten
- ```pt_vs_score.py``` - Generates profile plots via both ROOT and python ecosystem functions (seaborn) for pt vs BDT score
- ```utils.py``` - static definition of eta and pt maps. Also reweighting and flattening in pT functions, however these are less important since they're not being done anymore
- ```manual_roc_evaluation.py``` - Generates base CSV files to be analyzed for various model types depending on argument passed
- ```apply_all.sh``` - Runs ```apply_model_binned``` to each eta/pt bin and outputs to an argument

 
python/ (I've vaguely tried to order this in terms of usefulness)
- ```pID_histograms.py``` - Generates histogram files out of leading topo-cluster files output from preselection code
- ```pID_plotting.py``` - Generic plotting for all leading topo-cluster variables
- ```data_mc_ratioplots.py``` - makes comparison plots (both normal and ratio) for data/MC
- ```root2numpy.py``` - Converts root ntuples to numpy arrays
- ```labels.py``` - dictionary of labels for plotting
- ```standard_functions.py``` - importable functions. Ultimately just a simple watermarking script for plots and a normalization function.
- ```quickcomp.py``` - Quick comparison of BDT to Cuts method in the same file (hardcoded).
- ```correlation_matrix.py``` - generates nice correlation matrix
- ```get_ROCAUC.py``` - Get AUC ROC for a TMVA BDT (and rectangular cuts), as well as in the 80-90% signal efficiency ROI
- ```numpy_preselection``` - Applies preselection to numpy arrays
- ```reweighting_check.py``` - Draws reweighting function as a cross-check
- ```roc_comp.py``` - Compares the ROC Curves of 2 different BDTs (topo vs SS-only)
- ```tight_v_normal.py``` - plots distributions for tight vs loose preselections


To derive the gain in background rejection for the same signal efficiency, there's a bunch of scripts to add the output from various methods to the CSV. These were sort of constructed as-needed and unfortunately spiraled out of control a bit.... they should be updated if they're worth supporting.

- Start with ```python/bdt_roc_comparison.py``` and build off the CSV from it.
- Add the output of ```learning/apply_model_binned.py``` and ```learning/apply_bdt_binned.py``` through the following (as needed): ```python/add_nn.py```, ```python/add_bdt_ptInclusive.py```.
- Add even more output by running ```python/merge_result_csvs.py``` (alternatively, if you're merging things without the base CSV there's ```python/merge_csvs_without_base.py```.
- Make tables out of them through ```evaluate_binned_gain.py```





Deprecated:
- ```learning/train_test_split.py``` - deprecated
- ```learning/training_inclusively.py``` - trains NN inclusively (probably don't want this)

- ```python/pID_eventloop_histograms.py``` - Fills histograms via eventloop rather than TTree::Draw
- ```python/pID_etaSplit_histos.py``` - Generates histogram files split only in eta (inclusive in pT)
- ```python/evaluate_inclusive_gain.py```- Evaluates gain for inclusive models

* Histogramming
* Plotting
* TMVA 



## Presentations 

* Oct 25, 2018: [Topo Cluster Variables for Photon ID](https://indico.cern.ch/event/767664/contributions/3191328/attachments/1741917/2818630/topo_cluster_1025.pdf)
* Dec 06, 2018: [Updates on Optimization Studies](https://indico.cern.ch/event/778886/contributions/3244011/attachments/1767065/2869528/12062018_optimization_studies.pdf)
* Dec 12, 2018: Egamma Meeting - [Photon ID Status Report](https://indico.cern.ch/event/779122/contributions/3245226/attachments/1770230/2876177/burch_egamma_20181212.pdf)
* Jan 17, 2019: [Data-MC Comparisons of topo-cluster variables](https://indico.cern.ch/event/788831/timetable/)
* Jan 28, 2019: [e/gamma workshop](https://indico.cern.ch/event/748648/timetable/) - [Optimization, Tests on Data, MVA Perspectives](https://indico.cern.ch/event/748648/contributions/3219008/attachments/1783251/2909027/burch_optimization_mvaperspectives.pdf)

### Dependencies
Varies by script, but in general you'll need the following packages:

c++:
* ROOT (6.xx) 

Python:
* ROOT
* rootpy (especially for plotting)
* matplotlib
* numpy
* root_numpy

Learning:
* sklearn