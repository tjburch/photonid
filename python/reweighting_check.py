import ROOT
from collections import OrderedDict

f_in = ROOT.TFile("/bdata/tburch/tmva_submit/TMVA_ready_withSF_traintest.root")
t_background = f_in.Get("JetJet_train")
#h1 = ROOT.TH1F("h_test","h_test",25, 0, 1)
#t_background.Draw("y_Rphi>>h_test","mcTotWeight","hist")
#h1.SetLineColor(ROOT.kRed)
#c1.SaveAs("Test.pdf")


binning_dict = {}
binning_dict['y_Rphi'] = ( 25, 0, 1 )
binning_dict['y_fracs1'] = ( 20, 0, 2 )
binning_dict['y_Eratio'] = ( 15, 0, 1.5 )
binning_dict['y_wtots1'] = ( 10, 0, 10 )
binning_dict['y_deltae'] = ( 50, 0, 2500 )
binning_dict['y_weta1'] = ( 25, 0, 1 )
binning_dict['y_weta2'] = ( 40, 0, 0.02 )
binning_dict['y_Reta'] = ( 40, 0.7, 1.1 )
binning_dict['y_Rhad1'] = ( 30, -0.07, 0.07 )
binning_dict['y_topoCluster0_engPos'] = (50, 0, 500)
binning_dict['y_topoCluster0_centerLambda'] = (50, 0, 1000)

ss_vars = [
    'y_Rphi',
    'y_fracs1',
    'y_Eratio',
    'y_wtots1',
    'y_deltae',
    'y_weta1',
    'y_weta2',
    'y_Reta',
    'y_Rhad1',
    'y_topoCluster0_engPos',
    'y_topoCluster0_centerLambda'
]

c1 = ROOT.TCanvas()

for var in ss_vars:
    binning = binning_dict[var]

    if var == "y_topoCluster0_engPos":
        extrafactor = "0.001*"
    else:
        extrafactor = ""

    hist = ROOT.TH1F("h_"+var, "h_"+var, binning[0],binning[1],binning[2])
    drawing_string = extrafactor+"%s>>%s"%(var,"h_"+var)
    t_background.Draw(drawing_string,"mcTotWeight","hist")
    hist.Scale(1/hist.Integral())    

    hist_rw = ROOT.TH1F("h_rw_"+var, "h_rw_"+var, binning[0],binning[1],binning[2])
    drawing_string = extrafactor+"%s>>%s"%(var,"h_rw_"+var)
    t_background.Draw(drawing_string,"mcTotWeight * reweight_SF","hist sames")
    hist_rw.Scale(1/hist_rw.Integral())    
    hist_rw.SetLineColor(ROOT.kRed)
    hist_rw.SetLineStyle(7)

    hist.SetMaximum(1.2*max([hist.GetMaximum(),hist_rw.GetMaximum()]))

    ROOT.gStyle.SetOptStat(000000)

    leg = ROOT.TLegend(0.7,0.8,0.9,0.9)
    leg.AddEntry(hist,"Unreweighted","l")
    leg.AddEntry(hist_rw,"Reweighted","l")
    leg.Draw()
    c1.SaveAs("/xdata/tburch/workspace/photonid/run/validation/histocheck/"+var+".pdf")



hist = ROOT.TH1F("h_rw", "h_rw",  25, 0, 0.05)
drawing_string = "reweight_SF>>h_rw"
t_background.Draw(drawing_string,"mcTotWeight","hist")

ROOT.gStyle.SetOptStat(000000)

c1.SaveAs("/xdata/tburch/workspace/photonid/run/validation/histocheck/sf.pdf")



