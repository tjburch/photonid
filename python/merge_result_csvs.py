import pandas as pd

def fix_eta_bins(df):
    # solves 4 gap
    df['eta'] = df['eta'].where(df['eta'] < 5, df['eta'] - 1)
    return df


if __name__ == "__main__":

    # Run add_nn before this
    # Add standard NN
    vanilla_nn = "data/efficiency_w_nn.csv"
    nn_df = pd.read_csv(vanilla_nn)
    nn_df['pt_bin'] = nn_df['pt_bin'].astype(str)

    # Add inclusive non-RW
    inclusive_nonRW = "data/performance.csv"
    inclusive_df = pd.read_csv(inclusive_nonRW)
    inclusive_df['pt'] = inclusive_df['pt'].astype(str)
    fix_eta_bins(inclusive_df)

    inclusive_df = inclusive_df.rename(columns={"eta":"eta_bin",
                                                "pt": "pt_bin",
                                                "signal_efficiency" : "NN_nonRW_sig_eff",
                                                "background_rejection": "NN_nonRW_bkg_rej"})

    merged_df = pd.merge(nn_df, inclusive_df, on=["eta_bin","pt_bin"], how='outer')

    # Add inclusive RW
    inclusive_RW = "data/performance_rw.csv"
    inclusiveRW_df = pd.read_csv(inclusive_RW)
    inclusiveRW_df['pt'] = inclusiveRW_df['pt'].astype(str)
    inclusiveRW_df['eta'] = inclusiveRW_df['eta'].where(inclusiveRW_df['eta'] < 5, inclusiveRW_df['eta'] - 1)

    inclusiveRW_df = inclusiveRW_df.rename(columns={"eta":"eta_bin",
                                                "pt": "pt_bin",
                                                "signal_efficiency" : "NN_RW_sig_eff",
                                                "background_rejection": "NN_RW_bkg_rej"})
    merged_df = pd.merge(merged_df, inclusiveRW_df, on=["eta_bin","pt_bin"], how='outer')
    
    # Add pt dependent BDT reweighted
    bdtRW = "data/efficiency_w_ptinclusive_bdt.csv"
    bdtRW_df = pd.read_csv(bdtRW)
    bdtRW_df['pt'] = bdtRW_df['pt'].astype(str)
    bdtRW_df['eta'] = bdtRW_df['eta'].where(bdtRW_df['eta'] < 5, bdtRW_df['eta'] - 1)

    bdtRW_df = bdtRW_df.rename(columns={"eta":"eta_bin",
                                                "pt": "pt_bin",
                                                "signal_efficiency" : "BDT_RW_sig_eff",
                                                "background_rejection": "BDT_RW_bkg_rej"})
    merged_df = pd.merge(merged_df, bdtRW_df, on=["eta_bin","pt_bin"], how='outer')    

    # Add pt dependent BDT unreweighted
    bdt_noRW = "data/efficiency_w_ptinclusive_bdt_unreweighted.csv"
    bdt_noRW_df = pd.read_csv(bdt_noRW)
    bdt_noRW_df['pt'] = bdt_noRW_df['pt'].astype(str)
    #print(bdt_noRW_df[bdt_noRW_df['eta']>4])

    bdt_noRW_df['eta'] = bdt_noRW_df['eta'].where(bdt_noRW_df['eta'] < 5, bdt_noRW_df['eta'] - 1)
    bdt_noRW_df = bdt_noRW_df.rename(columns={"eta":"eta_bin",
                                        "pt": "pt_bin",
                                        "signal_efficiency" : "BDT_unRW_sig_eff",
                                        "background_rejection": "BDT_unRW_bkg_rej"})
    merged_df = pd.merge(merged_df, bdt_noRW_df, on=["eta_bin","pt_bin"], how='outer')

    # Add high pT model
    bdt_high = "data/efficiency_w_highpT_model.csv"
    bdt_high_df = pd.read_csv(bdt_high)
    bdt_high_df['pt'] = bdt_high_df['pt'].astype(str)
    #print(bdt_noRW_df[bdt_noRW_df['eta']>4])

    bdt_high_df['eta'] = bdt_high_df['eta'].where(bdt_high_df['eta'] < 5, bdt_high_df['eta'] - 1)
    bdt_high_df = bdt_high_df.rename(columns={"eta":"eta_bin",
                                        "pt": "pt_bin",
                                        "signal_efficiency" : "BDT_high_sig_eff",
                                        "background_rejection": "BDT_high_bkg_rej"})
    merged_df = pd.merge(merged_df, bdt_high_df, on=["eta_bin","pt_bin"], how='outer')  

    #
    csv_secondary = "data/merged_csv.csv"
    df_secondary = pd.read_csv(csv_secondary)
    df_secondary = df_secondary[df_secondary['pt_bin'] < 7]
    df_secondary['pt_bin'] = df_secondary['pt_bin'].astype(str)
    #print(bdt_noRW_df[bdt_noRW_df['eta']>4])

    df_secondary['eta_bin'] = df_secondary['eta_bin'].where(df_secondary['eta_bin'] < 5, df_secondary['eta_bin'] - 1)
    df_secondary = df_secondary[["eta_bin",
                                "pt_bin",
                                "topo_inclusive_bkg_rej",
                                "updated_cuts_bkg_rej", 
                                "updated_cuts_topo_bkg_rej", 
                                "inclusive_bkg_rej", 
                                "topo_inclusive_reweight_bkg_rej",
                                ]]
    merged_df = pd.merge(merged_df, df_secondary, on=["eta_bin","pt_bin"], how='outer')  



    merged_df.to_csv('data/full_results.csv')