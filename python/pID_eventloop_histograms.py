import ROOT 
import argparse
from math import sqrt


def create_histo_dictionary():
    """Create 1d Histograms.
    
    Returns:
        [dict] -- dictionary of TH1Fs

    """
    histo_dictionary ={}
    histo_dictionary['y_topoCluster_secondLambda']  = ROOT.TH1F('y_topoCluster_secondLambda', 'y_topoCluster_secondLambda', 40, 0, 320)
    histo_dictionary['y_topoCluster_secondR']       = ROOT.TH1F('y_topoCluster_secondR', 'y_topoCluster_secondR', 40, 0, 150)
    histo_dictionary['y_topoCluster_centerMag']     = ROOT.TH1F('y_topoCluster_centerMag', 'y_topoCluster_centerMag', 20, 1000, 5000)
    histo_dictionary['y_topoCluster_centerLambda']  = ROOT.TH1F('y_topoCluster_centerLambda', 'y_topoCluster_centerLambda', 100, 0, 800)
    histo_dictionary['y_topoCluster_isolation']     = ROOT.TH1F('y_topoCluster_isolation', 'y_topoCluster_isolation', 50, 0, 1)
    histo_dictionary['y_topoCluster_engBadCells']   = ROOT.TH1F('y_topoCluster_engBadCells', 'y_topoCluster_engBadCells', 150, 0, 150)
    histo_dictionary['y_topoCluster_nBadCells']     = ROOT.TH1F('y_topoCluster_nBadCells', 'y_topoCluster_nBadCells', 6, 0, 6)
    histo_dictionary['y_topoCluster_badLarQFrac']   = ROOT.TH1F('y_topoCluster_badLarQFrac', 'y_topoCluster_badLarQFrac', 20, 0, 0.4)
    histo_dictionary['y_topoCluster_engPos']        = ROOT.TH1F('y_topoCluster_engPos', 'y_topoCluster_engPos', 30, 0, 500)
    histo_dictionary['y_topoCluster_avgLarQ']      = ROOT.TH1F('y_topoCluster_avgLarQ', 'y_topoCluster_avgLarQ', 30, 0, 400)
    histo_dictionary['y_topoCluster_avgTileQ']      = ROOT.TH1F('y_topoCluster_avgTileQ', 'y_topoCluster_avgTileQ', 30, 0, 20)
    histo_dictionary['y_topoCluster_emProbability'] = ROOT.TH1F('y_topoCluster_emProbability', 'y_topoCluster_emProbability', 15, 0, 1)
    histo_dictionary['y_nTopoClusters']             = ROOT.TH1F('y_nTopoClusters', 'y_nTopoClusters', 7, 1, 8)

    # Create dictionary for leading photon 
    histo_dictionary['y_topoCluster0_secondLambda']  = ROOT.TH1F('y_topoCluster0_secondLambda', 'y_topoCluster0_secondLambda', 40, 0, 320)
    histo_dictionary['y_topoCluster0_secondR']       = ROOT.TH1F('y_topoCluster0_secondR', 'y_topoCluster0_secondR', 40, 0, 150)
    histo_dictionary['y_topoCluster0_centerMag']     = ROOT.TH1F('y_topoCluster0_centerMag', 'y_topoCluster0_centerMag', 20, 1000, 5000)
    histo_dictionary['y_topoCluster0_centerLambda']  = ROOT.TH1F('y_topoCluster0_centerLambda', 'y_topoCluster0_centerLambda', 100, 0, 800)
    histo_dictionary['y_topoCluster0_isolation']     = ROOT.TH1F('y_topoCluster0_isolation', 'y_topoCluster0_isolation', 50, 0, 1)
    histo_dictionary['y_topoCluster0_engBadCells']   = ROOT.TH1F('y_topoCluster0_engBadCells', 'y_topoCluster0_engBadCells', 150, 0, 150)
    histo_dictionary['y_topoCluster0_nBadCells']     = ROOT.TH1F('y_topoCluster0_nBadCells', 'y_topoCluster0_nBadCells', 6, 0, 6)
    histo_dictionary['y_topoCluster0_badLarQFrac']   = ROOT.TH1F('y_topoCluster0_badLarQFrac', 'y_topoCluster0_badLarQFrac', 20, 0, 0.4)
    histo_dictionary['y_topoCluster0_engPos']        = ROOT.TH1F('y_topoCluster0_engPos', 'y_topoCluster0_engPos', 30, 0, 500)
    histo_dictionary['y_topoCluster0_avgLarQ']      = ROOT.TH1F('y_topoCluster0_avgLarQ', 'y_topoCluster0_avgLarQ', 30, 0, 400)
    histo_dictionary['y_topoCluster0_avgTileQ']      = ROOT.TH1F('y_topoCluster0_avgTileQ', 'y_topoCluster0_avgTileQ', 30, 0, 20)
    histo_dictionary['y_topoCluster0_emProbability'] = ROOT.TH1F('y_topoCluster0_emProbability', 'y_topoCluster0_emProbability', 15, 0, 1)

    # Add distributions for validation
    histo_dictionary['y_Rphi'] = ROOT.TH1F('y_Rphi','y_Rphi', 25, 0, 1 )
    histo_dictionary['y_fracs1'] = ROOT.TH1F('y_fracs','y_fracs', 20, 0, 2 )
    histo_dictionary['y_Eratio'] = ROOT.TH1F('y_Eratio','y_Eratio', 15, 0, 1.5 )
    histo_dictionary['y_wtots1'] = ROOT.TH1F('y_wtots1','y_wtots1', 10, 0, 10 )
    histo_dictionary['y_deltae'] = ROOT.TH1F('y_deltae','y_deltae', 50, 0, 2500 )
    histo_dictionary['y_weta1'] = ROOT.TH1F('y_weta1','y_weta1', 25, 0, 1 )
    histo_dictionary['y_weta2'] = ROOT.TH1F('y_weta2','y_weta2', 40, 0, 0.02 )
    histo_dictionary['y_Reta'] = ROOT.TH1F('y_Reta','y_Reta', 40, 0.7, 1.1 )
    histo_dictionary['y_Rhad1'] = ROOT.TH1F('y_Rhad1','y_Rhad1', 30, -0.06, 0.06 )

    return histo_dictionary


def create_twoD_dictionary():
    """Create 2d Histograms.
    
    Returns:
        [dict] -- dictionary of TH2Fs

    """
    histo_dictionary ={}
    histo_dictionary['secondLambda_v_secondR'] = ROOT.TH2F('secondLambda_v_secondR','secondLambda_v_secondR',50, 0, 150, 40, 0, 320)
    histo_dictionary['firstisolation_v_nClusters'] = ROOT.TH2F('isolation_v_nClusters','isolation_v_nClusters',7, 1, 8, 50, 0, 1)
    return histo_dictionary


def fill_histograms(tree, histograms, weight):
    """Fill 1d histograms.
    
    Arguments:
        tree {ttree} -- hgamsinglephotons
        histograms {dict} -- created by function above
        weight {float} -- event weight
        
    """
    for iClust in range(0, tree.y_nTopoClusters):
        histograms['y_topoCluster_secondLambda'] .Fill(sqrt(tree.y_topoCluster_secondLambda[iClust]), weight) 
        histograms['y_topoCluster_secondR']      .Fill(sqrt(tree.y_topoCluster_secondR[iClust]), weight    ) 
        histograms['y_topoCluster_centerMag']    .Fill(tree.y_topoCluster_centerMag[iClust], weight  ) 
        histograms['y_topoCluster_centerLambda'] .Fill(tree.y_topoCluster_centerLambda[iClust], weight) 
        histograms['y_topoCluster_isolation']    .Fill(tree.y_topoCluster_isolation[iClust], weight  ) 
        histograms['y_topoCluster_engBadCells']  .Fill(tree.y_topoCluster_engBadCells[iClust] / 1000, weight) 
        histograms['y_topoCluster_nBadCells']    .Fill(tree.y_topoCluster_nBadCells[iClust], weight  ) 
        histograms['y_topoCluster_badLarQFrac']  .Fill(tree.y_topoCluster_badLarQFrac[iClust], weight) 
        histograms['y_topoCluster_engPos']       .Fill(tree.y_topoCluster_engPos[iClust] / 1000, weight     ) 
        histograms['y_topoCluster_avgLarQ']     .Fill(tree.y_topoCluster_avgLarQ[iClust], weight   ) 
        histograms['y_topoCluster_avgTileQ']     .Fill(tree.y_topoCluster_avgTileQ[iClust], weight   ) 
        histograms['y_topoCluster_emProbability'].Fill(tree.y_topoCluster_emProbability[iClust], weight) 
    
    histograms['y_nTopoClusters']            .Fill(tree.y_nTopoClusters, weight) 
    histograms['y_topoCluster0_secondLambda'] .Fill(sqrt(tree.y_topoCluster_secondLambda[0]), weight) 
    histograms['y_topoCluster0_secondR']      .Fill(sqrt(tree.y_topoCluster_secondR[0]), weight    ) 
    histograms['y_topoCluster0_centerMag']    .Fill(tree.y_topoCluster_centerMag[0], weight  ) 
    histograms['y_topoCluster0_centerLambda'] .Fill(tree.y_topoCluster_centerLambda[0], weight) 
    histograms['y_topoCluster0_isolation']    .Fill(tree.y_topoCluster_isolation[0], weight  ) 
    histograms['y_topoCluster0_engBadCells']  .Fill(tree.y_topoCluster_engBadCells[0] / 1000, weight) 
    histograms['y_topoCluster0_nBadCells']    .Fill(tree.y_topoCluster_nBadCells[0], weight  ) 
    histograms['y_topoCluster0_badLarQFrac']  .Fill(tree.y_topoCluster_badLarQFrac[0], weight) 
    histograms['y_topoCluster0_engPos']       .Fill(tree.y_topoCluster_engPos[0] / 1000, weight     ) 
    histograms['y_topoCluster0_avgLarQ']     .Fill(tree.y_topoCluster_avgLarQ[0], weight   ) 
    histograms['y_topoCluster0_avgTileQ']     .Fill(tree.y_topoCluster_avgTileQ[0], weight   ) 
    histograms['y_topoCluster0_emProbability'].Fill(tree.y_topoCluster_emProbability[0], weight) 

    histograms['y_Rphi'].Fill(tree.y_Rphi, weight)
    histograms['y_fracs1'].Fill(tree.y_fracs1, weight)
    histograms['y_Eratio'].Fill(tree.y_Eratio, weight)
    histograms['y_wtots1'].Fill(tree.y_wtots1, weight)
    histograms['y_deltae'].Fill(tree.y_deltae, weight)
    histograms['y_weta1'].Fill(tree.y_weta1, weight)
    histograms['y_weta2'].Fill(tree.y_weta2, weight)
    histograms['y_Reta'].Fill(tree.y_Reta, weight)
    histograms['y_Rhad1'].Fill(tree.y_Rhad1, weight)


def fill_twoD(tree, histograms, weight):
    """Fill 2d histograms
    
    Arguments:
        tree {ttree} -- hgamsinglephotons
        histograms {dict} -- created by function above
        weight {float} -- event weight

    """
    for iClust in range(0, tree.y_nTopoClusters):
        histograms['secondLambda_v_secondR'].Fill(tree.y_topoCluster_secondR[iClust], tree.y_topoCluster_secondLambda[iClust], weight)
    if tree.y_nTopoClusters > 0:
        histograms['firstisolation_v_nClusters'] .Fill(tree.y_nTopoClusters, tree.y_topoCluster_isolation[0], weight)



if __name__=='__main__':
    parser = argparse.ArgumentParser('Generates TH objects from single photon nTuples')
    parser.add_argument('-i', dest='inputFile', required=True, help='Input SinglePhoton ntuple')
    parser.add_argument('-o', dest='outputFile', required=True, help='Output Collection of TH objects')
    args = parser.parse_args()

    input_file = args.inputFile

    # Get file
    treename = 'SinglePhoton'
    infile = ROOT.TFile(input_file)
    
    # Get tree
    tree = infile.Get(treename)
    nEntries = tree.GetEntries()
    print "sample: ", infile," has entries ", nEntries

    histogram_dictionary = create_histo_dictionary()
    twoD_histograms = create_twoD_dictionary()

    for evt in range(nEntries):
        tree.GetEntry(evt)
        # Show progress
        if (evt % 1000000 == 0):
            print "%.1f percent completed. On event %i." % (100*float(evt)/nEntries, evt)

        fill_histograms(tree, histogram_dictionary, tree.evtWeight)
        fill_twoD(tree, twoD_histograms, tree.evtWeight)

    output_file = ROOT.TFile(args.outputFile,'recreate')
    for key in histogram_dictionary:
        histogram_dictionary[key].Write()

    for key in twoD_histograms:
        twoD_histograms[key].Write()

    output_file.Close()