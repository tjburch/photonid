from root_numpy import root2array
import numpy as np
import sys
from socket import gethostname
import argparse
import h5py

parser = argparse.ArgumentParser()
parser.add_argument('-i', dest='inputFile', required=True, help='Input SinglePhoton ntuple')
parser.add_argument('-t', dest='treename',required=True, help='Tree name')
parser.add_argument('-s',dest='slim', action='store_true', required=False, help='Make slim?')
args = parser.parse_args()


if "lxplus" not in gethostname() and "nicadd" not in gethostname():
    print "You're running this on your laptop, that's going to be really slow. You brought this on yourself."
    print "Hostname: ", gethostname()

list_of_branches = [
    'y_topoCluster0_secondLambda',
    'y_topoCluster0_secondR',
    'y_topoCluster0_centerMag',
    'y_topoCluster0_centerLambda',
    'y_topoCluster0_isolation',
    'y_topoCluster0_engBadCells',
    'y_topoCluster0_nBadCells',
    'y_topoCluster0_badLarQFrac',
    'y_topoCluster0_engPos',
    #'y_topoCluster0_avgLarQ',
    'y_topoCluster0_avgTileQ',
    'y_topoCluster0_emProbability',
    'y_Rphi',
    'y_fracs1',
    'y_Eratio',
    'y_wtots1',
    'y_deltae',
    'y_weta1',
    'y_weta2',
    'y_Reta',
    'y_Rhad1',
    'y_pt',
    'y_iso_FixedCutLoose',
    'y_iso_FixedCutTight',
    'y_iso_FixedCutTightCaloOnly',
]


input_name = args.inputFile.strip('.root')
print "Converting tree %s in file %s to numpy" % (args.treename, args.inputFile)
try:
    if args.slim:
        print "Doing slim array"
        array = root2array(args.inputFile, args.treename, branches=list_of_branches)
        input_name = input_name+'_slim'
    else:
        array = root2array(args.inputFile, args.treename)

    h5f = h5py.File(name=input_name+'.h5', mode='w')
    h5f.create_dataset(args.treename, data=array)
    h5f.close()

except IOError:
    print "Tree %s does not exist!" % treename
    
    try: 
        from rootpy.io import root_open 
        print "Available items in %s: " % args.inputFile
        infile = root_open(args.inputFile)
        for item in infile:
            print item

    except ImportError:
        print "Cannot import rootpy.io to list TTrees. Try setting up you venv?"
