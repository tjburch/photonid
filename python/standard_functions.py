import matplotlib.pyplot as plt

def add_watermark():
    plt.annotate(s='Tyler James Burch', xy=(.005,.007), xycoords='figure fraction',
                textcoords='figure fraction', color='grey',alpha=0.7)

def normalize(histogram):
    histogram.Scale(1/histogram.Integral())