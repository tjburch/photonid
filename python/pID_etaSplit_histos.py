import ROOT 
import argparse
from math import sqrt
# Largely a reshash of pID histograms so import all the stuff from there
from pID_eventloop_histograms import *

if __name__=='__main__':
    parser = argparse.ArgumentParser('Generates TH objects from single photon nTuples')
    parser.add_argument('-i', dest='inputFile', required=True, help='Input SinglePhoton ntuple')
    parser.add_argument('-o', dest='outputFilePrefix', required=True, help='Output Collection of TH objects')
    args = parser.parse_args()

    input_file = args.inputFile
    infile = ROOT.TFile(input_file)
    # Get file
    tree_names = [
        "eta_0_0p6",
        "eta_0p6_0p8",
        "eta_0p8_1p15",
        "eta_1p15_1p37",
        "eta_1p52_1p81",
        "eta_1p81_2p01",
        "eta_2p01_2p37",
    ]
    for treename in tree_names:
        
        # Get tree
        tree = infile.Get(treename)
        nEntries = tree.GetEntries()
        print "Doing Tree %s with entries %i. This is tree no %i of %i" % (treename, nEntries, tree_names.index(treename)+1, len(tree_names))

        histogram_dictionary = create_histo_dictionary()
        twoD_histograms = create_twoD_dictionary()

        for evt in range(nEntries):
            tree.GetEntry(evt)

            fill_histograms(tree, histogram_dictionary, tree.evtWeight)
            fill_twoD(tree, twoD_histograms, tree.evtWeight)

        output_file = ROOT.TFile(args.outputFilePrefix+'_'+treename+'.root' ,'recreate')
        for key in histogram_dictionary:
            histogram_dictionary[key].Write()
        del histogram_dictionary

        for key in twoD_histograms:
            twoD_histograms[key].Write()
        del twoD_histograms


        output_file.Close()