import ROOT 
import argparse
from math import sqrt
from tqdm import tqdm

def create_histo_dictionary(eta_range, pt_range):
    """Create 1d Histograms.
    Returns:
        [dict] -- dictionary of TH1Fs

    """

    histo_dictionary ={}
    for conversion_type in ['Unconverted', 'Converted']:
        s = '_{0}eta{1}_{2}pt{3}'.format(eta_range[0], eta_range[1], pt_range[0], pt_range[1])
        #catch for inclusive
        if pt_range[1]==10000: s='_inclusive'
        
        s = s+'_'+conversion_type+'_'
        """  
        Currently Just look at leading topo Cluster
        histo_dictionary['y_topoCluster_secondLambda']  = ROOT.TH1F('y_topoCluster_secondLambda'+s, 'y_topoCluster_secondLambda'+s, 40, 0, 320)
        histo_dictionary['y_topoCluster_secondR']       = ROOT.TH1F('y_topoCluster_secondR'+s, 'y_topoCluster_secondR'+s, 40, 0, 150)
        histo_dictionary['y_topoCluster_centerMag']     = ROOT.TH1F('y_topoCluster_centerMag'+s, 'y_topoCluster_centerMag'+s, 20, 1000, 5000)
        histo_dictionary['y_topoCluster_centerLambda']  = ROOT.TH1F('y_topoCluster_centerLambda'+s, 'y_topoCluster_centerLambda'+s, 100, 0, 800)
        histo_dictionary['y_topoCluster_isolation']     = ROOT.TH1F('y_topoCluster_isolation'+s, 'y_topoCluster_isolation'+s, 50, 0, 1)
        histo_dictionary['y_topoCluster_engBadCells']   = ROOT.TH1F('y_topoCluster_engBadCells'+s, 'y_topoCluster_engBadCells'+s, 150, 0, 150)
        histo_dictionary['y_topoCluster_nBadCells']     = ROOT.TH1F('y_topoCluster_nBadCells'+s, 'y_topoCluster_nBadCells'+s, 6, 0, 6)
        histo_dictionary['y_topoCluster_badLarQFrac']   = ROOT.TH1F('y_topoCluster_badLarQFrac'+s, 'y_topoCluster_badLarQFrac'+s, 20, 0, 0.4)
        histo_dictionary['y_topoCluster_engPos']        = ROOT.TH1F('y_topoCluster_engPos'+s, 'y_topoCluster_engPos'+s, 30, 0, 500)
        histo_dictionary['y_topoCluster_avgLarQ']      = ROOT.TH1F('y_topoCluster_avgLarQ'+s, 'y_topoCluster_avgLarQ'+s, 30, 0, 400)
        histo_dictionary['y_topoCluster_avgTileQ']      = ROOT.TH1F('y_topoCluster_avgTileQ'+s, 'y_topoCluster_avgTileQ'+s, 30, 0, 20)
        histo_dictionary['y_topoCluster_emProbability'] = ROOT.TH1F('y_topoCluster_emProbability'+s, 'y_topoCluster_emProbability'+s, 15, 0, 1)
        """
        histo_dictionary['y_nTopoClusters'+conversion_type]             = ROOT.TH1F('y_nTopoClusters'+s, 'y_nTopoClusters'+s, 7, 1, 8)
        # Create dictionary for leading photon 
        histo_dictionary['y_topoCluster0_secondLambda_'+conversion_type]  = ROOT.TH1F('y_topoCluster0_secondLambda'+s, 'y_topoCluster0_secondLambda'+s, 40, 0, 300)
        histo_dictionary['y_topoCluster0_secondR_'+conversion_type]       = ROOT.TH1F('y_topoCluster0_secondR'+s, 'y_topoCluster0_secondR'+s, 40, 0, 150)
        histo_dictionary['y_topoCluster0_centerMag_'+conversion_type]     = ROOT.TH1F('y_topoCluster0_centerMag'+s, 'y_topoCluster0_centerMag'+s, 20, 1000, 5000)
        histo_dictionary['y_topoCluster0_centerLambda_'+conversion_type]  = ROOT.TH1F('y_topoCluster0_centerLambda'+s, 'y_topoCluster0_centerLambda'+s, 100, 0, 800)
        histo_dictionary['y_topoCluster0_isolation_'+conversion_type]     = ROOT.TH1F('y_topoCluster0_isolation'+s, 'y_topoCluster0_isolation'+s, 50, 0, 1)
        histo_dictionary['y_topoCluster0_engBadCells_'+conversion_type]   = ROOT.TH1F('y_topoCluster0_engBadCells'+s, 'y_topoCluster0_engBadCells'+s, 150, 0, 150)
        histo_dictionary['y_topoCluster0_nBadCells_'+conversion_type]     = ROOT.TH1F('y_topoCluster0_nBadCells'+s, 'y_topoCluster0_nBadCells'+s, 6, 0, 6)
        histo_dictionary['y_topoCluster0_badLarQFrac_'+conversion_type]   = ROOT.TH1F('y_topoCluster0_badLarQFrac'+s, 'y_topoCluster0_badLarQFrac'+s, 20, 0, 0.4)
        histo_dictionary['y_topoCluster0_engPos_'+conversion_type]        = ROOT.TH1F('y_topoCluster0_engPos'+s, 'y_topoCluster0_engPos'+s, 30, 0, 500)
        #histo_dictionary['y_topoCluster0_avgLarQ']      = ROOT.TH1F('y_topoCluster0_avgLarQ'+s, 'y_topoCluster0_avgLarQ'+s, 30, 0, 400)
        histo_dictionary['y_topoCluster0_avgTileQ_'+conversion_type]      = ROOT.TH1F('y_topoCluster0_avgTileQ'+s, 'y_topoCluster0_avgTileQ'+s, 30, 0, 20)
        histo_dictionary['y_topoCluster0_emProbability_'+conversion_type] = ROOT.TH1F('y_topoCluster0_emProbability'+s, 'y_topoCluster0_emProbability'+s, 15, 0, 1)

        # Add distributions for validation
        histo_dictionary['y_Rphi_'+conversion_type] = ROOT.TH1F('y_Rphi'+s,'y_Rphi'+s, 25, 0, 1 )
        histo_dictionary['y_fracs1_'+conversion_type] = ROOT.TH1F('y_fracs1'+s,'y_fracs1'+s, 20, 0, 2 )
        histo_dictionary['y_Eratio_'+conversion_type] = ROOT.TH1F('y_Eratio'+s,'y_Eratio'+s, 15, 0, 1.5 )
        histo_dictionary['y_wtots1_'+conversion_type] = ROOT.TH1F('y_wtots1'+s,'y_wtots1'+s, 10, 0, 10 )
        histo_dictionary['y_deltae_'+conversion_type] = ROOT.TH1F('y_deltae'+s,'y_deltae'+s, 50, 0, 2500 )
        histo_dictionary['y_weta1_'+conversion_type] = ROOT.TH1F('y_weta1'+s,'y_weta1'+s, 25, 0, 1 )
        histo_dictionary['y_weta2_'+conversion_type] = ROOT.TH1F('y_weta2'+s,'y_weta2'+s, 40, 0, 0.02 )
        histo_dictionary['y_Reta_'+conversion_type] = ROOT.TH1F('y_Reta'+s,'y_Reta'+s, 40, 0.7, 1.1 )
        histo_dictionary['y_Rhad1_'+conversion_type] = ROOT.TH1F('y_Rhad1'+s,'y_Rhad1'+s, 30, -0.07, 0.07 )

    return histo_dictionary


if __name__=='__main__':
    # Read inputs
    parser = argparse.ArgumentParser('Generates TH objects from single photon nTuples')
    parser.add_argument('-i', dest='inputFile', required=True, help='Input SinglePhoton ntuple')
    # Outputfile will also be prefix for split eta/pt, so keep it short buddy.
    parser.add_argument('-o', dest='outputFile', required=True, help='Output Collection of TH objects')
    args = parser.parse_args()
    ROOT.gROOT.SetBatch(ROOT.kTRUE) # turn off visual plotting, hopefully

    # Get file
    input_file = args.inputFile
    treename = 'SinglePhoton'
    infile = ROOT.TFile(input_file)
    
    # Get tree
    tree = infile.Get(treename)
    nEntries = tree.GetEntries()
    print "sample: ", infile," has entries ", nEntries


    variable_list = [
        'y_nTopoClusters',
        'y_topoCluster0_secondLambda',
        'y_topoCluster0_secondR',
        'y_topoCluster0_centerMag',
        'y_topoCluster0_centerLambda',
        'y_topoCluster0_isolation',
        'y_topoCluster0_engBadCells',
        'y_topoCluster0_nBadCells',
        'y_topoCluster0_badLarQFrac',
        'y_topoCluster0_engPos',
        'y_topoCluster0_avgLarQ',
        'y_topoCluster0_avgTileQ',
        'y_topoCluster0_emProbability',
        'y_Rphi',
        'y_fracs1',
        'y_Eratio',
        'y_wtots1',
        'y_deltae',
        'y_weta1',
        'y_weta2',
        'y_Reta',
        'y_Rhad1',
    ]                    


    class Sample:

        def __init__(self, eta_range, pt_range):
            """
            eta_range {tuple} - lower/upper eta bounds
            pt+range {tuple} - smame for pt
            """            
            self.eta_range = eta_range
            self.pt_range = pt_range
            self.default_file = args.outputFile+'_{0}eta{1}_{2}pt{3}'.format(eta_range[0], eta_range[1], pt_range[0], pt_range[1])+'.root'
            self.histograms = create_histo_dictionary(self.eta_range, self.pt_range)


        def fill_histograms(self, tree):
            """internal method to fill up the histograms
            """


            for h in self.histograms:
                hist_name = self.histograms[h].GetName()
                variable_name = ''                
                for v in variable_list:
                    if v in hist_name:
                        variable_name = v
                        break
                # debug statement for own good
                if variable_name == '': print "issue in hist: ", hist_name

                # Build variable name
                if variable_name == 'y_topoCluster0_secondLambda' or variable_name == 'y_topoCluster0_secondR':
                    draw_name = 'TMath::Sqrt(%s)' % variable_name
                else: draw_name = variable_name

                # build TCut
                selection = 'fabs(y_eta) > ' + str(self.eta_range[0])
                selection += ' && fabs(y_eta) < ' + str(self.eta_range[1])
                selection += ' && y_pt > ' + str(self.pt_range[0])
                selection += ' && y_pt < ' + str(self.pt_range[1])
                if 'Unconverted' in hist_name:
                    selection += ' && y_convType == 0 '
                elif 'Converted' in hist_name:
                    selection += ' && y_convType != 0 '
                else: print 'conversion error'
                cut = '(%s) * evtWeight' % selection
                tree.Draw('%s >> %s' % (draw_name, hist_name), cut)

        def finalize(self, filename=None):
            # Can't use self as default in method, so detect None
            if filename is None:
                filename = self.default_file

            outputfile = ROOT.TFile(filename,'recreate')
            for h in self.histograms:
                self.histograms[h].Write()
            outputfile.Close()


    # List of hists to change from MeV to GeV
    hists_to_convert = ['engPos', 'engBadCells'] #TODO 
    c1 = ROOT.TCanvas()
    
    eta_splits = [0.0, 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37]
    pt_splits = [10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 80]
    pbar = tqdm(total=(len(eta_splits)-1) * (len(pt_splits)-1))
    
    for eta in eta_splits:
        if eta == 2.37: continue
        eta_index = eta_splits.index(eta)
        eta_upper = eta_splits[eta_index+1]

        iEtaSamples = []
        for pt in pt_splits:            
            if pt == 80: continue
            pt_index = pt_splits.index(pt)
            pt_upper = pt_splits[pt_index+1]

            #print "Doing eta: %.2f pt: %i" %(eta, pt)
            i = Sample((eta,eta_upper), (pt, pt_upper))
            i.fill_histograms(tree)
            iEtaSamples.append(i)
            i.finalize()
            del i
            pbar.update(1)

    # make an inclusive sample
    inclusive = Sample((0,100),(0,10000))
    inclusive.fill_histograms(tree)
    inclusive.finalize(filename=args.outputFile+'Inclusive.root')