import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import itertools
from evaluate_inclusive_gain import pt_labels, eta_labels, add_cols_for_method, isolate_conversion_pt
from rootpy.plotting.style import get_style, set_style
from standard_functions import add_watermark
from matplotlib import rc
import logging
logging.getLogger("matplotlib").setLevel(logging.WARNING)
set_style(get_style('ATLAS'))
import pdb

def add_gains(df):
    add_cols_for_method(df, 'cuts_bkg_rej', 'cuts_topo_bkg_rej', 'cuts')
    add_cols_for_method(df, 'BDT_unRW_bkg_rej', 'NN_nonRW_bkg_rej', 'NNinclusive_v_BDTinclusive')

def use_high_pt_bin(df):
    # Drop off current high pT bins
    df = df[df["pt_bin"] < 7]
    join_df = pd.read_csv("data/efficiency_w_g100.csv")
    join_df["pt_range"] = "100 < pt < 1500"
    join_df["pt_bin"] = "7"
    columns={"eta":"eta_bin",
            "signal_efficiency" : "NN_nonRW_sig_eff",
            "background_rejection": "NN_nonRW_bkg_rej"}

    return_df = pd.merge(df, join_df, on="eta_bin")




def pivot_df(df, value_col):
    """pivots DF, just maintating the values we want
    Arguments:
        df {pd.DataFrame} -- Original DF, with at least 'pt_bin', 'eta_bin','dRej
        value_col {string} -- What column to make the Z value
    Returns:
        [np.array] -- pivoted df
    """
    df['pt_bin'] = pd.to_numeric(df['pt_bin'])
    df['eta_bin'] = pd.to_numeric(df['eta_bin'])
    return df.pivot_table(columns='eta_bin',index='pt_bin',values=value_col)#.values


def generate_mask(plottable_df, original_df, event_threshold):
    """Generates mask for sns heatmap based, removing nans and low event bins
    Arguments:
        plottable_df {np.array} -- pivoted plottable df with only 'pt_bin', 'eta_bin', 'dRej'
        original_df {pd.DataFrame} -- Needs columns pt_bin, eta_bin, sig_events, bkg_events
        event_threshold {int} -- Min number of bkg events to plot
    Returns:
        [np.array] -- mask to apply to seaborn heatmap
    """
    ptbins = list(plottable_df.index)
    etabins = list(plottable_df.columns)
    allcombs = [str(x)+str(y) for x in etabins for y in ptbins]
    original_df["etapt"] = original_df["eta_bin"].astype(str)+original_df["pt_bin"].astype(str)

    original_df = original_df[original_df["etapt"].isin(allcombs)]
    signal_events_matrix = original_df.pivot('pt_bin','eta_bin','sig_entries').values
    background_events_matrix = original_df.pivot('pt_bin','eta_bin','bkg_entries').values
    mask = np.logical_or(np.isnan(plottable_df), np.where(np.nan_to_num(signal_events_matrix,0) < event_threshold,True,False))
    mask = np.logical_or(mask, np.where(np.nan_to_num(background_events_matrix,0) < event_threshold,True,False))

    #original_df.pivot('pt_bin','eta_bin','sig_events').values
    return mask

def make_heatmap(pivoted_array, mask):
    #plt.figure(figsize=(12,12))
    maxval = np.nanmax(pivoted_array)
    sns.heatmap(pivoted_array, annot=True, cmap='bwr', vmin=-100,vmax=100)#, vmax=maxval)
    #sns.heatmap(pivoted_array, mask=mask, annot=True, cmap='Purples', vmin=0, vmax=maxval)
    eta_slice = eta_labels[pivoted_array.columns.astype(int).min() : pivoted_array.columns.astype(int).max()+1]
    pt_slice = pt_labels[pivoted_array.index.astype(int).min()-1 : pivoted_array.index.astype(int).max()]

    plt.ylim(0, len(pt_slice))
    plt.gca().set_xticklabels(eta_slice,rotation=90)
    plt.gca().set_yticklabels(pt_slice,rotation=0)

    # This assumes consistent working point
    wp = 0.85
    #conversion = name.split('_')[-1].capitalize()
    conversion = 'Unconverted'
    hfont = {'fontname':'Helvetica'}

    plt.annotate('{0:g}% Signal Efficiency'.format(wp*100), xy=(.005,.04),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.annotate('{0} Photons'.format(conversion), xy=(.005,.07),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    #rc('text', usetex=True)
    ##plt.annotate(r"\textit{\textbf{ATLAS}} Internal", xy=(.005,.10),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    #rc('text', usetex=False)

    plt.xlabel('')
    plt.ylabel('')
    #add_watermark()
    plt.tight_layout()

if __name__ == "__main__":

    # input files
    #full_df =  pd.read_csv('data/efficiency_w_ptflat_nn.csv')
    #full_df =  pd.read_csv('data/efficiency_w_ptinclusive_nn.csv')
    full_df =  pd.read_csv('data/full_results.csv')


    # Separate
    #df_cuts_unconverted = isolate_conversion_pt(df_cuts, conversion='Unconverted', pt_inclusive=False)
    df_bdt_unconverted = isolate_conversion_pt(full_df, conversion='Unconverted', pt_inclusive=False)
    #df_cuts_converted = isolate_conversion_pt(df_cuts, conversion='Converted', pt_inclusive=False)
    #df_bdt_converted = isolate_conversion_pt(df_bdt, conversion='Converted', pt_inclusive=False)

    df_bdt_unconverted = df_bdt_unconverted[df_bdt_unconverted['pt_bin'] != 'Inclusive']
    #df_bdt_unconverted.sort_values('pt_bin')
    add_gains(df_bdt_unconverted)

    # Plot For nEvents
    hfont = {'fontname':'Helvetica'}

    fig = plt.figure(figsize=(12,8))
    pivot_array = pivot_df(df_bdt_unconverted, 'bkg_entries')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 0)
    make_heatmap(pivot_array, mask)
    plt.annotate('Number of Bkg Events', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/bkg_events.pdf')
    plt.close()

    fig = plt.figure(figsize=(12,8))
    pivot_array = pivot_df(df_bdt_unconverted, 'sig_entries')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 0)
    make_heatmap(pivot_array, mask)
    plt.annotate('Number of Signal Events', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/sig_events.pdf')
    plt.close()

    #df_bdt_unconverted = df_bdt_unconverted[df_bdt_unconverted['pt_bin'] < 7]

    # Topo Cluster
    pivot_array = pivot_df(df_bdt_unconverted, 'dRej_cuts')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 500)
    make_heatmap(pivot_array, mask)
    plt.annotate('Absolute Gain\nAdding Topo Clusters\n Cuts Method', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/cutsTopo_v_cuts.pdf')
    plt.close()
    rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_cuts_norm')
    make_heatmap(rel_pivot_array, mask)
    plt.annotate('Relative Gain\nAdding Topo Clusters\nCuts Method', xy=(.005,.15),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/cutsTopo_v_cuts_norm.pdf')
    plt.close()

    pivot_array = pivot_df(df_bdt_unconverted, 'dRej_bdt')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 500)
    make_heatmap(pivot_array, mask)
    plt.annotate('Absolute Gain\nAdding Topo Clusters\n Cuts Method', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDTtopo_v_bdt.pdf')
    plt.close()
    rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_bdt_norm')
    make_heatmap(rel_pivot_array, mask)
    plt.annotate('Relative Gain\nAdding Topo Clusters\nBDT Method', xy=(.005,.15),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDTtopo_v_bdt_norm.pdf')
    plt.close()


    # Plot For BDT
    pivot_array = pivot_df(df_bdt_unconverted, 'dRej_bdt2cut')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 500)
    make_heatmap(pivot_array, mask)
    plt.annotate('BDT Gain wrt to Cuts', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDT_v_cuts.pdf')
    plt.close()
    rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_bdt2cut_norm')
    make_heatmap(rel_pivot_array, mask)
    plt.annotate('BDT Relative Gain\nwrt to Cuts', xy=(.005,.15),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDT_v_cuts_norm.pdf')
    plt.close()


    # Plot For NN
    pivot_array = pivot_df(df_bdt_unconverted, 'dRej_nn2cut')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 500)
    make_heatmap(pivot_array, mask)
    plt.annotate('NN Gain wrt to Cuts', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/NN_v_cuts.pdf')
    plt.close()
    rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_nn2cut_norm')
    make_heatmap(rel_pivot_array, mask)
    plt.annotate('NN Relative Gain\nwrt to Cuts', xy=(.005,.15),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/NN_v_cuts_normed.pdf')
    plt.close()

    # Plot For NN
    pivot_array = pivot_df(df_bdt_unconverted, 'dRej_nn2bdt')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 500)
    make_heatmap(pivot_array, mask)
    plt.annotate('NN Gain wrt to BDT', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/NN_v_BDT.pdf')
    plt.close()
    rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_nn2cut_norm')
    make_heatmap(rel_pivot_array, mask)
    plt.annotate('NN Relative Gain\nwrt to BDT', xy=(.005,.15),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/NN_v_BDT_normed.pdf')
    plt.close()

    # Plot For NN
    pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDT_inclusive')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 500)
    make_heatmap(pivot_array, mask)
    plt.annotate('Absolute Gain\nInclusive to Binned\n(BDT)', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDTInclusive_v_BDT.pdf')
    plt.close()
    rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDT_inclusive_norm')
    make_heatmap(rel_pivot_array, mask)
    plt.annotate('Relative Gain\nInclusive to Binned\n(BDT)', xy=(.005,.15),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDTInclusive_v_BDT_normed.pdf')
    plt.close()


    # Plot For NN
    pivot_array = pivot_df(df_bdt_unconverted, 'dRej_NN_inclusive')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 500)
    make_heatmap(pivot_array, mask)
    plt.annotate('Absolute Gain\nInclusive to Binned\n(NN)', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/NNinclusive_v_NN.pdf')
    plt.close()
    rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_NN_inclusive_norm')
    make_heatmap(rel_pivot_array, mask)
    plt.annotate('Relative Gain\nInclusive to Binned\n(NN)', xy=(.005,.15),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/NNinclusive_v_NN_normed.pdf')
    plt.close()

    # Plot For High pT model
    pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDT_high')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 500)
    mask = generate_mask(pivot_array, df_bdt_unconverted, 0)
    make_heatmap(pivot_array, mask)
    #pdb.set_trace()
    plt.annotate('Absolute Gain\nHigh pT Model\nto Binned (BDT)', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDThigh_v_BDT.pdf')
    plt.close()
    rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDT_high_norm')
    make_heatmap(rel_pivot_array, mask)
    plt.annotate('Relative Gain\nHigh pT Model\nto Binned (BDT)', xy=(.005,.15),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDThigh_v_BDT_normed.pdf')
    plt.close()


    # Inclusive BDT vs cuts
    pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusive_v_cuts')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 500)
    mask = generate_mask(pivot_array, df_bdt_unconverted, 0)
    make_heatmap(pivot_array, mask)
    plt.annotate('Absolute Gain\nInclusive BDT\nto Cuts', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDTinclusive_v_cuts.pdf')
    plt.close()
    rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusive_v_cuts_norm')
    make_heatmap(rel_pivot_array, mask)
    plt.annotate('Relative Gain\nInclusive BDT\nto Cuts', xy=(.005,.15),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDTinclusive_v_cuts_normed.pdf')
    plt.close()

    # Inclusive BDT vs High model
    pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusive_v_BDThigh')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 0)
    make_heatmap(pivot_array, mask)
    plt.annotate('Absolute Gain\nInclusive BDT\nto pT > 80 GeV Model', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDTinclusive_v_BDThigh.pdf')
    plt.close()
    rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusive_v_BDThigh_norm')
    make_heatmap(rel_pivot_array, mask)
    plt.annotate('Relative Gain\nInclusive BDT\nto pT > 80 GeV Model', xy=(.005,.15),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDTinclusive_v_BDThigh_normed.pdf')
    plt.close()

    # Inclusive BDT vs NN
    pivot_array = pivot_df(df_bdt_unconverted, 'dRej_NN_v_BDTinclusive')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 0)
    make_heatmap(pivot_array, mask)
    plt.annotate('Absolute Gain\nNN to Inclusive BDT', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/NN_v_BDTinclusive.pdf')
    plt.close()
    rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_NN_v_BDTinclusive_norm')
    make_heatmap(rel_pivot_array, mask)
    plt.annotate('Relative Gain\nNN to Inclusive BDT', xy=(.005,.15),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/NN_v_BDTinclusive_normed.pdf')
    plt.close()

    # Inclusive BDT vs Inclusive NN
    pivot_array = pivot_df(df_bdt_unconverted, 'dRej_NNinclusive_v_BDTinclusive')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 0)
    make_heatmap(pivot_array, mask)
    plt.annotate('Absolute Gain\nInclusive NN\nto Inclusive BDT', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/NNinclusive_v_BDTinclusive.pdf')
    plt.close()
    rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_NNinclusive_v_BDTinclusive_norm')
    make_heatmap(rel_pivot_array, mask)
    plt.annotate('Relative Gain\nInclusive NN\nto Inclusive BDT', xy=(.005,.15),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/NNinclusive_v_BDTinclusive_normed.pdf')
    plt.close()

    # Just rejection Values
    pivot_array = pivot_df(df_bdt_unconverted, 'BDT_unRW_bkg_rej')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 500)
    make_heatmap(pivot_array, mask)
    plt.annotate('BDT Inclusive Bkg Rejection', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDTinclusive_background_rejection.pdf')
    plt.close()
    pivot_array = pivot_df(df_bdt_unconverted, 'NN_nonRW_bkg_rej')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 500)
    make_heatmap(pivot_array, mask)
    plt.annotate('NN Inclusive Bkg Rejection', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/NNinclusive_background_rejection.pdf')
    plt.close()

    pivot_array = pivot_df(df_bdt_unconverted, 'bdt_nominal_bkg_rej')
    pivot_array[pivot_array==100]=np.nan
    pivot_array[pivot_array==-100]=np.nan
    mask = generate_mask(pivot_array, df_bdt_unconverted, 500)
    make_heatmap(pivot_array, mask)
    plt.annotate('BDT Inclusive Bkg Rejection', xy=(.005,.13),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    plt.savefig('run/gain/BDT_rejection.pdf')
    plt.close()
