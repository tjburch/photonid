from rootpy.io import root_open
import rootpy.plotting.root2matplotlib as rplt
import argparse
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from standard_functions import add_watermark
from rootpy.plotting.style import get_style, set_style
from labels import topo_refs

def hist_titles_1d(histogram):
    """Pass a 1d histogram, references against a dictionary and returns correct title (xlabel)

    Arguments:
        histogram {Hist} -- 1d histogram in rootpy

    Returns:
        [String] -- Title (xlabel)
    """
    other_refs = {
    "y_Rhad1" : "y_Rhad1",
    "y_weta1" : "y_weta1",
    "y_weta2" : "y_weta2",
    "y_Reta" : "y_Reta",
    "y_deltae" : "y_deltae",
    "y_Rphi" : "y_Rphi",
    "y_Eratio" : "y_Eratio",
    "y_wtots1" : "y_wtots1",
    "y_fracs" : "y_fracs",
    }
    refs = {**topo_refs , **other_refs}
    # Add keys for leading, match the originals
    return_dict = {}
    for key in refs:
        return_dict[key] = refs[key]
        return_dict[key.replace("topoCluster","topoCluster0")] = refs[key]

    key_value = histogram.GetTitle()
    return return_dict[key_value]

def hist_titles_2d(histogram):
    """Pass a 1d histogram, references against a dictionary and returns correct x and y labels

    Arguments:
        histogram {Hist2D} -- rootpy 2d histogram

    Returns:
        [list] -- [xlabel, ylabel]
    """
    refs = {
    "isolation_v_nClusters" : ["Number of Topo Clusters", "Isolation"],
    "secondLambda_v_secondR" : ["$< r^{2} >$", "$< \lambda^{2} >$"]
    }
    key_value = histogram.GetTitle()
    return refs[key_value]


def find_hist_match(hist, second_file):
    name = hist.GetName()
    for obj in second_file:
        if obj.GetName() == name:
            return obj
    pass



if __name__=='__main__':
    parser = argparse.ArgumentParser('Generates TH objects from single photon nTuples')
    parser.add_argument('-s', dest='signalFile', required=True, help='Signal')
    parser.add_argument('-b', dest='backgroundFile', required=False, help='Background')
    parser.add_argument('-o', dest='outputFolder', required=True, help='Output folder')
    args = parser.parse_args()

    style = get_style('ATLAS')
    set_style(style)

    signal_file = root_open(args.signalFile)
    labels = ['$\gamma$+jets']
    if args.backgroundFile: backgroundFile = root_open(args.backgroundFile)


    for obj in signal_file:
        print(obj)
        if 1/obj.Integral() == 0: continue

        try:
            # case for Hist
            obj.Scale(1/obj.Integral())
            rplt.errorbar(obj, markersize=0)

            if args.backgroundFile:
                bkg_obj = find_hist_match(obj, backgroundFile)
                bkg_obj.linecolor = "firebrick"
                bkg_obj.Scale(1/bkg_obj.Integral())

                rplt.errorbar(bkg_obj, markersize=0)
                labels.append('jet-fakes')

            plt.xlabel(hist_titles_1d(obj))
            plt.ylabel("AU (normalized to 1)")
            #add_watermark()
            leg = plt.legend(labels)

            plt.savefig(args.outputFolder+obj.GetTitle())
            plt.close()

        # TODO : Fix this for two files!!!
        except TypeError:
             # case for Hist2D
             im = rplt.imshow(obj, norm=LogNorm())
             titles =  hist_titles_2d(obj)
             plt.xlabel(titles[0])
             plt.ylabel(titles[1])
             plt.colorbar(im)
             plt.savefig(args.outputFolder+obj.GetTitle())
             plt.close()
