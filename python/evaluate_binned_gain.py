print("Loading numerical tools")
import pandas as pd
import numpy as np
import seaborn as sns
print("Loading plotting tools")
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
from matplotlib import rc

print("Loading internal functions")
from evaluate_inclusive_gain import pt_labels, eta_labels, add_cols_for_method, isolate_conversion_pt
from standard_functions import add_watermark
import logging 
print("Setting Style")
logging.getLogger("matplotlib").setLevel(logging.WARNING)
set_style(get_style('ATLAS'))
import pdb

# Globally defined labels
eta_labels = [
    '$0 < |\eta| < 0.6$',
    '$0.6 <|\eta| < 0.8$',
    '$0.8 < |\eta|< 1.15$',
    '$1.15 < |\eta|< 1.37$',
    '$1.52 < |\eta|< 1.81$',
    '$1.81 < |\eta|< 2.01$',
    '$2.01 < |\eta| < 2.37$',
]
pt_labels = [
    '$25 < p_{T} < 30 $',
    '$30 < p_{T} < 40 $',
    '$40 < p_{T} < 50 $',
    '$50 < p_{T} < 60 $',
    '$60 < p_{T} < 80 $',
    '$80 < p_{T} < 100 $',
    #'$100 < p_{T} < 1500 $',
    #'$100 < p_{T} < 125 $',
    #'$125 < p_{T} < 150 $',
    #'$150 < p_{T} < 175 $',
    #'$175 < p_{T} < 250 $',
    #'$250 < p_{T} < 500 $',
    #'$500 < p_{T} < 1500 $',
]

newFormat = False

def add_gains(df):

    if not newFormat:
        skip = True
        if not skip:
            add_cols_for_method(df, 'cuts_bkg_rej', 'cuts_topo_bkg_rej', 'cuts')
            add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'bdt_topo_bkg_rej','bdt')
            #add_cols_for_method(df, 'nn_bkg_rej', 'nn_topo_bkg_rej','nn')

            # Method to Method
            add_cols_for_method(df, 'cuts_bkg_rej', 'bdt_nominal_bkg_rej', 'bdt2cut')
            add_cols_for_method(df, 'cuts_bkg_rej', 'nn_bkg_rej', 'nn2cut')
            add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'nn_bkg_rej', 'nn2bdt')

            # Full gains
            add_cols_for_method(df, 'cuts_bkg_rej', 'bdt_topo_bkg_rej', 'bdt2base')
            #add_cols_for_method(df, 'cuts_bkg_rej', 'nn_topo_bkg_rej', 'nn2base')

            # reweighted
            add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'BDT_RW_bkg_rej','BDT')
            add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'NN_RW_bkg_rej','NN_nonRW_bkg_rej')

            # Reweighted vs trained non-inclusively
            #    add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'BDT_unRW_bkg_rej','BDT_inclusive')

            add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'BDT_unRW_bkg_rej','BDT_inclusive')
            add_cols_for_method(df, 'nn_bkg_rej', 'NN_nonRW_bkg_rej','NN_inclusive')
            

            add_cols_for_method(df, 'cuts_bkg_rej', 'BDT_unRW_bkg_rej', 'BDTinclusive_v_cuts')
            add_cols_for_method(df, 'BDT_high_bkg_rej', 'BDT_unRW_bkg_rej', 'BDTinclusive_v_BDThigh')
            add_cols_for_method(df, 'BDT_unRW_bkg_rej', 'nn_bkg_rej', 'NN_v_BDTinclusive')
            
            add_cols_for_method(df, 'BDT_unRW_bkg_rej', 'NN_nonRW_bkg_rej', 'NNinclusive_v_BDTinclusive')
        add_cols_for_method(df, 'updated_cuts_bkg_rej', 'topo_inclusive_bkg_rej', 'BDTinclusiveTopo_v_cuts')

    else:
        add_cols_for_method(df, 'updated_cuts_bkg_rej', 'topo_inclusive_bkg_rej', 'BDTinclusiveTopo_v_cuts')
        add_cols_for_method(df, 'inclusive_bkg_rej', 'topo_inclusive_bkg_rej', 'BDTinclusiveTopo_v_BDTinclusive')
        add_cols_for_method(df, 'updated_cuts_bkg_rej', 'cuts_bkg_rej', 'cutsdiff')
        
        #add_cols_for_method(df, 'updated_cuts_bkg_rej', 'updated_cuts_topo_bkg_rej', 'cutsTopo_v_cuts')
        #add_cols_for_method(df, 'updated_cuts_conv_bkg_rej', 'updated_cuts_topo_conv_bkg_rej', 'cutsTopo_v_cuts_conv')

        #add_cols_for_method(df, 'inclusive_bkg_rej', 'topo_inclusive_reweight_bkg_rej', 'BDTinclusiveTopoRW_v_BDTinclusive')
        #add_cols_for_method(df, 'inclusive_bkg_rej', 'bdt_topo_bkg_rej', 'BDTtopo_v_BDTinclusive')
        #add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'inclusive_bkg_rej', 'BDTinclusive_v_BDT')

def add_gains_conv(df):

    add_cols_for_method(df, 'cuts_bkg_rej', 'cuts_topo_bkg_rej', 'cuts')
    add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'bdt_topo_bkg_rej','bdt')



def pivot_df(df, value_col):
    """pivots DF, just maintating the values we want    
    Arguments:
        df {pd.DataFrame} -- Original DF, with at least 'pt_bin', 'eta_bin','dRej    
        value_col {string} -- What column to make the Z value
    Returns:
        [np.array] -- pivoted df
    """
    df['pt_bin'] = pd.to_numeric(df['pt_bin'])
    df['eta_bin'] = pd.to_numeric(df['eta_bin'])
    return df.pivot_table(columns='eta_bin',index='pt_bin',values=value_col)#.values


def generate_mask(plottable_df, original_df, event_threshold):
    """Generates mask for sns heatmap based, removing nans and low event bins    
    Arguments:
        plottable_df {np.array} -- pivoted plottable df with only 'pt_bin', 'eta_bin', 'dRej'
        original_df {pd.DataFrame} -- Needs columns pt_bin, eta_bin, sig_events, bkg_events
        event_threshold {int} -- Min number of bkg events to plot    
    Returns:
        [np.array] -- mask to apply to seaborn heatmap
    """
    ptbins = list(plottable_df.index)
    etabins = list(plottable_df.columns)
    allcombs = [str(x)+str(y) for x in etabins for y in ptbins]
    original_df["etapt"] = original_df["eta_bin"].astype(str)+original_df["pt_bin"].astype(str)

    original_df = original_df[original_df["etapt"].isin(allcombs)]
    #original_df = original_df.reset_index()
    signal_events_matrix = original_df.pivot('pt_bin','eta_bin','sig_entries').values
    background_events_matrix = original_df.pivot('pt_bin','eta_bin','bkg_entries').values
    #pdb.set_trace()
    mask = np.logical_or(np.isnan(plottable_df), np.where(np.nan_to_num(signal_events_matrix,0) < event_threshold,True,False))
    mask = np.logical_or(mask, np.where(np.nan_to_num(background_events_matrix,0) < event_threshold,True,False))
    
    #original_df.pivot('pt_bin','eta_bin','sig_events').values
    return mask

def make_heatmap(pivoted_array, mask, conversion="Unconverted"):
    #plt.figure(figsize=(12,12))
    maxval = np.nanmax(pivoted_array)
    ax = sns.heatmap(pivoted_array, annot=True, cmap='bwr', vmin=-50,vmax=50, mask=mask)#, vmax=maxval)
    #ax = sns.heatmap(pivoted_array, mask=mask, annot=True, fmt='g', cmap='Purples', vmin=0, vmax=maxval, annot_kws={"size": 14})
    cbar = ax.collections[0].colorbar
    cbar.set_label('$Z_{\mathrm{rel}}$ [%]', rotation=270, fontsize=16,labelpad=18)
    eta_slice = eta_labels[pivoted_array.columns.astype(int).min() : pivoted_array.columns.astype(int).max()+1]
    pt_slice = pt_labels[pivoted_array.index.astype(int).min()-1 : pivoted_array.index.astype(int).max()]

    plt.ylim(0, len(pt_slice))
    plt.gca().set_xticklabels(eta_slice,rotation=75, fontsize=10)
    plt.gca().set_yticklabels(pt_slice,rotation=0,fontsize=10)


    # This assumes consistent working point
    wp = 0.85
    #conversion = name.split('_')[-1].capitalize()
    #conversion = 'Unconverted'
    hfont = {'fontname':'Helvetica'}

    plt.annotate('{0:g}% Signal Efficiency'.format(wp*100), xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    #plt.annotate('{0} Photons'.format(conversion), xy=(.006,.01),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
    #rc('text', usetex=True)
    #plt.annotate(r"\textit{\textbf{ATLAS}} Internal", xy=(.005,.07),xycoords='figure fraction', horizontalalignment='left',fontsize=14,  **hfont)
    #rc('text', usetex=False)

    #Event Labels
    #cbar.set_label('Number of Events', rotation=270, fontsize=16,labelpad=20)
    plt.annotate('{0} Photons'.format(conversion), xy=(.006,.02),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)


    plt.xlabel('')
    plt.ylabel('')
    #add_watermark()
    plt.tight_layout()

if __name__ == "__main__":
    
    # input files
    #full_df =  pd.read_csv('data/efficiency_w_ptflat_nn.csv')    
    #full_df =  pd.read_csv('data/efficiency_w_ptinclusive_nn.csv')  
    print ("loading csv")
    full_df =  pd.read_csv('data/full_results.csv')    
    conv_df =  pd.read_csv('data/bdt_roc_efficiency2020.csv')
    conv_df = conv_df[conv_df["conversion"] == "Converted"]

    # Separate
    #df_cuts_unconverted = isolate_conversion_pt(df_cuts, conversion='Unconverted', pt_inclusive=False)
    df_bdt_unconverted = isolate_conversion_pt(full_df, conversion='Unconverted', pt_inclusive=False)
    df_cuts_converted = isolate_conversion_pt(conv_df, conversion='Converted', pt_inclusive=False)
    #df_bdt_converted = isolate_conversion_pt(df_bdt, conversion='Converted', pt_inclusive=False)

    df_bdt_unconverted = df_bdt_unconverted[df_bdt_unconverted['pt_bin'].astype(str) != 'Inclusive']
    df_bdt_unconverted = df_bdt_unconverted[df_bdt_unconverted['pt_bin'] < 7]

    df_cuts_converted = df_cuts_converted[df_cuts_converted['pt_bin'].astype(str) != 'Inclusive']
    df_cuts_converted = df_cuts_converted[df_cuts_converted['pt_bin'] < 7]


    df_bdt_unconverted.sort_values('pt_bin')
    print ("Adding cols")

    add_gains(df_bdt_unconverted)
    add_gains_conv(df_cuts_converted)
    

    # Plot For nEvents
    hfont = {'fontname':'Helvetica'}
    

    if not newFormat: 
        skip = True
        if skip:
            # TODO : Flipping the formatting around on these... requires independent run - find a way to streamline
            fig = plt.figure(figsize=(12,8))
            pivot_array = pivot_df(df_bdt_unconverted, 'bkg_entries')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 0)
            make_heatmap(pivot_array, mask)
            plt.annotate('Background Events', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=14,  **hfont)
            plt.savefig('run/gain/bkg_events.pdf')
            plt.close()

            fig = plt.figure(figsize=(12,8))
            pivot_array = pivot_df(df_bdt_unconverted, 'sig_entries')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 0)
            make_heatmap(pivot_array, mask)
            plt.annotate('Signal Events', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=14,  **hfont)
            plt.savefig('run/gain/sig_events.pdf')
            plt.close()


            fig = plt.figure(figsize=(12,8))
            pivot_array = pivot_df(df_cuts_converted, 'bkg_entries')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_cuts_converted, 0)
            make_heatmap(pivot_array, mask, conversion="Converted")
            plt.annotate('Background Events', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=14,  **hfont)
            plt.savefig('run/gain/conv_bkg_events.pdf')
            plt.close()

            fig = plt.figure(figsize=(12,8))
            pivot_array = pivot_df(df_cuts_converted, 'sig_entries')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_cuts_converted, 0)
            make_heatmap(pivot_array, mask, conversion="Converted")
            plt.annotate('Signal Events', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=14,  **hfont)
            plt.savefig('run/gain/conv_sig_events.pdf')
            plt.close()

            
            print("DONE")

            # Topo Cluster
            pivot_array = pivot_df(df_bdt_unconverted, 'dRej_cuts')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            make_heatmap(pivot_array, mask)
            plt.annotate('Absolute Gain\nAdding Topo-Clusters\nCuts Method', xy=(.005,.07),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/cutsTopo_v_cuts.pdf')
            plt.close()
            rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_cuts_norm')
            make_heatmap(rel_pivot_array, mask)
            plt.annotate('Relative Gain\nAdding Topo-Clusters\nCuts Method', xy=(.005,.07),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/cutsTopo_v_cuts_norm.pdf')
            plt.close()


            #TJB new
            print("Adding converted")
            pivot_array = pivot_df(df_cuts_converted, 'dRej_cuts')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_cuts_converted, 400)
            rel_pivot_array = pivot_df(df_cuts_converted, 'dRej_cuts_norm')
            make_heatmap(rel_pivot_array, mask,conversion="Converted")
            plt.annotate('Relative Gain\nAdding Topo-Clusters\nCuts Method', xy=(.005,.04),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain_converted/cutsTopo_v_cuts_norm.pdf')
            plt.close()        
            print("done_converted")


            pivot_array = pivot_df(df_bdt_unconverted, 'dRej_bdt')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            make_heatmap(pivot_array, mask)
            plt.annotate('Absolute Gain\nAdding Topo \nClusters to\nBDT Method', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/BDTtopo_v_bdt.pdf')
            plt.close()
            rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_bdt_norm')
            make_heatmap(rel_pivot_array, mask)
            plt.annotate('Relative Gain\nAdding Topo\nClusters to\nBDT Method', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/BDTtopo_v_bdt_norm.pdf')
            plt.close()    

            pivot_array = pivot_df(df_bdt_unconverted, 'dRej_bdt2base')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            make_heatmap(pivot_array, mask)
            plt.annotate('Absolute Gain\nBDT w/topo\nto Cuts', xy=(.005,.07),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/BDTtopo_v_cuts.pdf')
            plt.close()
            rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_bdt2base_norm')
            make_heatmap(rel_pivot_array, mask)
            plt.annotate('Relative Gain\nBDT w/topo\nto Cuts', xy=(.005,.08),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/BDTtopo_v_cuts_normed.pdf')
            plt.close()

            # Plot For BDT
            pivot_array = pivot_df(df_bdt_unconverted, 'dRej_bdt2cut')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            make_heatmap(pivot_array, mask)
            plt.annotate('BDT Gain wrt to Cuts', xy=(.005,.07),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/BDT_v_cuts.pdf')
            plt.close()
            rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_bdt2cut_norm')
            make_heatmap(rel_pivot_array, mask)
            plt.annotate('BDT Relative Gain\nwrt to Cuts', xy=(.005,.07),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/BDT_v_cuts_norm.pdf')
            plt.close()


            # Plot For NN
            pivot_array = pivot_df(df_bdt_unconverted, 'dRej_nn2cut')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            make_heatmap(pivot_array, mask)
            plt.annotate('NN Gain wrt to Cuts', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/NN_v_cuts.pdf')
            plt.close()
            rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_nn2cut_norm')
            make_heatmap(rel_pivot_array, mask)
            plt.annotate('NN Relative Gain\nwrt to Cuts', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/NN_v_cuts_normed.pdf')
            plt.close()

            # Plot For NN
            pivot_array = pivot_df(df_bdt_unconverted, 'dRej_nn2bdt')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            make_heatmap(pivot_array, mask)
            #plt.annotate('NN Gain wrt to BDT', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            #plt.savefig('run/gain/NN_v_BDT.pdf')
            #plt.close()
            rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_nn2cut_norm')
            make_heatmap(rel_pivot_array, mask)
            plt.annotate('NN Relative Gain\nwrt to BDT', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/NN_v_BDT_normed.pdf')
            plt.close()


            # Plot For NN
            pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDT_inclusive')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            #make_heatmap(pivot_array, mask)
            #plt.annotate('Absolute Gain\nInclusive to Binned\n(BDT)', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            #plt.savefig('run/gain/BDTInclusive_v_BDT.pdf')
            #plt.close()
            rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDT_inclusive_norm')
            make_heatmap(rel_pivot_array, mask)
            plt.annotate('Relative Gain\nInclusive to Binned\n(BDT)', xy=(.005,.08),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/BDTInclusive_v_BDT_normed.pdf')
            plt.close()

            # Plot For NN
            pivot_array = pivot_df(df_bdt_unconverted, 'dRej_NN_inclusive')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            make_heatmap(pivot_array, mask)
            plt.annotate('Absolute Gain\nInclusive to Binned\n(NN)', xy=(.005,.08),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/NNinclusive_v_NN.pdf')
            plt.close()
            rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_NN_inclusive_norm')
            make_heatmap(rel_pivot_array, mask)
            plt.annotate('Relative Gain\nInclusive to Binned\n(NN)', xy=(.005,.08),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/NNinclusive_v_NN_normed.pdf')
            plt.close()
        
            # Inclusive BDT vs cuts
            pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusive_v_cuts')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            #make_heatmap(pivot_array, mask)
            #plt.annotate('Absolute Gain\nInclusive BDT\nto Cuts', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            #plt.savefig('run/gain/BDTinclusive_v_cuts.pdf')
            #plt.close()
            rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusive_v_cuts_norm')
            make_heatmap(rel_pivot_array, mask)
            plt.annotate('Relative Gain\nInclusive BDT to Cuts', xy=(.005,.07),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/BDTinclusive_v_cuts_normed.pdf')
            plt.close()
            print("TJB")


            # Inclusive BDT vs NN
            pivot_array = pivot_df(df_bdt_unconverted, 'dRej_NN_v_BDTinclusive')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            make_heatmap(pivot_array, mask)
            plt.annotate('Absolute Gain\nNN to Inclusive BDT', xy=(.005,.08),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/NN_v_BDTinclusive.pdf')
            plt.close()
            rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_NN_v_BDTinclusive_norm')
            make_heatmap(rel_pivot_array, mask)
            plt.annotate('Relative Gain\nNN to Inclusive BDT', xy=(.005,.08),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/NN_v_BDTinclusive_normed.pdf')
            plt.close()

            # Inclusive BDT vs Inclusive NN
            pivot_array = pivot_df(df_bdt_unconverted, 'dRej_NNinclusive_v_BDTinclusive')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            make_heatmap(pivot_array, mask)
            plt.annotate('Absolute Gain\nInclusive NN\nto Inclusive BDT', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/NNinclusive_v_BDTinclusive.pdf')
            plt.close()
            rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_NNinclusive_v_BDTinclusive_norm')
            make_heatmap(rel_pivot_array, mask)
            plt.annotate('Relative Gain\nInclusive NN\nto Inclusive BDT', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/NNinclusive_v_BDTinclusive_normed.pdf')
            plt.close()

            # Just rejection Values
            pivot_array = pivot_df(df_bdt_unconverted, 'BDT_unRW_bkg_rej')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            make_heatmap(pivot_array, mask)
            plt.annotate('BDT Inclusive Bkg Rejection', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/BDTinclusive_background_rejection.pdf')
            plt.close()
            pivot_array = pivot_df(df_bdt_unconverted, 'NN_nonRW_bkg_rej')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            make_heatmap(pivot_array, mask)
            plt.annotate('NN Inclusive Bkg Rejection', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/NNinclusive_background_rejection.pdf')
            plt.close()

            pivot_array = pivot_df(df_bdt_unconverted, 'bdt_nominal_bkg_rej')
            pivot_array[pivot_array==100]=np.nan
            pivot_array[pivot_array==-100]=np.nan
            mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
            make_heatmap(pivot_array, mask)
            plt.annotate('BDT Inclusive Bkg Rejection', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
            plt.savefig('run/gain/BDT_rejection.pdf')
            plt.close()

        print("TJB here")
        pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusiveTopo_v_cuts')
        pivot_array[pivot_array==100]=np.nan
        pivot_array[pivot_array==-100]=np.nan
        mask = generate_mask(pivot_array, df_bdt_unconverted, 0)
        #make_heatmap(pivot_array, mask)
        #plt.annotate('Absolute Gain\nInclusive BDT w/Topo\nto Cuts Method', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=12,  **hfont)
        #plt.savefig('run/gain/BDTinclusiveTopo_v_cuts.pdf')
        #plt.close()

        rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusiveTopo_v_cuts_norm')
        make_heatmap(rel_pivot_array, mask)
        plt.annotate('Relative Gain\nInclusive BDT w/Topo\nto Cuts Method', xy=(.005,.08),xycoords='figure fraction', horizontalalignment='left',fontsize=12,  **hfont)
        plt.savefig('run/gain/BDTinclusiveTopo_v_cuts_normed.pdf')
        plt.close()

    else: 
        df_bdt_unconverted = df_bdt_unconverted[df_bdt_unconverted['pt_bin'] < 7]

        # cuts check
        pivot_array = pivot_df(df_bdt_unconverted, 'dRej_cutsdiff')
        pivot_array[pivot_array==100]=np.nan
        pivot_array[pivot_array==-100]=np.nan
        mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
        make_heatmap(pivot_array, mask)
        plt.annotate(' ', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
        plt.savefig('run/gain/cutsdiff.pdf')
        plt.close()
        rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_cutsdiff_norm')
        make_heatmap(rel_pivot_array, mask)
        plt.annotate(' ', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=10,  **hfont)
        plt.savefig('run/gain/cutsdiff_normed.pdf')
        plt.close()

        # Inclusive cuts topo vs cuts
        #pivot_array = pivot_df(df_bdt_unconverted, 'dRej_cutsTopo_v_cuts')
        #pivot_array[pivot_array==100]=np.nan
        #pivot_array[pivot_array==-100]=np.nan
        #mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
        #make_heatmap(pivot_array, mask)
        #plt.annotate('Absolute Gain\nAdding Topo-Clusters\nCuts Method', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=9,  **hfont)
        #plt.savefig('run/gain/cutsTopo_v_cuts.pdf')
        #plt.close()
        #rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_cutsTopo_v_cuts_norm')
        #make_heatmap(rel_pivot_array, mask)
        #plt.annotate('Relative Gain\nAdding Topo-Clusters\nCuts Method', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=9,  **hfont)
        #plt.savefig('run/gain/cutsTopo_v_cuts_normed.pdf')
        #plt.close()


        # Inclusive cuts topo vs cuts
        """
        pivot_array = pivot_df(df_bdt_unconverted, 'dRej_cutsTopo_v_cuts_conv')
        pivot_array[pivot_array==100]=np.nan
        pivot_array[pivot_array==-100]=np.nan
        mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
        make_heatmap(pivot_array, mask)
        plt.annotate('Absolute Gain\nAdding Topo-Clusters\nCuts Method', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=9,  **hfont)
        plt.savefig('run/gain/cutsTopo_v_cuts_conv.pdf')
        plt.close()
        rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_cutsTopo_v_cuts_conv_norm')
        make_heatmap(rel_pivot_array, mask)
        plt.annotate('Relative Gain\nAdding Topo-Clusters\nCuts Method', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=9,  **hfont)
        plt.savefig('run/gain/cutsTopo_v_cuts_conv_normed.pdf')
        plt.close()        
        """

        # Inclusive BDT topo vs bdt
        #pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusiveTopoRW_v_BDTinclusive')
        #pivot_array[pivot_array==100]=np.nan
        #pivot_array[pivot_array==-100]=np.nan
        #mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
        #make_heatmap(pivot_array, mask)
        #plt.annotate('Absolute Gain\nInclusive BDT w/Topo\nto BDT Method', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=12,  **hfont)
        #plt.savefig('run/gain/BDTinclusiveTopoRW_v_BDT.pdf')
        #plt.close()
        #rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusiveTopoRW_v_BDTinclusive_norm')
        #make_heatmap(rel_pivot_array, mask)
        #plt.annotate('Relative Gain\nInclusive BDT w/Topo\nto BDT Method', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=12,  **hfont)
        #plt.savefig('run/gain/BDTinclusiveTopoRW_v_BDT_normed.pdf')
        #plt.close()

        #pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTtopo_v_BDTinclusive')
        #pivot_array[pivot_array==100]=np.nan
        #pivot_array[pivot_array==-100]=np.nan
        #mask = generate_mask(pivot_array, df_bdt_unconverted, 400)
        #make_heatmap(pivot_array, mask)
        #plt.annotate('Absolute Gain\nBDT w/Topo\nto BDT Inclusive', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=12,  **hfont)
        #plt.savefig('run/gain/BDTtopo_v_BDTinclusive.pdf')
        #plt.close()
        #rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTtopo_v_BDTinclusive_norm')
        #make_heatmap(rel_pivot_array, mask)
        #plt.annotate('Relative Gain\nBDT w/Topo\nto BDT Inclusive', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=12,  **hfont)
        #plt.savefig('run/gain/BDTtopo_v_BDTinclusive_normed.pdf')
        #plt.close()

        # Inclusive BDT topo vs bdt
        pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusiveTopo_v_BDTinclusive')
        pivot_array[pivot_array==100]=np.nan
        pivot_array[pivot_array==-100]=np.nan
        mask = generate_mask(pivot_array, df_bdt_unconverted, 0)
        make_heatmap(pivot_array, mask)
        plt.annotate('Absolute Gain\nInclusive BDT w/Topo\nto BDT Method', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=12,  **hfont)
        plt.savefig('run/gain/BDTinclusiveTopo_v_BDT.pdf')
        plt.close()
        rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusiveTopo_v_BDTinclusive_norm')
        make_heatmap(rel_pivot_array, mask)
        plt.annotate('Relative Gain\nInclusive BDT w/Topo\nto BDT Method', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=12,  **hfont)
        plt.savefig('run/gain/BDTinclusiveTopo_v_BDT_normed.pdf')
        plt.close()

        pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusiveTopo_v_cuts')
        pivot_array[pivot_array==100]=np.nan
        pivot_array[pivot_array==-100]=np.nan
        mask = generate_mask(pivot_array, df_bdt_unconverted, 0)
        make_heatmap(pivot_array, mask)
        plt.annotate('Absolute Gain\nInclusive BDT w/Topo\nto Cuts Method', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=12,  **hfont)
        plt.savefig('run/gain/BDTinclusiveTopo_v_cuts.pdf')
        plt.close()
        rel_pivot_array = pivot_df(df_bdt_unconverted, 'dRej_BDTinclusiveTopo_v_cuts_norm')
        make_heatmap(rel_pivot_array, mask)
        plt.annotate('Relative Gain\nInclusive BDT w/Topo\nto Cuts Method', xy=(.005,.05),xycoords='figure fraction', horizontalalignment='left',fontsize=12,  **hfont)
        plt.savefig('run/gain/BDTinclusiveTopo_v_cuts_normed.pdf')
        plt.close()


