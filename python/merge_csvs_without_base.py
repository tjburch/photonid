import pandas as pd


def fix_eta_bins(df):
    # solves 4 gap
    df['eta'] = df['eta'].where(df['eta'] < 5, df['eta'] - 1)
    return df


def format_csv(csv, prefix):
    df = pd.read_csv(csv)
    df['pt'] = df['pt'].astype(str)
    #fix_eta_bins(df)
    df = df.rename(columns={"eta":"eta_bin",
                            "pt": "pt_bin",
                            "signal_efficiency" : prefix + "_sig_eff",
                            "background_rejection": prefix+"_bkg_rej"})
    return df

def main():

    csvs_to_merge = {
        "inclusive" :"data/efficiency_w_inclusive.csv",
        "nominal" :"data/efficiency_w_bdt.csv",
        "topo" :"data/efficiency_w_topo.csv",
        "topo_inclusive" :"data/efficiency_w_inclusiveTopo.csv",
        "updated_cuts" :"data/efficiency_w_cuts.csv",
        "updated_cuts_topo" :"data/efficiency_w_cuts_topo.csv",
        "topo_inclusive_reweight" :"data/efficiency_w_ptinclusive_bdt_reweight.csv",
        "updated_cuts_conv" : "data/efficiency_w_cuts_conv.csv",
        "updated_cuts_topo_conv" : "data/efficiency_w_cuts_topo_conv.csv",
    }
    base_df = pd.DataFrame(columns=["eta_bin","pt_bin"])
    for csvtype, csv in csvs_to_merge.items():
        print("Merging {0}, type {1}".format(csv, csvtype))
        df = format_csv(csv, csvtype)
        base_df = pd.merge(base_df, df, on=["eta_bin","pt_bin"], how='outer')

    pt_map={
        "1" : "25 < pT < 30",
        "2" : "30 < pT < 40",
        "3" : "40 < pT < 50",
        "4" : "50 < pT < 60",
        "5" : "60 < pT < 80",
        "6" : "80 < pT < 100",
        "7":  "100 < pT < 1500",
    }
    base_df["pt_range"] = base_df["pt_bin"].map(pt_map)

    #base_df = pd.merge(base_df, df, on=["eta_bin","pt_bin"], how='outer')


    base_df.to_csv("data/merged_csv.csv")
    

if __name__ == "__main__":
    main()