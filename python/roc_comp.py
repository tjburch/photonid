from rootpy.io import root_open
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
import rootpy.plotting.root2matplotlib as rplt
from standard_functions import add_watermark

ROI = False
#fbase = '/afs/cern.ch/work/t/tburch/photonID/PhotonIDCutsOptimization/photonIDoptim/build/'
f1 = 'bdt_files/BDT_eta0ptInclusiveUnconverted_nominal.root'
f2 = 'bdt_files/BDT_eta0ptInclusiveUnconverted_topo.root'
#f1 = fbase+'Eta0Pt1Mu1UnconvertedMC15.root'
#f2 = fbase+'Eta0Pt1Mu1UnconvertedMC15_topo.root'

nominal = root_open(f1)
topoAdded = root_open(f2)

nominal_bdt = nominal.Get('SinglePhoton/Method_BDT/BDT/MVA_BDT_rejBvsS')
topo_bdt = topoAdded.Get('SinglePhoton/Method_BDT/BDT/MVA_BDT_rejBvsS')
#nominal_bdt = nominal.Method_Cuts.Eta0Pt1Mu1UnconvertedMC15.MVA_Eta0Pt1Mu1UnconvertedMC15_trainingRejBvsS
#topo_bdt = topoAdded.Method_Cuts.Eta0Pt1Mu1UnconvertedMC15_topo.MVA_Eta0Pt1Mu1UnconvertedMC15_topo_trainingRejBvsS

fig= plt.figure()
ax=plt.gca()

nominal_bdt.linecolor = "firebrick"
auc = nominal_bdt.Integral()
roi_auc = nominal_bdt.Integral(nominal_bdt.GetXaxis().FindBin(0.8), nominal_bdt.GetXaxis().FindBin(0.9))
nominal_label ='SS-only:\n    AUC=%.2f\n    ROI=%.2f' % (auc, roi_auc) 


topo_bdt.linecolor = "cornflowerblue"
auc = topo_bdt.Integral()
roi_auc = topo_bdt.Integral(topo_bdt.GetXaxis().FindBin(0.8), topo_bdt.GetXaxis().FindBin(0.9))
topo_label = 'Topo-Added:\n    AUC=%.2f\n    ROI=%.2f' % (auc, roi_auc)

curves = [nominal_bdt]
rplt.hist(nominal_bdt, markersize=0, label=nominal_label ,fill=False)
rplt.hist(topo_bdt, markersize=0, label=topo_label, fill=False)
#rplt.hist(curves, stacked=False, markersize=0)

plt.xlabel('Signal Efficiency')
plt.ylabel('Background Rejection')

ax.set_xlim(0,1.0)
ax.set_ylim(0,1.0)
leg = plt.legend(loc='upper left')
hfont = {'fontname':'Helvetica'}
plt.annotate('$p_{T}$ Inclusive', xy=(.02,.16),xycoords='axes fraction', horizontalalignment='left',fontsize=14,  **hfont)
plt.annotate('$0.0 < |\eta| < 0.6$', xy=(.02,.09),xycoords='axes fraction', horizontalalignment='left',fontsize=14,  **hfont)
plt.annotate('Unconverted Photons', xy=(.02,.02),xycoords='axes fraction', horizontalalignment='left',fontsize=14,  **hfont)
add_watermark()

def get_eff_points(roc):
    binA = roc.GetXaxis().FindBin(0.8) 
    binB = roc.GetXaxis().FindBin(0.9)
    return roc.GetBinContent(binA), roc.GetBinContent(binB)

# ROI
if ROI:
    plt.axvline(x=0.8,color='g',linestyle='--')
    plt.axvline(x=0.9,color='g',linestyle='--')
    plt.annotate('ROI',xy=(0.825,0.15), xycoords='figure fraction',color='g',ha='center')
    print  'Nominal:\n    0.8=%.2f\n    0.9=%.2f' % get_eff_points(nominal_bdt)
    print  'Topo-Added:\n    0.8=%.2f\n    0.9=%.2f' % get_eff_points(topo_bdt)

# Signal for background
else:
    # Get background rejection
    wp = 0.80
    topo_bin_values = []
    nominal_bin_values = []
    for i in range(0, nominal_bdt.GetNbinsX()):
        nominal_bin_values.append(nominal_bdt.GetBinContent(i))
        topo_bin_values.append(topo_bdt.GetBinContent(i))
    # closest value to working point
    nominal_closest_signal_val = min(nominal_bin_values, key=lambda x:abs(x-wp))
    topo_closest_signal_val = min(topo_bin_values, key=lambda x:abs(x-wp))

    # Bin number of closest bin to working point
    nominal_signal_bin_no = nominal_bdt.FindBin(wp)
    topo_signal_bin_no = topo_bdt.FindBin(wp)

    # Background of closest bin
    nominal_backgroundRej_value_at_wp = nominal_bdt.GetBinContent(nominal_signal_bin_no)
    topo_backgroundRej_value_at_wp = topo_bdt.GetBinContent(topo_signal_bin_no)

    print "Nominal - for %.3f background rejection, %.3f signal" % (nominal_backgroundRej_value_at_wp,wp)
    print "Topo - for %.3f background rejection, %.3f signal" % (topo_backgroundRej_value_at_wp, wp)
    print "improvement - ", (topo_backgroundRej_value_at_wp-nominal_backgroundRej_value_at_wp)*100

plt.tight_layout()

plt.savefig('run/plots/roc_comp_cuts.pdf')
plt.close()
