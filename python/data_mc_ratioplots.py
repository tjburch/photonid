from rootpy.io import root_open
import rootpy.plotting.root2matplotlib as rplt
import matplotlib.pyplot as plt
from standard_functions import add_watermark
from rootpy.plotting.style import get_style, set_style
import numpy as np
import glob
import argparse
# this is to make one, complete pdf with all variables
import matplotlib.backends.backend_pdf
import matplotlib.gridspec as gridspec
import matplotlib.lines as mlines
from labels import topo_refs
import pdb

# Globally define the splits
eta_splits = [0.0, 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37]
eta_inner = [0.0, 0.6, 0.8, 1.15, 1.37]
eta_outer = [1.52, 1.81, 2.01, 2.37]
#pt_splits = [10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 80]
pt_splits = [20, 25, 30, 35, 40, 45, 50, 60, 80]

variable_list = [
    'y_nTopoClusters',
    'y_topoCluster0_secondLambda',
    'y_topoCluster0_secondR',
    'y_topoCluster0_centerMag',
    'y_topoCluster0_centerLambda',
    'y_topoCluster0_isolation',
    'y_topoCluster0_engBadCells',
    'y_topoCluster0_nBadCells',
    'y_topoCluster0_badLarQFrac',
    'y_topoCluster0_engPos',
    'y_topoCluster0_avgLarQ',
    'y_topoCluster0_avgTileQ',
    'y_topoCluster0_emProbability',
    'y_Rphi',
    'y_fracs1',
    'y_Eratio',
    'y_wtots1',
    'y_deltae',
    'y_weta1',
    'y_weta2',
    'y_Reta',
    'y_Rhad1',
]         
class Document:

    def __init__(self, output_directory, files):

        self.full_doc = matplotlib.backends.backend_pdf.PdfPages(output_directory+".pdf")
        for variable in variable_list:
            print "Plotting: ", variable
            for conversion_type in ['Converted', 'Unconverted']:
                for eta_scheme in ['lower', 'upper']:
                    thisPage = Page(variable, input_sets=files, conversion=conversion_type, eta_range=eta_scheme)                     
                    self.full_doc.savefig(thisPage.figure)
                    plt.savefig(output_directory+'/ratio_'+thisPage.variable+'_'+conversion_type+'_'+eta_scheme+'Eta')
                    plt.close()

class Page:

    def __init__(self, variable,  input_sets, ID='loose', eta_range='lower', conversion='Converted'):

        self.variable = variable
        if eta_range == "lower": self.eta = eta_inner
        elif eta_range == "upper": self.eta = eta_outer
        
        self.figure, self.axes = plt.subplots(nrows=len(pt_splits)-1, ncols=len(self.eta)-1, sharex=True, sharey=True, squeeze=True, figsize=(11,8.5))

        # Container to be used for proxy lines
        lBuilder =[]
        maxy = 0.0

        # Match eta and pt to file
        for ieta in self.eta:
            if ieta == 2.37 or ieta == 1.37: continue
            eta_index = self.eta.index(ieta)
            this_eta_range = (ieta, self.eta[eta_index+1])

            for pt in pt_splits:
                if pt == 80: continue
                pt_index = pt_splits.index(pt)
                this_pt_range = (pt, pt_splits[pt_index+1])

                # Once a match is found, get associated histogram
                # Do this for each input file
                hists_to_plot = []
                for set in input_sets:
                    for iFile in set:
                        if this_eta_range == iFile.eta_range and this_pt_range == iFile.pt_range:
                            for obj in iFile.file:
                                # find right variable                            
                                if variable in obj.GetName() and conversion in obj.GetName():
                                    hists_to_plot.append(Histogram(obj, this_eta_range, this_pt_range, conversion, ID, iFile.isData))
                # Plot stuff                
                # Change to right axis
                ax = self.axes[pt_index][eta_index]
                p = Plot(hists_to_plot, ax)
                if p.max > maxy: maxy = p.max     
                # Fill proxy lines for legend iff all lines are valid
                if len(hists_to_plot) == len(input_sets) \
                and len(lBuilder) == 0 \
                and all([h.valid for h in hists_to_plot]):
                    lBuilder = hists_to_plot

        for xAx in self.axes:
            for iAx in xAx:
                iAx.set_ylim(0,2)
                iAx.axhline(1, color='gray',linestyle='--')

        # Formatting 
        plt.subplots_adjust(wspace=0.1, hspace=0.15)
        # Add space below plot for labels
        plt.tight_layout()
        self.figure.subplots_adjust(bottom=0.11)
        # General annotations
        plt.annotate(s=conversion, xy=(0.01,0.06), xycoords='figure fraction', ha='left')
        plt.annotate(s=variable, xy=(0.01,0.04), xycoords='figure fraction', ha='left')
        if variable in topo_refs:
            plt.annotate(s=topo_refs[variable], xy=(0.01,0.01), xycoords='figure fraction', ha='left')
        plt.annotate(s='Data/MC', xy=(0.90,0.01), xycoords='figure fraction', ha='left',fontsize=18)



class Plot:

    def __init__(self, histogram_list, ax):
        self.max = 0
        if len(histogram_list) < 2: 
            # print "Empty histogram list"
            return
            #raise IndexError

        # pull properties from the first histogram        
        if histogram_list[0].valid == True:            


            self.eta_range = histogram_list[0].eta_range
            self.pt_range = histogram_list[0].pt_range

            if all([h.valid for h in histogram_list]):
                for h in histogram_list:
                    if h.isData: data_hist = h.hist
                    else: mc_hist = h.hist
                h_ratio = data_hist.Clone()
                h_ratio = h_ratio.divide(data_hist, mc_hist)
                
                h_ratio.linecolor = 'cornflowerblue'
                rplt.errorbar(h_ratio, markersize=0, axes=ax)

                if (self.eta_range[0] == 0.0 or self.eta_range[0] == 1.52):
                    ax.set_ylabel('$%i < p_{T} < %i$'%(self.pt_range[0],self.pt_range[1]),labelpad=40, rotation=0)
                    plt.ylim(0,max([1.1*h.hist.GetMaximum() for h in histogram_list]))

                if (self.pt_range[0] == 60 ):
                    ax.set_xlabel('$%.2f < |\eta| < %.2f$'%(self.eta_range[0],self.eta_range[1]), rotation=0)

                self.max = h_ratio.GetMaximum()

class Histogram:

    def __init__(self, hist, eta_range, pt_range, conversion, ID, isData):
        if hist.Integral() == 0 : 
            self.valid = False
            return
        self.valid = True
        hist.Scale(1/hist.Integral())
        self.name = hist.GetName()
        self.variable = (x for x in variable_list if self.name in x)
        self.convType = conversion
        # if 'Unconverted' in self.name: self.convType = 'Unconverted'
        # else: self.convType = 'Converted'
        self.hist = hist
        # Propagate these to histogram
        self.eta_range = eta_range
        self.pt_range = pt_range
        self.ID = ID
        self.mean = hist.GetMean()
        self.isData = isData


class InputFile:

    def __init__(self, filename):

        self.file = root_open(filename)
        # Build info based on properties of the filename
        # Lock format XetaX_YptY_Conversion
        # Lock _ delimiter
        split_filename = filename.replace('.root','').split('_')
        self.eta_range = (
            next(float(item.split('eta')[0]) for item in split_filename if 'eta' in item),
            next(float(item.split('eta')[1]) for item in split_filename if 'eta' in item)
        )
        self.pt_range = (
            int(next(float(item.split('pt')[0]) for item in split_filename if 'pt' in item)),
            int(next(float(item.split('pt')[1]) for item in split_filename if 'pt' in item))
        )
        if 'tight' in filename: self.ID = 'tight'
        else: self.ID = 'loose'

        self.isData = False
        if "data" in filename: self.isData = True

        self.histograms = []
        for h in self.file:
            if h.Integral() != 0:
                if 'Unconverted' in h.GetName():
                    self.histograms.append(Histogram(h, self.eta_range, self.pt_range, 'Unconverted', self.ID, self.isData))
                else: 
                    self.histograms.append(Histogram(h, self.eta_range, self.pt_range, 'Converted', self.ID, self.isData))

if __name__=='__main__':

    style = get_style('ATLAS')
    set_style(style)
    
    # Get the files
    signal_tight = glob.glob('histograms/signal_tight_histograms/*eta*')
    #background_tight = glob.glob('histograms/background_tight_histograms/*eta*')
    data_tight = glob.glob('histograms/data_tight_histograms/*eta*')

    # Test code, delete when done
    mc_files = []
    d_files = []
    #for f in background_tight:
    #    b_files.append(InputFile(f))
    for f in signal_tight:
        mc_files.append(InputFile(f))
    for f in data_tight:
        d_files.append(InputFile(f))

    inputs = [mc_files, d_files]
    #inputs = [mc_files]

    #print len(s_file.histograms)

    Document(output_directory='run/dataMC', files=inputs)