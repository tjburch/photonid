from rootpy.io import root_open
import glob
from collections import namedtuple

eta_map={
    "0" : (0, 0.6),
    "1" : (0.6, 0.8),
    "2" : (0.8, 1.15),
    "3" : (1.15, 1.37),
    "5" : (1.52, 1.81),
    "6" : (1.81, 2.01),
    "7" : (2.01, 2.37)
}
pt_map={
    "1" : (25,30),
    "2" : (30,40),
    "3" : (40,50),
    "4" : (50,60),
    "5" : (60,80),
    "6" : (80,100),
    "7" : (100,125),
    "8" : (125,150),
    "9" : (150,175),
    "10" : (175,250),
    "11" : (250, 500),
    "12" : (500,1500),
}


# Input Files
f_inputs = glob.glob('cuts_files/*.root')
# Make into pairs
f_pairs = []
f_inclusive_pairs = []
for f in f_inputs:
    f_base = f.replace('.root','')
    for f_pair in f_inputs:
        if f == f_pair: continue # Skip over matching preemtively
        if f_base in f_pair:
            if "Inclusive" in f:
                f_inclusive_pairs.append((f,f_pair))
            else:
                f_pairs.append((f,f_pair))            
            # TODO: why does removal not work?
            #f_inputs.remove(f)            
            #f_inputs.remove(f_pair)
            continue

# Sort inputs
f_pairs = sorted(f_pairs, key=lambda f_tuple: (int(filter(str.isdigit, f_tuple[0])[0]), int(filter(str.isdigit, f_tuple[0])[1])))
f_inclusive_pairs = sorted(f_inclusive_pairs, key=lambda f_tuple: (int(filter(str.isdigit, f_tuple[0]))))

FileInputs = namedtuple('FileInputs',['eta_range','pt_range'])

# initalize csv
output_csv = open('data/cuts_roc_efficiency.csv','w+')
output_csv.write('filename,eta_range,pt_range,nominal_auc,topo_auc,nominal_roi,topo_roi,eta_bin,pt_bin,conversion,wp,nominal_bkg_rej,topo_bkg_rej,sig_entries,bkg_entries\n')

f_pairs = f_pairs+f_inclusive_pairs
#print f_pairs

for (f_nom, f_topo) in f_pairs:
    nominal = root_open(f_nom)
    topoAdded = root_open(f_topo)
    # Use str.isdigit to extract pt and eta ranges
    # assuming configEta0Pt1Mu1Converted_nominal.xml file type
    eta_string = filter(str.isdigit, f_nom)[0]
    pt_string = f_nom.split('Pt')[1].split('Mu')[0]
    if "Inclusive" in f_nom:
        thisFile = FileInputs(eta_range=eta_map[eta_string], pt_range='inclusive')
    else:
        thisFile = FileInputs(eta_range=eta_map[eta_string], pt_range=pt_map[pt_string])

    def build_name(method, filename):
            filename_strip = filename.split('/')[-1]
            filename_strip = filename_strip.replace('.root','')
            if "Inclusive" not in filename:        
                return ('%s/%s/MVA_%s_rejBvsS' % (method,filename_strip,filename_strip))
            else:
                filename_strip = filename_strip.replace('PtInclusive','Pt1')
                return ('%s/Method_%s/%s/MVA_%s_rejBvsS')% (filename_strip,filename_strip,filename_strip,filename_strip)

    nominal_roc = nominal.Get(build_name('Method_Cuts',f_nom))
    signal_events = nominal.Get('/'.join(build_name('Method_Cuts',f_nom).split('/')[:-1]) + '/Cuts_tr_S').GetEntries()
    background_events = nominal.Get('/'.join(build_name('Method_Cuts',f_nom).split('/')[:-1]) + '/Cuts_tr_B').GetEntries()
    topo_roc = topoAdded.Get(build_name('Method_Cuts',f_topo))

    def get_roc_vals(roc_curve):
        auc = roc_curve.Integral()
        roi_auc = roc_curve.Integral(roc_curve.GetXaxis().FindBin(0.8), roc_curve.GetXaxis().FindBin(0.9))
        return (auc, roi_auc)

    nominal_auc, nominal_roi_auc = get_roc_vals(nominal_roc)
    topo_auc, topo_roi_auc = get_roc_vals(topo_roc)

    # Get background rejection
    wp = 0.85
    topo_bin_values = []
    nominal_bin_values = []
    for i in range(0, nominal_roc.GetNbinsX()):
        nominal_bin_values.append(nominal_roc.GetBinContent(i))
        topo_bin_values.append(topo_roc.GetBinContent(i))
    # closest value to working point
    nominal_closest_signal_val = min(nominal_bin_values, key=lambda x:abs(x-wp))
    topo_closest_signal_val = min(topo_bin_values, key=lambda x:abs(x-wp))

    # Bin number of closest bin to working point
    nominal_signal_bin_no = nominal_roc.FindBin(wp)
    topo_signal_bin_no = topo_roc.FindBin(wp)

    # Background of closest bin
    nominal_backgroundRej_value_at_wp = nominal_roc.GetBinContent(nominal_signal_bin_no)
    topo_backgroundRej_value_at_wp = topo_roc.GetBinContent(topo_signal_bin_no)

    # Write to file
    eta_s = "%.2f < |eta| < %.2f" % (thisFile.eta_range[0], thisFile.eta_range[1])
    if "Inclusive" not in f_nom:
        pt_s = "%i < pt < %i" % (thisFile.pt_range[0], thisFile.pt_range[1])
    else: 
        pt_s = "inclusive"
    conversion = f_nom.split('Mu1')[1].split('MC15')[0]

    if int(eta_string) > 4: 
        eta_string = str(int(eta_string)-1)

    output_csv.write('%s,%s,%s,%.3f,%.3f,%.3f,%.3f,%s,%s,%s,%.2f,%.3f,%.3f,%i,%i\n' % (f_nom, eta_s, pt_s, nominal_auc, topo_auc, nominal_roi_auc, topo_roi_auc,eta_string,pt_string,conversion,wp,nominal_backgroundRej_value_at_wp,topo_backgroundRej_value_at_wp,signal_events,background_events))
