# Rendering jupyter notebooks to a real script
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc

# Globally defined labels
eta_labels = [
    '$0 < |\eta| < 0.6$',
    '$0.6 <|\eta| < 0.8$',
    '$0.8 < |\eta|< 1.15$',
    '$1.15 < |\eta|< 1.37$',
    '$1.52 < |\eta|< 1.81$',
    '$1.81 < |\eta|< 2.01$',
    '$2.01 < |\eta| < 2.37$',
]
pt_labels = [
    '$25 < p_{T} < 30 $',
    '$30 < p_{T} < 40 $',
    '$40 < p_{T} < 50 $',
    '$50 < p_{T} < 60 $',
    '$60 < p_{T} < 80 $',
    '$80 < p_{T} < 100 $',
    '$100 < p_{T} < 125 $',
    '$125 < p_{T} < 150 $',
    '$150 < p_{T} < 175 $',
    '$175 < p_{T} < 250 $',
    '$250 < p_{T} < 500 $',
    '$500 < p_{T} < 1500 $',
]


def add_cols_for_method(df, source_nominal, source_topo, method_name):
    df['dRej_%s' % method_name] = 100*(df[source_topo] - df[source_nominal])
    df['y_err_%s' % method_name] = np.sqrt((abs(df['dRej_%s' % method_name])/100*(1-abs(df['dRej_%s' % method_name])/100))/df['bkg_events'])*100
    df['dRej_%s_norm' % method_name] = 100*(df[source_topo] - df[source_nominal]) / (1 - df[source_nominal])


def add_gains(df):
    add_cols_for_method(df, 'cuts_bkg_rej', 'cuts_topo_bkg_rej', 'cuts')
    add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'bdt_topo_bkg_rej','bdt')
    #add_cols_for_method(df, 'nn_bkg_rej', 'nn_topo_bkg_rej','nn')

    # Method to Method
    add_cols_for_method(df, 'cuts_bkg_rej', 'bdt_nominal_bkg_rej', 'bdt2cut')
    add_cols_for_method(df, 'cuts_bkg_rej', 'nn_bkg_rej', 'nn2cut')

    add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'nn_bkg_rej', 'nn2bdt')

    # Full gains
    add_cols_for_method(df, 'cuts_bkg_rej', 'bdt_topo_bkg_rej', 'bdt2base')
    #add_cols_for_method(df, 'cuts_bkg_rej', 'nn_topo_bkg_rej', 'nn2base')

    # reweighted
    add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'BDT_RW_bkg_rej','BDT')
    add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'NN_RW_bkg_rej','NN_nonRW_bkg_rej')

    # Reweighted vs trained non-inclusively
    #    add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'BDT_unRW_bkg_rej','BDT_inclusive')
    #FIX
    add_cols_for_method(df, 'bdt_nominal_bkg_rej', 'BDT_unRW_bkg_rej','BDT_inclusive')
    add_cols_for_method(df, 'nn_bkg_rej', 'NN_nonRW_bkg_rej','NN_inclusive')


def isolate_conversion_pt(df, conversion='Unconverted', pt_inclusive=False):
    """Slices DF based on conversion and pt type
    Arguments:
        df {pd.DataFrame} -- DF to be sliced
    Keyword Arguments:
        conversion {str} -- Conversion type (default: {'Unconverted'})
        pt_inclusive {bool} -- Inclusive or not for pT (default: {False})    
    Returns:
        [pd.DataFrame] -- Sliced DataFrame
    """

    df = df[df.conversion == conversion]
    if pt_inclusive: df = df[df.pt_range == 'inclusive']
    else: df = df[df.pt_range != 'inclusive']
    return df


def offset_binning(df):
    """offsets location of variable for binning location
    TODO: you could do this with a series just as well
    Arguments:
        df {pd.DataFrame}    
    Returns:
        [pd.DataFrame] -- binning adjusted
    """
    df['loc_bin_eta'] = df['eta_bin']+0.5
    return df


def plot_style(ax):
    ax.set_xlim(xmin=0, xmax=7)
    ax.set_ylim(ymin=0)
    ax.xaxis.grid(True)
    ax.set_xticklabels(eta_labels, rotation=30, fontsize=12)
    ax.get_legend().remove()
    ax.set_xlabel('')


def annotate_plot(conversion, wp, method, location='upper left', doMethod=False):
    #print method
    if "2" not in method: doMethod = True
    hfont = {'fontname': 'Helvetica'}

    if method is not 'cuts': method = method.upper()
    elif method is 'cuts': method = method.capitalize()

    if location == 'upper left':
        rc('text', usetex=True)
        plt.annotate(r"\textit{\textbf{ATLAS}} Internal", xy=(.02, .93), xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
        rc('text', usetex=False)

        plt.annotate('$p_{T}$ Inclusive', xy=(.02, .87), xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
        plt.annotate('{0:g}% Signal Efficiency'.format(wp*100), xy=(.02, .81), xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
        plt.annotate('{0} Photons'.format(conversion), xy=(.02, .75), xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
        if doMethod:
            plt.annotate('{0}'.format(method), xy=(.02, .69), xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)


    elif location == 'lower left':

        if doMethod:
            rc('text', usetex=True)
            plt.annotate(r"\textit{\textbf{ATLAS}} Internal", xy=(.02, .28), xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
            rc('text', usetex=False)

            plt.annotate('$p_{T}$ Inclusive', xy=(.02, .22),xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
            plt.annotate('{0:g}% Signal Efficiency'.format(wp*100), xy=(.02,.16), xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
            plt.annotate('{0} Photons'.format(conversion), xy=(.02, .10), xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
            plt.annotate('{0}'.format(method), xy=(.02, .04), xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
        else:
            rc('text', usetex=True)
            plt.annotate(r"\textit{\textbf{ATLAS}} Internal", xy=(.02, .22),xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
            rc('text', usetex=False)

            plt.annotate('$p_{T}$ Inclusive', xy=(.02, .16), xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
            plt.annotate('{0:g}% Signal Efficiency'.format(wp*100), xy=(.02,.10),xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
            plt.annotate('{0} Photons'.format(conversion), xy=(.02, .04),xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)            


def inclusive_gain_plot(df, savename, method, doTotal=False, doMethod=False):
    hfont = {'fontname': 'Helvetica'}
    df.plot(x='loc_bin_eta', y='dRej_%s' % method, marker='_', linestyle="None", markersize=60, figsize=(8, 6), yerr='y_err_%s' % method)
    ax = plt.gca()
    plot_style(ax)
    ax.set_ylabel('Absolute change in background rejection (%)', fontsize=16, **hfont)
    ax.tick_params(axis='y', labelsize=12)

    # This assumes consistent working point
    wp = df['wp'].iloc[0]
    # This assumes the DF has been sliced to just have one conversion already
    conversion = df['conversion'].iloc[0]
    annotate_plot(conversion, wp, method, location='lower left')
    plt.tight_layout()
    plt.savefig('run/gain/{0}.pdf'.format(savename))
    plt.close()

def inclusive_gain_plot_normalized(df, savename, method, doTotal=False, doMethod=False, **kwargs):
    hfont = {'fontname': 'Helvetica'}
    ### Normalized plot
    df.plot(x='loc_bin_eta', y='dRej_%s_norm' % method, marker='_', linestyle="None", markersize=60, figsize=(8, 6), yerr='y_err_%s' % method, **kwargs)
    ax = plt.gca()
    plot_style(ax)
    ax.set_ylabel('Relative change in background rejection (%)', fontsize=16, **hfont)
    ax.tick_params(axis='y', labelsize=12)

    # This assumes consistent working point
    wp = df['wp'].iloc[0]
    # This assumes the DF has been sliced to just have one conversion already
    conversion = df['conversion'].iloc[0]
    annotate_plot(conversion, wp, method, location='lower left')
    #plt.tight_layout()
    #plt.savefig('run/gain/{0}.pdf'.format(savename+'_normalized'))
    #plt.close()


if __name__ == "__main__":

    # input files
    #df_cuts = pd.read_csv('data/cuts_roc_efficiency.csv')
    df_bdt =  pd.read_csv('data/efficiency_w_nn.csv')

    # Separate
    #df_cuts_unconverted_ptInclusive = isolate_conversion_pt(df_cuts, conversion='Unconverted', pt_inclusive=True)
    #df_cuts_converted_ptInclusive = isolate_conversion_pt(df_cuts, conversion='Converted', pt_inclusive=True)
    df_unconverted_ptInclusive = isolate_conversion_pt(df_bdt, conversion='Unconverted', pt_inclusive=True)
    #df_bdt_converted_ptInclusive = isolate_conversion_pt(df_bdt, conversion='Converted', pt_inclusive=True)

    # Add delta and errors
    
    #dataframes = [df_cuts_unconverted_ptInclusive, df_cuts_converted_ptInclusive, df_bdt_unconverted_ptInclusive, df_bdt_converted_ptInclusive]
    """
    dataframes = [df_cuts_unconverted_ptInclusive, df_bdt_unconverted_ptInclusive]
    for df in dataframes:
        df = add_gains(df)
        df = offset_binning(df)
    """
    # Plot total 
    tmva_dfs = [df_unconverted_ptInclusive]
    for df in tmva_dfs:
        df = offset_binning(df)
        df = add_gains(df)
   # Plot
    inclusive_gain_plot_normalized(df_unconverted_ptInclusive, 'inclusive_cuts_unconverted', 'cuts')
    plt.close()
    inclusive_gain_plot_normalized(df_unconverted_ptInclusive, 'inclusive_bdt_unconverted', 'bdt')
    plt.close()
    inclusive_gain_plot_normalized(df_unconverted_ptInclusive, 'inclusive_nn_unconverted', 'nn')
    plt.close()

    inclusive_gain_plot_normalized(df_unconverted_ptInclusive, 'inclusive_BDT2cut_unconverted', 'bdt2cut')
    plt.close()
    inclusive_gain_plot_normalized(df_unconverted_ptInclusive, 'inclusive_NN2cut_unconverted', 'nn2cut')
    plt.close()



    inclusive_gain_plot_normalized(df_unconverted_ptInclusive, 'inclusive_BDTtotal_unconverted', 'bdt2base')
    plt.close()
    inclusive_gain_plot_normalized(df_unconverted_ptInclusive, 'inclusive_NNtotal_unconverted', 'nn2base')
    plt.close()

    # Put those on the same axes
    fig = plt.figure()
    ax = plt.gca()
    inclusive_gain_plot_normalized(df_unconverted_ptInclusive, 'inclusive_NNtotal_unconverted', 'nn2base', color='firebrick', label='NN', ax=ax)    
    inclusive_gain_plot_normalized(df_unconverted_ptInclusive, 'inclusive_BDTtotal_unconverted', 'bdt2base', color='cornflowerblue', label='BDT', ax=ax)
    ax.legend(loc='lower right', fontsize='large', markerscale=.5, frameon=False)
    plt.tight_layout()
    plt.savefig('run/gain/nn_bdt_overlay.pdf')
    plt.close()