from rootpy.io import root_open
import rootpy.plotting.root2matplotlib as rplt
import argparse
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from standard_functions import add_watermark, normalize
from rootpy.plotting.style import get_style, set_style

# import from other files to save some space
from pID_plotting import hist_titles_1d, find_hist_match


def make_plot(h_signal, h_background, ax):    
    
    h_signal.linecolor = "cornflowerblue"
    h_background.linecolor = "firebrick"

    for hist in [h_signal, h_background]:
        try:
            normalize(hist)
        except ZeroDivisionError:
            print "Zero division error on ", hist
            pass
        rplt.errorbar(hist,  markersize=0, axes=ax)


if __name__=='__main__':

    parser = argparse.ArgumentParser('Generates TH objects from single photon nTuples')
    parser.add_argument('-o', dest='outputFolder', required=True, help='Output folder')
    parser.add_argument('-e', dest='etaRange', required=False, help='Eta range to scan (match filename)')
    args = parser.parse_args()

    style = get_style('ATLAS')
    set_style(style)

    # For this script I'll just hardcode the input files since it's a bit specialized
    # Open all the files
    dataset_dir = '/Users/tburch/Documents/gitDevelopment/photonid/histograms/'
    
    if args.etaRange:
        f_signal = root_open('%s/split_signal_histogram_eta_%s.root'% (dataset_dir, args.etaRange))
        f_signal_tight = root_open('%s/split_signal_tight_histogram_eta_%s.root'% (dataset_dir, args.etaRange))
        f_background = root_open('%s/split_background_histogram_eta_%s.root'% (dataset_dir, args.etaRange))
        f_background_tight = root_open('%s/split_background_tight_histogram_eta_%s.root'% (dataset_dir, args.etaRange))
    
    else:
        f_signal = root_open(dataset_dir + '/signal_histogram.root')
        f_signal_tight = root_open(dataset_dir + '/signal_tight_histogram.root')
        f_background = root_open(dataset_dir + '/background_histogram.root')
        f_background_tight = root_open(dataset_dir + '/background_tight_histogram.root')

    for signal_obj in f_signal:
        try:
            # case for Hist

            # Make subplots side by side
            fig = plt.figure(figsize=(11,6))
            shared_ax = fig.add_subplot(111, frameon=False)
            plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
            ax1 = fig.add_subplot(121)
            ax2 = fig.add_subplot(122)

            plt.tight_layout()

            # Do normal plot
            # ------------------------------------------------------------ 
            bkg_obj = find_hist_match(signal_obj, f_background)
            make_plot(signal_obj, bkg_obj, ax1)
            
            # Make matching for tight selection
            # ------------------------------------------------------------ 
            tight_signal_obj = find_hist_match(signal_obj, f_signal_tight)
            tight_background_obj = find_hist_match(signal_obj, f_background_tight)
            make_plot(tight_signal_obj, tight_background_obj, ax2)

            # Make y-lim min 0
            plt.axes(ax1)
            plt.ylim(ymin=0)
            plt.axes(ax2)
            plt.ylim(ymin=0)

            # Make x-axis match
            xlim = plt.xlim()
            plt.axes(ax1)
            plt.xlim(xlim)

            # legend
            labels = ['$\gamma$+jets', 'jet-fakes']
            leg = plt.legend(labels)

            plt.axes(ax1)
            labels_evts = [
                '$\gamma$+jets \nMean = %.2f; nEntries = %i '% (signal_obj.GetMean(), signal_obj.GetEntries()),
                'jet-fakes \nMean = %.2f; nEntries = %i '% (bkg_obj.GetMean(), bkg_obj.GetEntries()),
            ]
            leg_evts_loose = plt.legend(labels_evts)
            
            plt.axes(ax2)
            labels_evts = [
                '$\gamma$+jets \nMean = %.2f; nEntries = %i '% (tight_signal_obj.GetMean(), tight_signal_obj.GetEntries()),
                'jet-fakes \nMean = %.2f; nEntries = %i '% (tight_background_obj.GetMean(), tight_background_obj.GetEntries()),
            ]
            leg_evts_loose = plt.legend(labels_evts)



            # Annotate
            ax1.set_title("Loose ID Requirement")
            ax2.set_title("Tight ID Requirement")


            shared_ax.set_xlabel(hist_titles_1d(signal_obj))
            shared_ax.set_ylabel("AU (normalized to 1)")
            add_watermark()
            plt.savefig(args.outputFolder+signal_obj.GetTitle())
            plt.close()

        except TypeError:
            # Handle Hist2D
            pass