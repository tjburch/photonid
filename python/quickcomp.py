from rootpy.io import root_open
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
import rootpy.plotting.root2matplotlib as rplt
from standard_functions import add_watermark

set_style(get_style('ATLAS'))
f_bdt = root_open('bdt_files/BDT_eta0ptInclusiveUnconverted_nominal.root')
roc_bdt = f_bdt.Get('SinglePhoton/Method_BDT/BDT/MVA_BDT_rejBvsS')
roc_cuts = f_bdt.Get('SinglePhoton/Method_cuts/cuts/MVA_cuts_rejBvsS')

fig= plt.figure()
ax=plt.gca()

roc_bdt.linecolor = "firebrick"
roc_cuts.linecolor = "cornflowerblue"

rplt.hist(roc_bdt, markersize=0, label='BDT' ,fill=False)
rplt.hist(roc_cuts, markersize=0, label='Cuts Method', fill=False)

wp = 0.85
bdt_bin_values = []
cuts_bin_values = []
for i in range(0, roc_cuts.GetNbinsX()):
    cuts_bin_values.append(roc_cuts.GetBinContent(i))
    bdt_bin_values.append(roc_bdt.GetBinContent(i))
# closest value to working point
cuts_closest_signal_val = min(cuts_bin_values, key=lambda x:abs(x-wp))
bdt_closest_signal_val = min(bdt_bin_values, key=lambda x:abs(x-wp))

# Bin number of closest bin to working point
cuts_signal_bin_no = roc_cuts.FindBin(wp)
bdt_signal_bin_no = roc_bdt.FindBin(wp)

# Background of closest bin
cuts_backgroundRej_value_at_wp = roc_cuts.GetBinContent(cuts_signal_bin_no)
bdt_backgroundRej_value_at_wp = roc_bdt.GetBinContent(bdt_signal_bin_no)


ax.set_xlim(0,1.0)
ax.set_ylim(0,1.0)
leg = plt.legend(loc='lower right')

hfont = {'fontname':'Helvetica'}
plt.annotate('$p_{T}$ Inclusive', xy=(.02,.16),xycoords='axes fraction', horizontalalignment='left',fontsize=14,  **hfont)
plt.annotate('$0.0 < |\eta| < 0.6$', xy=(.02,.09),xycoords='axes fraction', horizontalalignment='left',fontsize=14,  **hfont)
plt.annotate('Unconverted Photons', xy=(.02,.02),xycoords='axes fraction', horizontalalignment='left',fontsize=14,  **hfont)


plt.xlabel('Signal Efficiency ($\epsilon_{sig}$)', x=1.0,ha='right')
plt.ylabel('Background Rejection ($1-\epsilon_{bkg}$)', y=1.0, ha='right')
add_watermark()
plt.tight_layout()
plt.savefig('bdt_to_cuts_compare.pdf')

print "cuts ", cuts_backgroundRej_value_at_wp
print "bdt ", bdt_backgroundRej_value_at_wp
print "improve by ", (bdt_backgroundRej_value_at_wp - cuts_backgroundRej_value_at_wp)/cuts_backgroundRej_value_at_wp
