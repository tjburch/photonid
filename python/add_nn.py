"""Jank quick script to update NN values into csv, let's make this better in the future!
"""

from glob import glob

fout = open('data/efficiency_w_nn.csv','w')
fbase = open('data/bdt_roc_efficiency.csv','r')

for line in fbase:

    for f in glob('nn_logs/*nominal*.log'):

        eta = f.split('/')[-1][3]
        if 'pt' in f:
            pt = f.split('/')[-1].split('_')[1].split('pt')[1]
        else:
            pt = 'Inclusive'

        if "unconverted" in f : conversion = "Unconverted"
        elif "converted" in f : conversion = "Converted"

        info_string = line.split('_')[2]
        nom_bkg_rej = 'failure'
        if 'eta{}'.format(eta) in info_string and 'pt%s%s'%(pt,conversion) in line:
            f_open = open(f,'r')
            for l in f_open.readlines():
                if "Signal Efficiency, " in l:
                    nom_bkg_rej = l.split()[4]
            
            line = line.rstrip() + ',%s'%nom_bkg_rej

    for f in glob('nn_logs/*topo*.log'):

        eta = f.split('/')[-1][3]
        if 'pt' in f:
            pt = f.split('/')[-1].split('_')[1].split('pt')[1]
        else:
            pt = 'Inclusive'

        if "unconverted" in f : conversion = "Unconverted"
        elif "converted" in f : conversion = "Converted"

        info_string = line.split('_')[2]
        topo_bkg_rej = 'failure'
        if 'eta{}'.format(eta) in info_string and 'pt%s%s'%(pt,conversion) in line:
            f_open = open(f,'r')
            for l in f_open.readlines():
                if "Signal Efficiency, " in l:                
                    topo_bkg_rej = l.split()[4]

            
            line = line.rstrip() + ',%s'%topo_bkg_rej

    # Change header
    if "filename,eta_range,pt_range" in line:
        line = line.rstrip() + ',nn_bkg_rej,nn_topo_bkg_rej'

    if '\n' not in line:
        line = line+'\n'

    fout.write(line)
fout.close()
fbase.close()