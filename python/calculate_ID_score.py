import ROOT
import sys
import numpy as np
import array 
import argparse

parser = argparse.ArgumentParser("Give pT and eta")
parser.add_argument("-e", dest="eta", required=True, help="eta bin")
parser.add_argument('-c', dest='converted',action='store_true', help='conversion')
parser.add_argument('-t', dest='topo',action='store_true', help='do topo clusters')
args = parser.parse_args()

eta_map={
    "0" : (0, 0.6),
    "1" : (0.6, 0.8),
    "2" : (0.8, 1.15),
    "3" : (1.15, 1.37),
    "5" : (1.52, 1.81),
    "6" : (1.81, 2.01),
    "7" : (2.01, 2.37)
}

def find_associated_score(filename):
    f = ROOT.TFile(filename)
    t = f.Get("GammaJet_train")
    if args.converted:
        model_name = "/bdata/tburch/public/forAri/BDT_eta{0}ptInclusive_c_/weights/BoostedDecisionTree_BDT.weights.xml".format(args.eta)
    else:
        model_name = "/bdata/tburch/public/forAri/BDT_eta{0}ptInclusive__/weights/BoostedDecisionTree_BDT.weights.xml".format(args.eta)

    # Build cut
    s_cut = 'acceptEventPtBin && '
    s_cut += 'y_f1 > 0.005 && '
    s_cut += 'y_wtots1 > -10 && '
    s_cut += 'y_weta1 > -100 && '
    if args.converted:
        s_cut += 'y_convType != 0 && '
    else:
        s_cut += 'y_convType == 0 && '
    s_cut += 'abs(y_eta) > %s && ' % eta_map[args.eta][0]
    s_cut += 'abs(y_eta) < %s && ' % eta_map[args.eta][1]
    s_cut += 'evt_mu > 1 && '
    s_cut += 'evt_mu < 60 '

    t.Draw(">>signalEventList", s_cut,'goff')
    eventList = ROOT.gDirectory.Get("signalEventList")

    reader = ROOT.TMVA.Reader()
    y_Rhad1 = array.array('f', [0])
    reader.AddVariable('y_Rhad1',y_Rhad1)
    y_Reta = array.array('f', [0])
    reader.AddVariable('y_Reta',y_Reta)
    y_Rphi = array.array('f', [0])
    reader.AddVariable('y_Rphi',y_Rphi)
    y_weta1 = array.array('f', [0])
    y_weta2 = array.array('f', [0])
    reader.AddVariable('y_weta1',y_weta1)
    reader.AddVariable('y_weta2',y_weta2)
    y_wtots1 = array.array('f', [0])
    reader.AddVariable('y_wtots1',y_wtots1)
    y_fracs1 = array.array('f', [0])
    reader.AddVariable('y_fracs1',y_fracs1)
    y_deltae = array.array('f', [0])
    reader.AddVariable('y_deltae',y_deltae)
    y_Eratio = array.array('f', [0])
    reader.AddVariable('y_Eratio',y_Eratio)

    y_ptcone20 = array.array('f', [0])
    reader.AddSpectator('y_ptcone20', y_ptcone20)
    y_ptcone40 = array.array('f', [0])
    reader.AddSpectator('y_ptcone40', y_ptcone40)
    y_topoetcone20 = array.array('f', [0])
    reader.AddSpectator('y_topoetcone20', y_topoetcone20)
    y_topoetcone40 = array.array('f', [0])
    reader.AddSpectator('y_topoetcone40', y_topoetcone40)
    y_iso_FixedCutLoose = array.array('f', [0])
    reader.AddSpectator('y_iso_FixedCutLoose', y_iso_FixedCutLoose)
    y_iso_FixedCutTight = array.array('f', [0])
    reader.AddSpectator('y_iso_FixedCutTight', y_iso_FixedCutTight)
    y_iso_FixedCutTightCaloOnly = array.array('f', [0])
    reader.AddSpectator('y_iso_FixedCutTightCaloOnly', y_iso_FixedCutTightCaloOnly)
    y_pt = array.array('f', [0])
    reader.AddSpectator('y_pt', y_pt)

    h_eval = ROOT.TH1F("h_eval","h_eval", 40,-1,1)
    reader.BookMVA("BDT::BDT", model_name)

    for i in range(0, eventList.GetN()):
        entry = eventList.GetEntry(i)
        t.GetEntry(entry)
        if t.y_isTruthMatchedPhoton == 0: continue

        y_Rhad1[0] = t.y_Rhad1
        y_Reta[0] = t.y_Reta
        y_Rphi[0] = t.y_Rphi
        y_weta2[0] = t.y_weta2
        y_weta1[0] = t.y_weta1
        y_wtots1[0] = t.y_wtots1
        y_fracs1[0] = t.y_fracs1
        y_deltae[0] = t.y_deltae
        y_Eratio[0] = t.y_Eratio
        y_ptcone20[0] = t.y_ptcone20
        y_ptcone40[0] = t.y_ptcone40
        y_topoetcone20[0] = t.y_topoetcone20
        y_topoetcone40[0] = t.y_topoetcone40
        y_iso_FixedCutLoose[0] = t.y_iso_FixedCutLoose
        y_iso_FixedCutTight[0] = t.y_iso_FixedCutTight
        y_iso_FixedCutTightCaloOnly[0] = t.y_iso_FixedCutTightCaloOnly
        y_pt[0] = t.y_pt       
        bdt_eval = reader.EvaluateMVA('BDT::BDT')
        h_eval.Fill(bdt_eval, t.mcTotWeight)

    total_signal = h_eval.Integral()

    signal_efficiency, distance, score =[], [], []
    maxbin = h_eval.GetXaxis().FindBin(1)
    for i in np.arange(-1, 1, 0.05):
        cutbin = h_eval.GetXaxis().FindBin(i)
        thisSignal = h_eval.Integral(cutbin, maxbin) / total_signal
        signal_efficiency.append(thisSignal)
        distance.append(abs(thisSignal - 0.85))
        score.append(i)
    minidx= np.argmin(distance)
    signal_efficiency_val = round(signal_efficiency[minidx], 3)

    print("Eta idx: {0} , Converted Photons: {1}".format(args.eta, args.converted))
    print("Min val at: {0}".format(minidx))
    print("True signal Efficiency: {0}".format(signal_efficiency_val))
    print("score: {0}".format(score[minidx]))
    c = ROOT.TCanvas()
    h_eval.Draw()
    c.SaveAs("{0}{1}.pdf".format(args.eta,args.converted))


def main():
    find_associated_score("/bdata/tburch/tmva_submit/TMVA_ready_withSF_traintest.root")


if __name__ == "__main__":
    main()