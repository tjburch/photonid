import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import h5py


def drop_columns(df, list_to_drop):
    #drop out columns we don't really need
    for column in to_drop:
        try:
            df = df.drop(column, axis='columns')
        except ValueError:
            print("%s not in columns, continuing." % column)
    return df


def make_plot(corr, annot=False):
    # Generate a mask for the upper triangle
    mask = np.zeros_like(corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True

    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(11, 9))

    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(220, 10, as_cmap=True)

    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(corr, mask=mask, cmap=cmap, vmax=1, vmin=-1, center=0,
                square=True, linewidths=.5, cbar_kws={"shrink": .5}, annot=annot)


def load_file(filename):
    """Returns pandas dataframe for file name

    Arguments:
        filename {string} -- file to load
    """

    h5 = h5py.File(filename, 'r')
    dataname = h5.get('SinglePhoton')
    #dataname = h5.get('eta_0_0p6')
    n_arr = np.array(dataname)
    df = pd.DataFrame(n_arr)
    return df


def rank_corr(corr):
    c = corr.abs()
    s = c.unstack()
    so = s.sort_values(kind="quicksort") #sort low to high
    so = so[so < 1.0] #skims off self ones
    so = so.iloc[::2]
    return so.iloc[::-1] # reverse (high to low)

if __name__=='__main__':

    do_Btight = False
    do_Stight = False
    do_Bloose = False
    do_Sloose = False
    do_Snone = True
    do_Bnone = True
    annotate = True

    if annotate:
        dir = 'run/corr_annotate/'
    else:
        dir = 'run/corr/'

    tmp_df_bkg_tight = load_file('/xdata/tburch/workspace/photonid/ntuples/background_none_selection_slim.h5')
    #to_drop = [ c for c in tmp_df_bkg_tight.columns if "topoCluster" in c]
    to_drop = ['y_topoCluster0_nBadCells', 'y_topoCluster0_engBadCells','y_topoCluster0_badLarQFrac','y_topoCluster0_centerMag','y_topoCluster0_engPos','y_topoCluster0_avgTileQ']
    f_name =dir+"ss_only_correlations.txt"
    f_output = open(f_name,'w')
    f_output.close()

    # Background Tight
    if do_Btight:
        df_bkg_tight = load_file('/xdata/tburch/workspace/photonid/ntuples/background_tight_selection_slim.h5')
        df_bkg_tight = drop_columns(df_bkg_tight, to_drop)
        corr = df_bkg_tight.corr()
        if annotate:
            make_plot(corr, annot=True)
        else:
            make_plot(corr)
        plt.annotate('Background Correlation', xy=(.9,.95),xycoords='figure fraction', horizontalalignment='right', verticalalignment='top', size=32)
        plt.annotate('TightID Applied', xy=(.9,.90),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)
        plt.annotate('$|\eta|<0.6$', xy=(.9,.87),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)
        plt.annotate('$p_{T}$ Inclusive', xy=(.9,.83),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)

        plt.tight_layout()
        plt.savefig(dir+'bkg_tight_corr.pdf')

        f_output = open(f_name,'a')
        f_output.write("\nBackground Tight Ranking\n")
        f_output.write("________________________\n")
        f_output.close()
        rank_corr(corr).head(30).round(3).to_csv(path=f_name, mode='a')

    # Signal Tight
    if do_Stight:
        df_sig_tight = load_file('/xdata/tburch/workspace/photonid/ntuples/signal_tight_selection_slim.h5')
        df_sig_tight = drop_columns(df_sig_tight, to_drop)
        corr = df_sig_tight.corr()
        if annotate:
            make_plot(corr, annot=True)
        else:
            make_plot(corr)
        plt.annotate('Signal Correlation', xy=(.9,.95),xycoords='figure fraction', horizontalalignment='right', verticalalignment='top', size=32)
        plt.annotate('TightID Applied', xy=(.9,.90),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)
        plt.annotate('$|\eta|<0.6$', xy=(.9,.87),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)
        plt.annotate('$p_{T}$ Inclusive', xy=(.9,.83),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)



        plt.tight_layout()
        plt.savefig(dir+'sig_tight_corr.pdf')

        f_output = open(f_name,'a')
        f_output.write("\nSignal Tight Ranking\n")
        f_output.write("________________________\n")
        f_output.close()
        rank_corr(corr).head(30).round(3).to_csv(path=f_name, mode='a')


    # Background loose
    if do_Bloose:
        df_bkg_loose = load_file('/xdata/tburch/workspace/photonid/ntuples/background_selection_slim.h5')
        df_bkg_loose = drop_columns(df_bkg_loose, to_drop)
        corr = df_bkg_loose.corr()
        if annotate:
            make_plot(corr, annot=True)
        else:
            make_plot(corr)
        plt.annotate('Background Correlation', xy=(.9,.95),xycoords='figure fraction', horizontalalignment='right', verticalalignment='top', size=32)
        plt.annotate('LooseID Applied', xy=(.9,.90),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)
        plt.annotate('$|\eta|<0.6$', xy=(.9,.87),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)
        plt.annotate('$p_{T}$ Inclusive', xy=(.9,.83),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)

        plt.tight_layout()
        plt.savefig(dir+'bkg_loose_corr.pdf')

        f_output = open(f_name,'a')
        f_output.write("\nBackground Loose Ranking\n")
        f_output.write("________________________\n")
        f_output.close()
        rank_corr(corr).head(30).round(3).to_csv(path=f_name, mode='a')


    # Signal loose
    if do_Sloose:
        df_sig_loose = load_file('/xdata/tburch/workspace/photonid/ntuples/signal_selection_slim.h5')
        df_sig_loose = drop_columns(df_sig_loose, to_drop)
        corr = df_sig_loose.corr()
        if annotate:
            make_plot(corr, annot=True)
        else:
            make_plot(corr)
        plt.annotate('Signal Correlation', xy=(.9,.95),xycoords='figure fraction', horizontalalignment='right', verticalalignment='top', size=32)
        plt.annotate('LooseID Applied', xy=(.9,.90),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)
        plt.annotate('$|\eta|<0.6$', xy=(.9,.87),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)
        plt.annotate('$p_{T}$ Inclusive', xy=(.9,.83),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)

        plt.tight_layout()
        plt.savefig(dir+'sig_loose_corr.pdf')

        f_output = open(f_name,'a')
        f_output.write("\nSignal Loose Ranking\n")
        f_output.write("________________________\n")
        f_output.close()
        rank_corr(corr).head(30).round(3).to_csv(path=f_name, mode='a')

    if do_Snone:
        df_sig_none = load_file('/xdata/tburch/workspace/photonid/ntuples/signal_none_selection_slim.h5')
        df_sig_none = drop_columns(df_sig_none, to_drop)
        corr = df_sig_none.corr()
        if annotate:
            make_plot(corr, annot=True)
        else:
            make_plot(corr)
        plt.annotate('Signal Correlation', xy=(.9,.95),xycoords='figure fraction', horizontalalignment='right', verticalalignment='top', size=32)
        plt.annotate('No ID Applied', xy=(.9,.90),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)
        plt.annotate('$|\eta|$ Inclusive', xy=(.9,.87),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)
        plt.annotate('$p_{T}$ Inclusive', xy=(.9,.83),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)

        plt.tight_layout()
        plt.savefig(dir+'sig_none_corr.pdf')

        f_output = open(f_name,'a')
        f_output.write("\nSignal None Ranking\n")
        f_output.write("________________________\n")
        f_output.close()
        rank_corr(corr).head(30).round(3).to_csv(path=f_name, mode='a')

    if do_Bnone:
        df_bkg_none = load_file('/xdata/tburch/workspace/photonid/ntuples/background_none_selection_slim.h5')
        df_bkg_none = drop_columns(df_bkg_none, to_drop)
        corr = df_bkg_none.corr()
        if annotate:
            make_plot(corr, annot=True)
        else:
            make_plot(corr)
        plt.annotate('Background Correlation', xy=(.9,.95),xycoords='figure fraction', horizontalalignment='right', verticalalignment='top', size=32)
        plt.annotate('No ID Applied', xy=(.9,.90),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)
        plt.annotate('$|\eta|$ Inclusive', xy=(.9,.87),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)
        plt.annotate('$p_{T}$ Inclusive', xy=(.9,.83),xycoords='figure fraction', horizontalalignment='right',verticalalignment='top',size=21)

        plt.tight_layout()
        plt.savefig(dir+'bkg_none_corr.pdf')

        f_output = open(f_name,'a')
        f_output.write("\Background None Ranking\n")
        f_output.write("________________________\n")
        f_output.close()
        rank_corr(corr).head(30).round(3).to_csv(path=f_name, mode='a')
