import ROOT
import sys

try:
    f = ROOT.TFile(sys.argv[1])
except IndexError:
    print "Pass TMVA output as arg"
    sys.exit(1)

# Get AUC for BDT Method
directory = f.Get('eta_0_0p6')
method_bdt = directory.Get('Method_BDT')
bdt = method_bdt.Get('BDT')
roc_bdt = bdt.Get('MVA_BDT_rejBvsS')
bdt_auc = roc_bdt.Integral()

binA=roc_bdt.GetXaxis().FindBin(0.8)
binB=roc_bdt.GetXaxis().FindBin(0.9)
roi_auc = roc_bdt.Integral(binA,binB)
# Get AUC for rectangular cuts method
# method_cuts = directory.Get('Method_rect_cuts')
# rect_cuts = method_cuts.Get('rect_cuts')
# roc_rect_cuts = rect_cuts.Get('MVA_rect_cuts_trainingRejBvsS')
# rect_cuts_auc = roc_rect_cuts.Integral()

print "ROC AUC for"
print "    BDT = %f" % bdt_auc
print "    ROI = %f" % roi_auc
# print "    rectangular cuts = %f" % rect_cuts_auc




