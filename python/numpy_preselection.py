import h5py
import numpy as np
import sys
if len(sys.argv) < 2 :
    print("Usage: python file_to_preselect.root output.py")
    sys.exit()

signal = True
if sys.argv[3] == '-b':
    signal = False
    print("Skimming %s for Background Events" % sys.argv[1])
else: print("Skimming %s for Signal Events" % sys.argv[1])

f_input = h5py.File(sys.argv[1])
arr = f_input.get('SinglePhoton')

if signal:
    selection = (arr["y_isTruthMatchedPhoton"]) &\
                (arr["acceptEventPtBin"]) &\
                (arr["y_IsLoose"] != 0) & \
                (arr["y_f1"] > 0.005) & \
                (arr["y_wtots1"] > -10) & \
                (arr["y_weta1"] > -100) & \
                (arr["evt_mu"] < 60) & \
                (arr["evt_mu"] > 1)
else:
    selection = (arr["y_isTruthMatchedPhoton"] == False) &\
                (arr["acceptEventPtBin"]) &\
                (arr["y_IsLoose"] != 0) & \
                (arr["y_f1"] > 0.005) & \
                (arr["y_wtots1"] > -10) & \
                (arr["y_weta1"] > -100) & \
                (arr["evt_mu"] < 60) & \
                (arr["evt_mu"] > 1)    

arr_preselected = np.extract(selection, arr)

h5f = h5py.File(sys.argv[2],'w')
h5f.create_dataset('SinglePhoton',data=arr_preselected)
h5f.close()