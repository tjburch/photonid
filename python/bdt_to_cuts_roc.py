from rootpy.io import root_open
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
import rootpy.plotting.root2matplotlib as rplt
from standard_functions import add_watermark

ROI = False
#fbase = '/afs/cern.ch/work/t/tburch/photonID/PhotonIDCutsOptimization/photonIDoptim/build/'
f1 = 'bdt_files/BDT_eta0ptInclusiveUnconverted_nominal.root'
#f2 = 'bdt_files/BDT_eta0ptInclusiveUnconverted_topo.root'
#f1 = fbase+'Eta0Pt1Mu1UnconvertedMC15.root'
#f2 = fbase+'Eta0Pt1Mu1UnconvertedMC15_topo.root'

nominal = root_open(f1)

cut_roc = nominal.Get('SinglePhoton/Method_cuts/cuts/MVA_cuts_rejBvsS')
bdt_roc = nominal.Get('SinglePhoton/Method_BDT/BDT/MVA_BDT_rejBvsS')
#nominal_bdt = nominal.Method_Cuts.Eta0Pt1Mu1UnconvertedMC15.MVA_Eta0Pt1Mu1UnconvertedMC15_trainingRejBvsS
#topo_bdt = topoAdded.Method_Cuts.Eta0Pt1Mu1UnconvertedMC15_topo.MVA_Eta0Pt1Mu1UnconvertedMC15_topo_trainingRejBvsS


fig= plt.figure()
ax=plt.gca()

cut_roc.linecolor = "firebrick"
auc = cut_roc.Integral()
cuts_label ='Cut-based:\n    AUC=%.2f' % (auc) 


bdt_roc.linecolor = "cornflowerblue"
auc = bdt_roc.Integral()
bdt_label = 'BDT:\n    AUC=%.2f' % (auc)

curves = [cut_roc]
rplt.hist(cut_roc, markersize=0, label=cuts_label ,fill=False)
rplt.hist(bdt_roc, markersize=0, label=bdt_label, fill=False)
#rplt.hist(curves, stacked=False, markersize=0)

plt.xlabel('Signal Efficiency')
plt.ylabel('Background Rejection')

ax.set_xlim(0,1.0)
ax.set_ylim(0,1.0)
leg = plt.legend(loc='upper left', frameon=False,bbox_to_anchor=(0.00, 0.95))
hfont = {'fontname':'Helvetica'}
plt.annotate('$p_{T}$ Inclusive', xy=(.02,.16),xycoords='axes fraction', horizontalalignment='left',fontsize=14,  **hfont)
plt.annotate('$0.0 < |\eta| < 0.6$', xy=(.02,.09),xycoords='axes fraction', horizontalalignment='left',fontsize=14,  **hfont)
plt.annotate('Unconverted Photons', xy=(.02,.02),xycoords='axes fraction', horizontalalignment='left',fontsize=14,  **hfont)
add_watermark()

def get_eff_points(roc):
    binA = roc.GetXaxis().FindBin(0.8) 
    binB = roc.GetXaxis().FindBin(0.9)
    return roc.GetBinContent(binA), roc.GetBinContent(binB)

# ROI
if ROI:
    plt.axvline(x=0.8,color='g',linestyle='--')
    plt.axvline(x=0.9,color='g',linestyle='--')
    plt.annotate('ROI',xy=(0.825,0.15), xycoords='figure fraction',color='g',ha='center')
    print('Nominal:\n    0.8=%.2f\n    0.9=%.2f' % get_eff_points(cut_roc))
    print('Topo-Added:\n    0.8=%.2f\n    0.9=%.2f' % get_eff_points(bdt_roc))

# Signal for background
else:
    # Get background rejection
    wp = 0.95
    plt.axvline(wp,color='g',linestyle='--')
    bdt_bin_values = []
    cut_bin_values = []
    for i in range(0, cut_roc.GetNbinsX()):
        cut_bin_values.append(cut_roc.GetBinContent(i))
        bdt_bin_values.append(bdt_roc.GetBinContent(i))
    # closest value to working point
    cut_closest_signal_val = min(cut_bin_values, key=lambda x:abs(x-wp))
    bdt_closest_signal_val = min(bdt_bin_values, key=lambda x:abs(x-wp))

    # Bin number of closest bin to working point
    cut_signal_bin_no = cut_roc.FindBin(wp)
    bdt_signal_bin_no = bdt_roc.FindBin(wp)

    # Background of closest bin
    cut_backgroundRej_value_at_wp = cut_roc.GetBinContent(cut_signal_bin_no)
    bdt_backgroundRej_value_at_wp = bdt_roc.GetBinContent(bdt_signal_bin_no)

    print("Cuts - for %.3f background rejection, %.3f signal" % (cut_backgroundRej_value_at_wp,wp))
    print("BDT - for %.3f background rejection, %.3f signal" % (bdt_backgroundRej_value_at_wp, wp))
    print("improvement - ", 100*(bdt_backgroundRej_value_at_wp-cut_backgroundRej_value_at_wp)/cut_backgroundRej_value_at_wp)

plt.tight_layout()

plt.savefig('run/plots/roc_comp_cuts.pdf')
plt.close()
