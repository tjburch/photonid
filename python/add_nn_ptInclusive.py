"""
This script will add the NN background rejection to the data CSVs
Meant to caputure the output of ../learning/apply_model_binned.py
"""
fout = open('data/efficiency_w_ptinclusive_nn.csv','w')
fbase = open('data/bdt_roc_efficiency_ptReweighted.csv','r')
f_csv = 'data/performance_rw.csv'

for l in fbase:

    # Change header
    if "filename,eta_range,pt_range" in l:
        l = l.rstrip() + ',nn_bkg_rej\n'

    elif "Unconverted" in l:
        base_eta = l.split(',')[7]
        base_pt = l.split(',')[8]

        with open(f_csv,'r') as csvfile:
            for line in csvfile.readlines():
                line =line.rstrip()
                if "background_rejection" not in line:

                    splitline = line.split(',')
                    print(splitline)
                    if int(splitline[0]) > 3:
                        this_eta = int(splitline[0])-1
                    else:
                        this_eta = int(splitline[0])

                    this_pt = int(splitline[1])
                    this_signal_efficiency = float(splitline[2])
                    this_background_rejection = float(splitline[3].replace('\n',''))
                    if int(base_eta) == int(this_eta) and base_pt == this_pt:
                        l = l.rstrip() + ",{0}\n".format(this_background_rejection)

    fout.write(l)

fout.close()
fbase.close()
