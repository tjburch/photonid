//////////////////////////////////
//   Adds rw sF to ntuples      //
//       Tyler James Burch      //
//////////////////////////////////

// Root Junk
#include <TROOT.h>
#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <TH1F.h>
#include <TLorentzVector.h>
#include <TBranch.h>

// Other Junk
#include <iostream>
#include <utility>
#include <string>
#include <vector>
#include <math.h>
#include <boost/progress.hpp>

using namespace std;

void activate_ttree(TTree *t_selection);

int main(int argc, char *argv[])
{

    TString input_signal(argv[1]);
    TString input_bkg(argv[2]);
    TString output_file(argv[3]);

    //Get File, Tree
    cout << "Opening Files" << endl;
    TFile *i_sig = TFile::Open(input_signal);
    cout << "Signal File Opened" << endl;
    TFile *i_bkg = TFile::Open(input_bkg);
    cout << "Background File Opened" << endl;
    TFile *f_out = new TFile(output_file, "RECREATE");

    cout << "Getting TTrees" << endl;
    TTree *t_sig = (TTree *)i_sig->Get("SinglePhoton");
    TTree *t_bkg = (TTree *)i_bkg->Get("SinglePhoton");
    cout << "TTrees received, activating useful branches" << endl;
    // Turn off useless branches
    activate_ttree(t_sig);
    activate_ttree(t_bkg);

    // Generate output trees
    cout << "Cloning for outputs" << endl;
    TTree *tout_sig = t_sig->CloneTree(0);
    tout_sig->SetName("GammaJet");
    TTree *tout_bkg = t_bkg->CloneTree(0);
    tout_bkg->SetName("JetJet");

    // Set histogram parameters
    float binwidth = 5.; // width in GeV
    float minval = 0.;
    float maxval = 1000.;
    int nbins = round((maxval - minval) / binwidth);

    // Draw the histograms
    cout << "Drawing Histograms" << endl;
    TH1F *h_sig_pt = new TH1F("h_sig_pt", "h_sig_pt", nbins, minval, maxval);
    TH1F *h_bkg_pt = new TH1F("h_bkg_pt", "h_bkg_pt", nbins, minval, maxval);

    // Draw these
    TCanvas *c1 = new TCanvas();
    t_sig->Draw("y_pt >> h_sig_pt", "mcTotWeight", "HIST");
    t_bkg->Draw("y_pt >> h_bkg_pt", "mcTotWeight","HIST SAME");

    // Divide those
    TH1F *h_ratio = (TH1F*)h_sig_pt->Clone();
    h_ratio->Divide(h_bkg_pt);

    // Normalize for drawing
    h_sig_pt->Scale(1/h_sig_pt->Integral());
    h_bkg_pt->Scale(1/h_bkg_pt->Integral());

    h_sig_pt->SetLineColor(kBlue);
    h_bkg_pt->SetLineColor(kRed);
    h_sig_pt->SetAxisRange(0, 1.3*max(h_sig_pt->GetMaximum(), h_bkg_pt->GetMaximum()),"Y");
    h_sig_pt->SetAxisRange(0, 200,"X");

    c1->SaveAs("/xdata/tburch/workspace/photonid/run/validation/validation_pre.eps");
    cout << "File Saved" << endl;


    // pT to be used
    Float_t y_pt_sig, y_pt_bkg;
    t_sig->SetBranchAddress("y_pt", &y_pt_sig);
    t_bkg->SetBranchAddress("y_pt", &y_pt_bkg);

    // Adding SF branch
    Double_t reweight_SF_signal = 0;
    Double_t reweight_SF_background = 0;

    tout_sig->Branch("reweight_SF", &reweight_SF_signal);
    tout_bkg->Branch("reweight_SF", &reweight_SF_background);

    // ------------------------------------------------------------------------
    // Loop over signal tree
    Long64_t nentries = t_sig->GetEntries();
    cout << "Starting running over Signal Tree" << endl;
    cout << "nEntries Used: " << nentries << endl;
    boost::progress_display show_progress(nentries);
    Long64_t nbytes = 0, nb = 0;
    for (Long64_t jentry = 0; jentry < nentries; jentry++)
    {
        ++show_progress;
        t_sig->GetEntry(jentry);
        nbytes += nb;

        // Since we're reweighting the signal, we just need to fill 1 over and over
        reweight_SF_signal = 1.;
        tout_sig->Fill();
    }
    cout << "Done with Signal Tree" << endl;


    // Now Background
    Long64_t nentries_bkg = t_bkg->GetEntries();
    cout << "Starting running over Background Tree" << endl;
    cout << "nEntries Used: " << nentries_bkg << endl;
    boost::progress_display show_progress_bkg(nentries_bkg);
    Long64_t nbytes_bkg = 0, nb_bkg = 0;
    for (Long64_t jentry = 0; jentry < nentries_bkg; jentry++)
    {
        ++show_progress_bkg;
        t_bkg->GetEntry(jentry);
        nbytes_bkg += nb_bkg;

        int associated_pt_bin = h_ratio->GetXaxis()->FindBin(y_pt_bkg);
        reweight_SF_background = h_ratio->GetBinContent(associated_pt_bin);
        tout_bkg->Fill();
    }
    cout << "Done with Background Tree" << endl;

    // Make Histogram post-run
    cout << "Making post-run histogram" << endl;
    TH1F *h_sig_pt_post = new TH1F("h_sig_pt_post", "h_sig_pt_post", nbins, minval, maxval);
    TH1F *h_bkg_pt_post = new TH1F("h_bkg_pt_post", "h_bkg_pt_post", nbins, minval, maxval);

    c1->Clear(); 
    tout_sig->Draw("y_pt >> h_sig_pt_post", "mcTotWeight * reweight_SF", "HIST");
    tout_bkg->Draw("y_pt >> h_bkg_pt_post", "mcTotWeight * reweight_SF", "HIST SAME");

    h_sig_pt_post->SetLineColor(kBlue);
    h_bkg_pt_post->SetLineColor(kRed);
    h_bkg_pt_post->SetLineStyle(7);
    h_sig_pt_post->SetAxisRange(0, 1.3*max(h_sig_pt_post->GetMaximum(), h_bkg_pt_post->GetMaximum()), "Y");
    h_sig_pt_post->SetAxisRange(0, 200, "X");

    c1->SaveAs("/xdata/tburch/workspace/photonid/run/validation/validation_post.png");
    c1->Clear();
    h_ratio->Draw();
    c1->SaveAs("/xdata/tburch/workspace/photonid/run/validation/ratio.png");

    cout << "Writing Tree" << endl;
    f_out->cd();
    tout_sig->Write();
    tout_bkg->Write();
    f_out->Close();
    cout << "Done." << endl;

}

/** 
 * Activate only branches we want
*/
void activate_ttree(TTree *t_selection)
{

    t_selection->SetBranchStatus("*", 0);
    t_selection->SetBranchStatus("y_isTruthMatchedPhoton", 1);
    t_selection->SetBranchStatus("y_IsLoose", 1);
    t_selection->SetBranchStatus("acceptEventPtBin", 1);
    t_selection->SetBranchStatus("y_convType",1);
    t_selection->SetBranchStatus("y_eta",1);
    t_selection->SetBranchStatus("evt_mu",1);

    t_selection->SetBranchStatus("y_f1", 1);
    t_selection->SetBranchStatus("y_wtots1", 1);
    t_selection->SetBranchStatus("y_weta1", 1);
    t_selection->SetBranchStatus("y_weta2", 1);
    t_selection->SetBranchStatus("y_Reta", 1);
    t_selection->SetBranchStatus("y_Rphi", 1);
    t_selection->SetBranchStatus("y_fracs1", 1);
    t_selection->SetBranchStatus("y_deltae", 1);
    t_selection->SetBranchStatus("y_Eratio", 1);
    t_selection->SetBranchStatus("y_Rhad1", 1);

    t_selection->SetBranchStatus("y_ptcone20", 1);
    t_selection->SetBranchStatus("y_ptcone40", 1);
    t_selection->SetBranchStatus("y_topoetcone20", 1);
    t_selection->SetBranchStatus("y_topoetcone40", 1);
    t_selection->SetBranchStatus("y_iso_FixedCutLoose", 1);
    t_selection->SetBranchStatus("y_iso_FixedCutTight", 1);
    t_selection->SetBranchStatus("y_iso_FixedCutTightCaloOnly", 1);

    t_selection->SetBranchStatus("y_topoCluster0_secondLambda", 1);
    t_selection->SetBranchStatus("y_topoCluster0_avgLarQ", 1);
    t_selection->SetBranchStatus("y_topoCluster0_engBadCells", 1);
    t_selection->SetBranchStatus("y_topoCluster0_secondR", 1);
    t_selection->SetBranchStatus("y_topoCluster0_badLarQFrac", 1);
    t_selection->SetBranchStatus("y_topoCluster0_avgTileQ", 1);
    t_selection->SetBranchStatus("y_nTopoClusters", 1);
    t_selection->SetBranchStatus("y_topoCluster0_centerLambda", 1);
    t_selection->SetBranchStatus("y_topoCluster0_nBadCells", 1);
    t_selection->SetBranchStatus("y_topoCluster0_emProbability", 1);
    t_selection->SetBranchStatus("y_topoCluster0_isolation", 1);
    t_selection->SetBranchStatus("y_topoCluster0_centerMag", 1);
    t_selection->SetBranchStatus("y_topoCluster0_engPos", 1);

    t_selection->SetBranchStatus("y_pt", 1);
    t_selection->SetBranchStatus("mcTotWeight", 1);
}