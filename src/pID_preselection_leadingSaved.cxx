/** Applies preselection for photonID
 *  Operates under assumption that leading-topo variables are saved already
 *  Author : Tyler James Burch
*/


#include <TROOT.h>
#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <iostream>
#include <TMath.h>
#include <TH1F.h>
#include <utility>
#include <TLorentzVector.h>
#include <TBranch.h>
#include <string>
#include <vector>
#include <boost/progress.hpp>
using namespace std;

float find_deta(float obj1, float obj2);
void activate_ttree(TTree *t_selection);

int main(int argc, char *argv[]) {
    
	TString input_name(argv[1]);
	TString output_name(argv[2]);
	std::string file_type(argv[3]);
	std::string makeTight(argv[4]);
	bool isSignal, isData, applyTight;

	enum FileType { signal, background, data };
	FileType theType;
	if (file_type.compare("signal") == 0) {theType = signal;}
	else if (file_type.compare("background") == 0) {theType = background;}
	else if (file_type.compare("data") == 0) {theType = data;}
	else {
		std::cout << "Ill defined file type! Argv3 should be signal, background, or data but is " << file_type << std::endl;
		return 0;
		}
	if (makeTight.compare("tight") == 0) {applyTight = true;}
	else if (makeTight.compare("loose") == 0) {applyTight = false;}
	else {
		std::cout << "Ill defined ID requirment! Argv4 should be tight or loose, but is " << makeTight << std::endl;
		return 0;
		}

	//Get File, Tree
	TFile * f_input = TFile::Open(input_name);
	TFile *f_output = new TFile(output_name, "RECREATE");

	TTree * t_input= (TTree*)f_input->Get("SinglePhoton");
    Long64_t nentries = t_input->GetEntries();
	std::cout << "Input file: " << input_name << std::endl;
	std::cout << "Apply Tight ID? " << applyTight << std::endl;	

	// Variables used for cuts
    Bool_t y_isTruthMatchedPhoton, y_IsLoose, acceptEventPtBin, y_IsTight;
    Float_t y_f1, y_wtots1, y_weta1, y_eta; 

	t_input->SetBranchAddress("y_isTruthMatchedPhoton", &y_isTruthMatchedPhoton);
	t_input->SetBranchAddress("y_IsLoose", &y_IsLoose);
	t_input->SetBranchAddress("acceptEventPtBin", &acceptEventPtBin);
	t_input->SetBranchAddress("y_f1", &y_f1);
	t_input->SetBranchAddress("y_wtots1", &y_wtots1);
	t_input->SetBranchAddress("y_weta1", &y_weta1);
	t_input->SetBranchAddress("y_eta", &y_eta);
	t_input->SetBranchAddress("y_IsTight" , &y_IsTight);


	// clone tree format, 0 entries
	TTree *t_selection = t_input->CloneTree(0);
	//activate_ttree(t_selection);


	// ------------------------------------------------------------------------
	// Loop over entries, skim
	cout << "nEntries Used: " << nentries << endl;
	boost::progress_display show_progress( nentries ); 
	Long64_t nbytes = 0, nb = 0;
	for (Long64_t jentry = 0; jentry < nentries; jentry++) {
		t_input->GetEntry(jentry);   nbytes += nb;
 		++show_progress;

		if (!y_IsTight && applyTight) {continue;}

        // Overlapping selection
        if (acceptEventPtBin == true &&
            y_IsLoose != 0 &&
            y_f1 > 0.005 &&
            y_wtots1 > -10 &&
            y_weta1 > -100)
        {
            // Signal selection
			switch(theType){
				case signal : {if (y_isTruthMatchedPhoton!=0) { t_selection->Fill();}}
				case background : {if (y_isTruthMatchedPhoton==0) { t_selection->Fill();}}
				case data : {t_selection->Fill();}
			}		
        }
	}

	f_output->cd();
	std::cout << "Writing Tree" << std::endl;
	t_selection->Write();
	std::cout << "Closing File" << std::endl;
	f_output->Close();
	std::cout << "Done!" << std::endl;

}
/** 
 * Returns abs difference of two eta values
*/
float find_deta(float eta1, float eta2){
	return TMath::Abs(eta1 - eta2);
}

/** 
 * Activate only branches we want
 * Not currently called	
*/
void activate_ttree(TTree *t_selection)
{

	t_selection->SetBranchStatus("*",0);
	t_selection->SetBranchStatus("y_isTruthMatchedPhoton", 1);
	t_selection->SetBranchStatus("y_IsLoose", 1);
	t_selection->SetBranchStatus("acceptEventPtBin", 1);
	t_selection->SetBranchStatus("y_f1", 1);
	t_selection->SetBranchStatus("y_wtots1", 1);
	t_selection->SetBranchStatus("y_weta1", 1);
	t_selection->SetBranchStatus("y_weta2", 1);
	t_selection->SetBranchStatus("y_Reta", 1);
	t_selection->SetBranchStatus("y_Rphi", 1);
	t_selection->SetBranchStatus("y_fracs1", 1);
	t_selection->SetBranchStatus("y_deltae", 1);
	t_selection->SetBranchStatus("y_Eratio", 1);

	t_selection->SetBranchStatus("y_topoCluster0_secondLambda" , 1);
	t_selection->SetBranchStatus("y_topoCluster0_avgLarQ" , 1);
	t_selection->SetBranchStatus("y_topoCluster0_engBadCells", 1);
	t_selection->SetBranchStatus("y_topoCluster0_secondR", 1);
	t_selection->SetBranchStatus("y_topoCluster0_badLarQFrac", 1);
	t_selection->SetBranchStatus("y_topoCluster0_avgTileQ", 1);
	t_selection->SetBranchStatus("y_nTopoClusters", 1);
	t_selection->SetBranchStatus("y_topoCluster0_centerLambda" , 1);
	t_selection->SetBranchStatus("y_topoCluster0_nBadCells", 1);
	t_selection->SetBranchStatus("y_topoCluster0_emProbability", 1);
	t_selection->SetBranchStatus("y_topoCluster0_isolation", 1);
	t_selection->SetBranchStatus("y_topoCluster0_centerMag", 1);
	t_selection->SetBranchStatus("y_topoCluster0_engPos", 1);

}