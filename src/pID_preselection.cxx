//////////////////////////////////
//   Pass File, apply Selection //
//       Tyler James Burch      //
//////////////////////////////////
#include <TROOT.h>
#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <iostream>
#include <TMath.h>
#include <TH1F.h>
#include <utility>
#include <TLorentzVector.h>
#include <TBranch.h>
#include <string>
#include <vector>
#include <boost/progress.hpp>

using namespace std;

float find_deta(float obj1, float obj2);
void activate_ttree(TTree *t_selection);

int main(int argc, char *argv[]) {

	TString input_name(argv[1]);
	TString output_name(argv[2]);
	std::string file_type(argv[3]);
	std::string makeTight(argv[4]);
	bool isSignal, isData, applyTight, applyLoose;

	enum FileType { signal, background, data };
	FileType theType;
	if (file_type.compare("signal") == 0) {theType = signal;}
	else if (file_type.compare("background") == 0) {theType = background;}
	else if (file_type.compare("data") == 0) {theType = data;}
	else {
		std::cout << "Ill defined file type! Argv3 should be signal, background, or data but is " << file_type << std::endl;
		return 0;
		}
	if (makeTight.compare("tight") == 0) {
		applyTight = true;
		applyLoose = true;
		}
	else if (makeTight.compare("loose") == 0) {
		applyTight = false;
		applyLoose = true;
		}
	else if (makeTight.compare("none") == 0) {
		applyTight = false;
		applyLoose = false;
		}
	else {
		std::cout << "Ill defined ID requirment! Argv4 should be tight or loose, but is " << makeTight << std::endl;
		return 0;
		}

	//Get File, Tree
	TFile * f_input = TFile::Open(input_name);
	TFile *f_output = new TFile(output_name, "RECREATE");

	TTree * t_input= (TTree*)f_input->Get("SinglePhoton");
    Long64_t nentries = t_input->GetEntries();
	std::cout << "Input file: " << input_name << std::endl;
	std::cout << "Apply Tight ID? " << applyTight << std::endl;

	// Variables used for cuts
    Bool_t y_isTruthMatchedPhoton, y_IsLoose, acceptEventPtBin, y_IsTight;
    Float_t y_f1, y_wtots1, y_weta1, y_eta;

	vector<Double_t> *y_topoCluster_secondLambda = 0;
	vector<Double_t> *y_topoCluster_secondR = 0;
	vector<Double_t> *y_topoCluster_centerMag = 0;
	vector<Double_t> *y_topoCluster_centerLambda = 0;
	vector<Double_t> *y_topoCluster_isolation = 0;
	vector<Double_t> *y_topoCluster_engBadCells = 0;
	vector<Double_t> *y_topoCluster_nBadCells = 0;
	vector<Double_t> *y_topoCluster_badLarQFrac = 0;
	vector<Double_t> *y_topoCluster_engPos = 0;
	vector<Double_t> *y_topoCluster_avgLarQ  = 0;
	vector<Double_t> *y_topoCluster_avgTileQ = 0;
	vector<Double_t> *y_topoCluster_emProbability = 0;

	t_input->SetBranchAddress("y_isTruthMatchedPhoton", &y_isTruthMatchedPhoton);
	T_input->SetBranchAddress("y_IsLoose", &y_IsLoose);
	t_input->SetBranchAddress("acceptEventPtBin", &acceptEventPtBin);
	t_input->SetBranchAddress("y_f1", &y_f1);
	t_input->SetBranchAddress("y_wtots1", &y_wtots1);
	t_input->SetBranchAddress("y_weta1", &y_weta1);
	t_input->SetBranchAddress("y_eta", &y_eta);
	t_input->SetBranchAddress("y_IsTight" , &y_IsTight);

	// New Topo cluster variables
	t_input->SetBranchAddress("y_topoCluster_secondLambda", &y_topoCluster_secondLambda);
	t_input->SetBranchAddress("y_topoCluster_secondR", &y_topoCluster_secondR);
	t_input->SetBranchAddress("y_topoCluster_centerMag", &y_topoCluster_centerMag);
	t_input->SetBranchAddress("y_topoCluster_centerLambda", &y_topoCluster_centerLambda);
	t_input->SetBranchAddress("y_topoCluster_isolation", &y_topoCluster_isolation);
	t_input->SetBranchAddress("y_topoCluster_engBadCells", &y_topoCluster_engBadCells);
	t_input->SetBranchAddress("y_topoCluster_nBadCells", &y_topoCluster_nBadCells);
	t_input->SetBranchAddress("y_topoCluster_badLarQFrac", &y_topoCluster_badLarQFrac);
	t_input->SetBranchAddress("y_topoCluster_engPos", &y_topoCluster_engPos);
	t_input->SetBranchAddress("y_topoCluster_avgLarQ", &y_topoCluster_avgLarQ);
	t_input->SetBranchAddress("y_topoCluster_avgTileQ", &y_topoCluster_avgTileQ);
	t_input->SetBranchAddress("y_topoCluster_emProbability", &y_topoCluster_emProbability);

	// clone tree format, 0 entries
	TTree *t_selection = t_input->CloneTree(0);
	//activate_ttree(t_selection);

	// Add some branches
	double y_topoCluster0_secondLambda, y_topoCluster0_secondR, y_topoCluster0_centerMag, y_topoCluster0_centerLambda,\
	y_topoCluster0_isolation, y_topoCluster0_engBadCells, y_topoCluster0_badLarQFrac, y_topoCluster0_engPos, \
	y_topoCluster0_avgLarQ , y_topoCluster0_avgTileQ, y_topoCluster0_emProbability;
	int y_topoCluster0_nBadCells;

	t_selection->Branch("y_topoCluster0_secondLambda", &y_topoCluster0_secondLambda);
	t_selection->Branch("y_topoCluster0_secondR", &y_topoCluster0_secondR);
	t_selection->Branch("y_topoCluster0_centerMag", &y_topoCluster0_centerMag);
	t_selection->Branch("y_topoCluster0_centerLambda", &y_topoCluster0_centerLambda);
	t_selection->Branch("y_topoCluster0_isolation", &y_topoCluster0_isolation);
	t_selection->Branch("y_topoCluster0_engBadCells", &y_topoCluster0_engBadCells);
	t_selection->Branch("y_topoCluster0_nBadCells", &y_topoCluster0_nBadCells);
	t_selection->Branch("y_topoCluster0_badLarQFrac", &y_topoCluster0_badLarQFrac);
	t_selection->Branch("y_topoCluster0_engPos", &y_topoCluster0_engPos);
	t_selection->Branch("y_topoCluster0_avgLarQ", &y_topoCluster0_avgLarQ );
	t_selection->Branch("y_topoCluster0_avgTileQ", &y_topoCluster0_avgTileQ);
	t_selection->Branch("y_topoCluster0_emProbability", &y_topoCluster0_emProbability);
    t_selection->SetName("SinglePhoton");


	// ------------------------------------------------------------------------
	// Loop over entries, skim
	//if( nentries > 5000000) {nentries = 100000;}
	cout << "nEntries Used: " << nentries << endl;
	boost::progress_display show_progress( nentries );

	Long64_t nbytes = 0, nb = 0;
	for (Long64_t jentry = 0; jentry < nentries; jentry++) {
		++show_progress;
		t_input->GetEntry(jentry);   nbytes += nb;

		if (!y_IsTight && applyTight) {continue;}
		if (!y_IsLoose && applyLoose) {continue;}

        // Overlapping selection
        if (acceptEventPtBin == true &&
            y_f1 > 0.005 &&
            y_wtots1 > -10 &&
            y_weta1 > -100)
        {
			// Fill new ones
			y_topoCluster0_secondLambda = y_topoCluster_secondLambda->at(0);
			y_topoCluster0_secondR = y_topoCluster_secondR->at(0);
			y_topoCluster0_centerMag = y_topoCluster_centerMag->at(0);
			y_topoCluster0_centerLambda = y_topoCluster_centerLambda->at(0);
			y_topoCluster0_isolation = y_topoCluster_isolation->at(0);
			y_topoCluster0_engBadCells = y_topoCluster_engBadCells->at(0);
			y_topoCluster0_nBadCells = y_topoCluster_nBadCells->at(0);
			y_topoCluster0_badLarQFrac = y_topoCluster_badLarQFrac->at(0);
			y_topoCluster0_engPos = y_topoCluster_engPos->at(0);
			y_topoCluster0_avgLarQ  = y_topoCluster_avgLarQ->at(0);
			y_topoCluster0_avgTileQ = y_topoCluster_avgTileQ->at(0);
			y_topoCluster0_emProbability = y_topoCluster_emProbability->at(0);

            // Signal selection
			switch(theType){
				case signal : {if (y_isTruthMatchedPhoton!=0) { t_selection->Fill();}}
				case background : {if (y_isTruthMatchedPhoton==0) { t_selection->Fill();}}
				case data : {t_selection->Fill();}
			}
        }
	}

	f_output->cd();
	std::cout << "Writing Tree" << std::endl;
	t_selection->Write();
	std::cout << "Closing File" << std::endl;
	f_output->Close();
	std::cout << "Done!" << std::endl;

}
/**
 * Returns abs difference of two eta values
*/
float find_deta(float eta1, float eta2){
	return TMath::Abs(eta1 - eta2);
}

/**
 * Activate only branches we want
 * Not currently called
*/
void activate_ttree(TTree *t_selection)
{

	t_selection->SetBranchStatus("*",0);
	t_selection->SetBranchStatus("y_isTruthMatchedPhoton", 1);
	t_selection->SetBranchStatus("y_IsLoose", 1);
	t_selection->SetBranchStatus("acceptEventPtBin", 1);
	t_selection->SetBranchStatus("y_f1", 1);
	t_selection->SetBranchStatus("y_wtots1", 1);
	t_selection->SetBranchStatus("y_weta1", 1);
	t_selection->SetBranchStatus("y_weta2", 1);
	t_selection->SetBranchStatus("y_Reta", 1);
	t_selection->SetBranchStatus("y_Rphi", 1);
	t_selection->SetBranchStatus("y_fracs1", 1);
	t_selection->SetBranchStatus("y_deltae", 1);
	t_selection->SetBranchStatus("y_Eratio", 1);

	t_selection->SetBranchStatus("y_topoCluster_secondLambda" , 1);
	t_selection->SetBranchStatus("y_topoCluster_avgLarQ" , 1);
	t_selection->SetBranchStatus("y_topoCluster_engBadCells", 1);
	t_selection->SetBranchStatus("y_topoCluster_secondR", 1);
	t_selection->SetBranchStatus("y_topoCluster_badLarQFrac", 1);
	t_selection->SetBranchStatus("y_topoCluster_avgTileQ", 1);
	t_selection->SetBranchStatus("y_nTopoClusters", 1);
	t_selection->SetBranchStatus("y_topoCluster_centerLambda" , 1);
	t_selection->SetBranchStatus("y_topoCluster_nBadCells", 1);
	t_selection->SetBranchStatus("y_topoCluster_emProbability", 1);
	t_selection->SetBranchStatus("y_topoCluster_isolation", 1);
	t_selection->SetBranchStatus("y_topoCluster_centerMag", 1);
	t_selection->SetBranchStatus("y_topoCluster_engPos", 1);

}
