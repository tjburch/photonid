//////////////////////////////////
// Literally just makes topo0 variables //
//       Tyler James Burch             //
//////////////////////////////////
#include <TROOT.h>
#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <iostream>
#include <TMath.h>
#include <TH1F.h>
#include <utility>
#include <TLorentzVector.h>
#include <TBranch.h>
#include <string>
#include <vector>
using namespace std;

float find_deta(float obj1, float obj2);
void activate_ttree(TTree *t_selection);

int main(int argc, char *argv[]) {
    
	TString input_name(argv[1]);
	TString output_name(argv[2]);

	//Get File, Tree
	TFile * f_input = TFile::Open(input_name);
	TFile *f_output = new TFile(output_name, "RECREATE");

	TTree * t_input= (TTree*)f_input->Get("SinglePhoton");
    Long64_t nentries = t_input->GetEntries();
	std::cout << "Prepping for TMVA by adding topo0 variables" << std::endl;
	std::cout << "Input file: " << input_name << std::endl;

	// Variables used for cuts
    Bool_t y_isTruthMatchedPhoton, y_IsLoose, acceptEventPtBin, y_IsTight;
    Float_t y_f1, y_wtots1, y_weta1, y_eta; 

	vector<Double_t> *y_topoCluster_secondLambda = 0;
	vector<Double_t> *y_topoCluster_secondR = 0;
	vector<Double_t> *y_topoCluster_centerMag = 0;
	vector<Double_t> *y_topoCluster_centerLambda = 0;
	vector<Double_t> *y_topoCluster_isolation = 0;
	vector<Double_t> *y_topoCluster_engBadCells = 0;
	vector<Double_t> *y_topoCluster_nBadCells = 0;
	vector<Double_t> *y_topoCluster_badLarQFrac = 0;
	vector<Double_t> *y_topoCluster_engPos = 0;
	vector<Double_t> *y_topoCluster_avgLarQ  = 0;
	vector<Double_t> *y_topoCluster_avgTileQ = 0;
	vector<Double_t> *y_topoCluster_emProbability = 0;

	// New Topo cluster variables
	t_input->SetBranchAddress("y_topoCluster_secondLambda", &y_topoCluster_secondLambda);
	t_input->SetBranchAddress("y_topoCluster_secondR", &y_topoCluster_secondR);
	t_input->SetBranchAddress("y_topoCluster_centerMag", &y_topoCluster_centerMag);
	t_input->SetBranchAddress("y_topoCluster_centerLambda", &y_topoCluster_centerLambda);
	t_input->SetBranchAddress("y_topoCluster_isolation", &y_topoCluster_isolation);
	t_input->SetBranchAddress("y_topoCluster_engBadCells", &y_topoCluster_engBadCells);
	t_input->SetBranchAddress("y_topoCluster_nBadCells", &y_topoCluster_nBadCells);
	t_input->SetBranchAddress("y_topoCluster_badLarQFrac", &y_topoCluster_badLarQFrac);
	t_input->SetBranchAddress("y_topoCluster_engPos", &y_topoCluster_engPos);
	t_input->SetBranchAddress("y_topoCluster_avgLarQ", &y_topoCluster_avgLarQ);
	t_input->SetBranchAddress("y_topoCluster_avgTileQ", &y_topoCluster_avgTileQ);
	t_input->SetBranchAddress("y_topoCluster_emProbability", &y_topoCluster_emProbability);

	// clone tree format, 0 entries
	TTree *t_selection = t_input->CloneTree(0);

	// Add some branches
	Double_t y_topoCluster0_secondLambda, y_topoCluster0_secondR, y_topoCluster0_centerMag, y_topoCluster0_centerLambda,\
	y_topoCluster0_isolation, y_topoCluster0_engBadCells, y_topoCluster0_badLarQFrac, y_topoCluster0_engPos, \
	y_topoCluster0_avgLarQ , y_topoCluster0_avgTileQ, y_topoCluster0_emProbability;
	Int_t y_topoCluster0_nBadCells;

	t_selection->Branch("y_topoCluster0_secondLambda", &y_topoCluster0_secondLambda);
	t_selection->Branch("y_topoCluster0_secondR", &y_topoCluster0_secondR);
	t_selection->Branch("y_topoCluster0_centerMag", &y_topoCluster0_centerMag);
	t_selection->Branch("y_topoCluster0_centerLambda", &y_topoCluster0_centerLambda);
	t_selection->Branch("y_topoCluster0_isolation", &y_topoCluster0_isolation);
	t_selection->Branch("y_topoCluster0_engBadCells", &y_topoCluster0_engBadCells);
	t_selection->Branch("y_topoCluster0_nBadCells", &y_topoCluster0_nBadCells);
	t_selection->Branch("y_topoCluster0_badLarQFrac", &y_topoCluster0_badLarQFrac);
	t_selection->Branch("y_topoCluster0_engPos", &y_topoCluster0_engPos);
	t_selection->Branch("y_topoCluster0_avgLarQ", &y_topoCluster0_avgLarQ );
	t_selection->Branch("y_topoCluster0_avgTileQ", &y_topoCluster0_avgTileQ);
	t_selection->Branch("y_topoCluster0_emProbability", &y_topoCluster0_emProbability);
    t_selection->SetName("SinglePhoton");


	// ------------------------------------------------------------------------
	// Loop over entries, skim
	//if( nentries > 5000000) {nentries = 100000;}
	cout << "nEntries Used: " << nentries << endl;

	Long64_t nbytes = 0, nb = 0;
	for (Long64_t jentry = 0; jentry < nentries; jentry++) {
		t_input->GetEntry(jentry);   nbytes += nb;

		if (jentry % 1000000 == 0){std::cout << "On entry: " << jentry << std::endl;}

		// Fill new ones
		y_topoCluster0_secondLambda = y_topoCluster_secondLambda->at(0);
		y_topoCluster0_secondR = y_topoCluster_secondR->at(0);
		y_topoCluster0_centerMag = y_topoCluster_centerMag->at(0);
		y_topoCluster0_centerLambda = y_topoCluster_centerLambda->at(0);
		y_topoCluster0_isolation = y_topoCluster_isolation->at(0);
		y_topoCluster0_engBadCells = y_topoCluster_engBadCells->at(0);
		y_topoCluster0_nBadCells = y_topoCluster_nBadCells->at(0);
		y_topoCluster0_badLarQFrac = y_topoCluster_badLarQFrac->at(0);
		y_topoCluster0_engPos = y_topoCluster_engPos->at(0);
		y_topoCluster0_avgLarQ  = y_topoCluster_avgLarQ->at(0);
		y_topoCluster0_avgTileQ = y_topoCluster_avgTileQ->at(0);
		y_topoCluster0_emProbability = y_topoCluster_emProbability->at(0);

		// Signal selection 			
		t_selection->Fill();								
        
	}

	f_output->cd();
	std::cout << "Writing Tree" << std::endl;
	t_selection->Write();
	std::cout << "Closing File" << std::endl;
	f_output->Close();
	std::cout << "Done!" << std::endl;

}
/** 
 * Returns abs difference of two eta values
*/
float find_deta(float eta1, float eta2){
	return TMath::Abs(eta1 - eta2);
}
